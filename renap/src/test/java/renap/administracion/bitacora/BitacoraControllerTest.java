package renap.administracion.bitacora;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import javax.annotation.PostConstruct;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
//@ContextConfiguration({
//	"classpath:/config/spring/frontTile/mvc-config-test.xml",
//	"classpath:/config/spring/businessTile/application-service-test.xml"
//})
//@RunWith(SpringJUnit4ClassRunner.class)
public class BitacoraControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private AutenticacionService autenticacionService;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders
					.webAppContextSetup(context)
					.alwaysDo(print())
					.build();

	}

	@PostConstruct
	public void postConstruccion(){
		
	}
	
//	@Test
	public void buscar() {
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/administracion/bitacora/buscar")
				.param("idUsuario", "")
				.param("start", "0")
				.param("length", "10")
				.param("draw", "1");
		try {
			this.mockMvc.perform(requestBuilder).andDo(print());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
