package renap.administracion.bitacora;

import java.util.List;

import javax.annotation.PostConstruct;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.base.business.exception.BusinessException;
import mx.gob.segob.dgti.infraestructure.base.integration.exception.RepositoryException;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.service.AdministracionBitacoraService;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.ConsultaBitacoraVO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import renap.comun.UsuarioAutenticacionHelper;

//@ContextConfiguration({"classpath:/config/spring/businessTile/application-service-test.xml"})
//@RunWith(SpringJUnit4ClassRunner.class)
public class BitacoraBusinessTest {

	@Autowired
	private AutenticacionService autenticacionService;
	@Autowired
	private AdministracionBitacoraService administracionBitacoraService;

	@PostConstruct
	public void postConstruccion(){
		UsuarioAutenticacionHelper.autenticarUsuario("admin", autenticacionService);
	}

//	@Test
	public void consultarTotal() throws BusinessException, RepositoryException {
		BusquedaBitacoraVO filtros = null;
		Integer total = administracionBitacoraService.obtenerTotalBitacoras(filtros);
		System.out.println("Bitacora "+total);
		List<ConsultaBitacoraVO> bitacoras = administracionBitacoraService.obtenerBitacoras(filtros, 1, 10);
		System.out.println("Bitacora "+bitacoras);
		if(bitacoras != null){
			System.out.println("Bitacora "+bitacoras.size());
			for(ConsultaBitacoraVO vo : bitacoras){
				System.out.println(vo.getIdMUsuario()+" "+vo.getModulo()+" "+vo.getAccion());
			}
		}
	}


}
