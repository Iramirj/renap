package renap.comun;

import java.util.ArrayList;
import java.util.List;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.AutorizacionConstants;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class UsuarioAutenticacionHelper {
	public static void autenticarUsuario(String nombreUsuario, AutenticacionService autenticacionService){
		try {
			UsuarioAutenticadoVO usuario = autenticacionService.getUsuario(nombreUsuario);

			List<GrantedAuthority> grantedList = new ArrayList<GrantedAuthority>(0);
			List<String> roles = usuario.getPermisos();
			for(String rol : roles){
				grantedList.add(new SimpleGrantedAuthority(rol));
			}
			grantedList.add(new SimpleGrantedAuthority(AutorizacionConstants.ROL_NO_NECESARIO));
			grantedList.add(new SimpleGrantedAuthority(AutorizacionConstants.TODOS_LOS_ROLES));

			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(usuario, usuario.getPassword(), grantedList);
			SecurityContextHolder.getContext().setAuthentication(token);
		} catch (AutenticacionExcepcion e) {
			e.printStackTrace();
		}
	}
	
	public static void exitUser(){
		SecurityContextHolder.clearContext();
	}
}
