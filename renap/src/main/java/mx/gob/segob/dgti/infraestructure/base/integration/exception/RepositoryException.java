package mx.gob.segob.dgti.infraestructure.base.integration.exception;

import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;



public class RepositoryException extends Exception {
	private static final long serialVersionUID = 1714158587023997687L;
	
	private ExcepcionCausaEnum  causaEnum;
	
	public RepositoryException( ExcepcionCausaEnum causaEnum) {
		super(causaEnum.getDescripcion());	
	}

	
	public RepositoryException( ExcepcionCausaEnum causaEnum, Throwable cause) {
		super(causaEnum.getDescripcion(), cause);
				
	}


	public ExcepcionCausaEnum getCausaEnum() {
		return causaEnum;
	}
}
