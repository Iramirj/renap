package mx.gob.segob.dgti.infraestructure.mail.service;

import mx.gob.segob.dgti.infraestructure.mail.vo.MailMessage;

/**
 * @author Hp6460b
 *
 */
public interface MailServices {

	/**
	 * Metodo que soporta correo con formato HTML
	 * 
	 * @param mailMessage : Parametro contiene la informacion del mensaje de correo
	 * 
	 *  from 	: email que se identificara como remitente
	 *  to 		: email a quien se realizara el envio del correo
	 *  subject : asunto que identificara el correo 
	 *  body	: cuerpo del correo ( soporta formato html )
	 *  
	 *  attachments 	: elementos a adjuntar al correo de existir elementos.
	 *  resourcesInLine : elementos a incrustar en el correo ( identificado por un CID (String), y el recurso ) 
	 */
	public abstract void enviarTexto(MailMessage mailMessage);

	/**
	 * Metodo que soporta correo con formato HTML
	 * 
	 * @param mailMessage : Parametro contiene la informacion del mensaje de correo
	 * 
	 *  from 	: email que se identificara como remitente
	 *  to 		: email a quien se realizara el envio del correo
	 *  subject : asunto que identificara el correo 
	 *  body	: cuerpo del correo ( soporta formato html )
	 *  
	 *  attachments 	: elementos a adjuntar al correo de existir elementos.
	 *  resourcesInLine : elementos a incrustar en el correo ( identificado por un CID (String), y el recurso ) 
	 */
	void enviarCorreoHtml(MailMessage mailMessage);

}
