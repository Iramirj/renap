package mx.gob.segob.dgti.renap.comun.presentation.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("validarPermisos")
public class PermisosController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody 
		ResponseEntity<Object> getPerfilUsuario(){
		
		List<String> perfiles = new ArrayList<String>(0);
		Iterator<GrantedAuthority> it = (Iterator<GrantedAuthority>) SecurityContextHolder
										.getContext().getAuthentication()
										.getAuthorities().iterator();
		while(it.hasNext()){
			perfiles.add(it.next().getAuthority());
		}
		
		if(perfiles == null || perfiles.size() == 0){
			return new ResponseEntity<Object>(new String("Error"), HttpStatus.UNAUTHORIZED);
		}
		
		return new ResponseEntity<Object>(perfiles.toArray(new String[0]), HttpStatus.OK);
	}
}
