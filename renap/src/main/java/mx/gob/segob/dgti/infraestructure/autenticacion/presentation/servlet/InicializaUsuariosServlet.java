package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet implementation class InicializaUsuariosServlet
 */
public class InicializaUsuariosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	public void init() throws ServletException {
	    WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());

	    ((AutenticacionService)context.getBean("autenticacionService")).inicializarUsuarios();		
	}
}
