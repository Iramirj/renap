package mx.gob.segob.dgti.infraestructure.autenticacion.business.criptografia;

import java.security.NoSuchAlgorithmException;

public interface Criptografia {
	public String hash( String texto) throws NoSuchAlgorithmException;
}
