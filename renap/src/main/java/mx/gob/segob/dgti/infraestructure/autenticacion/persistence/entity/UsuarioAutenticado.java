package mx.gob.segob.dgti.infraestructure.autenticacion.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UsuarioAutenticado {

	private Integer usuarioId;
	private String usuario;
	private String password;
	private String stRegistro;
	private String logueado;
	private Date ultimoAcceso;
	private Integer numeroIntentos;
	private String bloqueado;
	private Date fechaBloqueo;

	@Id
	@Column(name = "id_m_usuario")
	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	@Column(name = "usuario")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "st_registro")
	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

	@Column(name = "logueado")
	public String getLogueado() {
		return logueado;
	}

	public void setLogueado(String logueado) {
		this.logueado = logueado;
	}

	@Column(name = "ultimo_acceso")
	public Date getUltimoAcceso() {
		return ultimoAcceso;
	}

	public void setUltimoAcceso(Date ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}

	@Column(name = "numero_intentos")
	public Integer getNumeroIntentos() {
		return numeroIntentos;
	}

	public void setNumeroIntentos(Integer numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	@Column(name = "bloqueado")
	public String getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	@Column(name = "fecha_bloqueo")
	public Date getFechaBloqueo() {
		return fechaBloqueo;
	}

	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}

}
