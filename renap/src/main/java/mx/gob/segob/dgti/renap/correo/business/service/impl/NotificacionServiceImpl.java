package mx.gob.segob.dgti.renap.correo.business.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.gob.segob.dgti.renap.correo.business.constants.CorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.constants.ParametrosCorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.helper.MensajeCorreoHelper;
import mx.gob.segob.dgti.renap.correo.business.service.NotificacionService;
import mx.gob.segob.dgti.infraestructure.base.business.service.impl.BaseServiceImpl;
import mx.gob.segob.dgti.infraestructure.base.presentation.helper.MensajeHelper;
import mx.gob.segob.dgti.infraestructure.base.presentation.helper.WebApplicationHelper;
import mx.gob.segob.dgti.infraestructure.mail.service.MailServices;
import mx.gob.segob.dgti.infraestructure.mail.vo.MailMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

@Service(NotificacionService.NOTIFICACION_SERVICE)
public class NotificacionServiceImpl extends BaseServiceImpl implements
		NotificacionService {

	/** mail services. */
	@Autowired
	private MailServices mailServices;

	/**
	 * Notificar lista.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatarios
	 *            destinatarios
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	@Override
	public void notificar(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			String destinatario,
			boolean mostrarPiePagina
			) {
		enviarCorreo(notificacionEnum, parametros, destinatario, mostrarPiePagina);
	}

	/**
	 * Notificar.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatario
	 *            destinatario
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	@Override
	public void notificarLista(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			List<String> destinatarios,
			boolean mostrarPiePagina) {
		enviarCorreoLista(notificacionEnum, parametros, destinatarios, mostrarPiePagina);
	}
	
	
	/**
	 * Notificar lista.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatarios
	 *            destinatarios
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	@Override
	public void notificar(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			String destinatario
			) {
		enviarCorreo(notificacionEnum, parametros, destinatario, Boolean.FALSE);

	}

	/**
	 * Notificar.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatario
	 *            destinatario
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	@Override
	public void notificarLista(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			List<String> destinatarios) {
		enviarCorreoLista(notificacionEnum, parametros, destinatarios, Boolean.FALSE);
	}

	/**
	 * Enviar correo lista.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatarios
	 *            destinatarios
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	private void enviarCorreoLista(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			List<String> destinatarios,
			boolean mostrarPiePagina) {
		
		Map<String, Resource> mapaIdentificador = obtenImagenesPlantillaCorreo(mostrarPiePagina);
		
		String asuntoPlantilla = MensajeCorreoHelper
				.obtenerMensajeResorceCorreo(notificacionEnum.getAsunto());
		String asunto = remplazarParametrosSinAcentos(asuntoPlantilla, parametros);
		
		String plantillaGeneral = MensajeCorreoHelper
				.obtenerMensajeResorceCorreo(notificacionEnum.getDescripcion());

		String mensaje = HTML;
		String bodymensaje = remplazarParametros(plantillaGeneral,
																	parametros);
		if (mensaje.contains(REMPLAZO)) {
			mensaje = mensaje.replace(REMPLAZO, bodymensaje);
		}
		MailMessage mailMessage = new MailMessage(CORREO_REMITENTE,

		CORREO_REMITENTE, asunto);
		
		if (!mapaIdentificador.containsKey(CID_ENCABEZDO)) {
			mensaje = mensaje.replace(ENCABEZADO, "");
		}
		
		if (!mapaIdentificador.containsKey(CID_PIE_PAGINA)) {
			mensaje = mensaje.replace(PIE_PAGINA, "");
		}
		mailMessage.setBody(mensaje.toString());

		// Envia el correo con copia oculta
		if (destinatarios != null && !destinatarios.isEmpty()) {

			for (String correoDest : destinatarios) {
				if (correoDest != null && !correoDest.isEmpty()) {

					mailMessage.setTo(correoDest);
					mailMessage.setResourcesInLine(mapaIdentificador);
					mailServices.enviarCorreoHtml(mailMessage);

				}
			}

		}

	}

	/**
	 * Enviar correo.
	 * 
	 * @param notificacionEnum
	 *            notificacion enum
	 * @param parametros
	 *            parametros
	 * @param destinatario
	 *            destinatario
	 * @param mapaIdentificador
	 *            mapa identificador
	 */
	private void enviarCorreo(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			String destinatario,
			boolean mostrarPiePagina) {
		
		Map<String, Resource> mapaIdentificador = obtenImagenesPlantillaCorreo(mostrarPiePagina);
		// Obtinen la plantilla del asunto de propertis dependiendo de el tipo
		// de notificacion
		String asuntoPlantilla = MensajeCorreoHelper
				.obtenerMensajeResorceCorreo(notificacionEnum.getAsunto());
		// Remplaza los parametros del asunto
		String asunto = remplazarParametrosSinAcentos(asuntoPlantilla, parametros);

		String plantillaGeneral = MensajeCorreoHelper
				.obtenerMensajeResorceCorreo(notificacionEnum.getDescripcion());

		String mensaje = HTML;
		String bodymensaje = remplazarParametros(plantillaGeneral,
										parametros);
		if (mensaje.contains(REMPLAZO)) {
			mensaje = mensaje.replace(REMPLAZO, bodymensaje);
		}
		MailMessage mailMessage = new MailMessage(CORREO_REMITENTE,
		CORREO_REMITENTE, asunto);
		
		if (!mapaIdentificador.containsKey(CID_ENCABEZDO)) {
			mensaje = mensaje.replace(ENCABEZADO, "");
		}
		if (!mapaIdentificador.containsKey(CID_PIE_PAGINA)) {
			mensaje = mensaje.replace(PIE_PAGINA, "");
		}
		
		mailMessage.setBody(mensaje.toString());
		
		mailMessage.setTo(destinatario);
		mailMessage.setResourcesInLine(mapaIdentificador);
		
		mailServices.enviarCorreoHtml(mailMessage);

	}

	/**
	 * Remplazar parametros.
	 * 
	 * @author
	 * @param remplazo
	 *            el remplazo
	 * @param mapa
	 *            el mapa
	 * @return el string
	 */
	private String remplazarParametros(String remplazo,
			Map<ParametrosCorreoEnum, String> mapa) {
		StringBuffer mensaje = new StringBuffer(remplazo);
		int indexOf, longitud;
		for (ParametrosCorreoEnum opcion : mapa.keySet()) {
			String parametro = mapa.get(opcion);
			String aRemplazar = opcion.getParametro();
			// ** Remplazar acentos
			parametro = remplazarAcentos(parametro);
			indexOf = mensaje.indexOf(aRemplazar);
			while (indexOf != -1) {
				longitud = indexOf + aRemplazar.length();
				mensaje.replace(indexOf, longitud, parametro);
				indexOf = mensaje.indexOf(aRemplazar);
			}
		}
		return mensaje.toString();
	}

	/**
	 * Remplazar parametros.
	 * 
	 * @param remplazo
	 *            el remplazo
	 * @param mapa
	 *            el mapa
	 * @return el string
	 */
	private String remplazarParametrosSinAcentos(String remplazo,
			Map<ParametrosCorreoEnum, String> mapa) {
		StringBuffer mensaje = new StringBuffer(remplazo);
		int indexOf, longitud;
		for (ParametrosCorreoEnum opcion : mapa.keySet()) {
			String parametro = mapa.get(opcion);
			String aRemplazar = opcion.getParametro();
			indexOf = mensaje.indexOf(aRemplazar);
			while (indexOf != -1) {
				longitud = indexOf + aRemplazar.length();
				mensaje.replace(indexOf, longitud, parametro);
				indexOf = mensaje.indexOf(aRemplazar);
			}
		}
		return mensaje.toString();
	}

	/**
	 * Remplazar acentos por codigo html.
	 * 
	 * @param mensaje
	 *            el mensaje
	 * @return el string
	 */
	private String remplazarAcentos(String mensaje) {
		int index, indexof;
		String conAcento;
		String sinAcento;
		for (index = 0; index < CUANTOS; index++) {
			conAcento = String.valueOf(CON_ACENTO[index]);
			indexof = mensaje.indexOf(conAcento);
			if (indexof != -1) {
				// Si se encuentra el caracter con acento en la cadena.
				// Se remplaza por acento en html

				sinAcento = String.valueOf(SIN_ACENTO[index]);
				mensaje = mensaje.replaceAll(conAcento, sinAcento);

			}
		}
		return mensaje;
	}
	
	
	private Map<String, Resource> obtenImagenesPlantillaCorreo(boolean mostrarPiePagina){
		
		Map<String, Resource> mapaIdentificador = new HashMap<String, Resource>(0);
		mapaIdentificador = new HashMap<String, Resource>();		
		
		
		Resource resourceEncabezado = obtenerResource(NOMBRE_IMAGEN_ENCABEZADO);
		if(resourceEncabezado != null){
			mapaIdentificador.put(CID_ENCABEZDO,resourceEncabezado);
		}
		
		if(mostrarPiePagina){
			Resource resourcePiePagina = obtenerResource(NOMBRE_IMAGEN_PIE_PAGINA);			
			if(resourcePiePagina != null){
				mapaIdentificador.put(CID_PIE_PAGINA, resourcePiePagina );	
			}
		}
		return mapaIdentificador;
	}
	
	private String getRutaReal(){		
		String rutaReal = null;
		try {
			rutaReal = WebApplicationHelper.realPathResource("/");
		} catch(Exception exception){
			rutaReal = null;
		}		
		return rutaReal;
	}
	
	/**
	 * Metodo que buscara el recurso, recibe como parametro la imagen con base a 
	 * su folder, 
	 * 
	 * Primero busca la ruta real, si no encuentra la ruta en el fileSystem tomaria una 
	 * URL donde podria tomarse el recurso, de no existir devuelve nulo
	 * @param imagen
	 * @return
	 */
	private Resource obtenerResource(String imagen){
		Resource resource = null;
		try {
			String ruta = getRutaReal();
			if(ruta != null){
				resource = new FileSystemResource(ruta.concat(imagen));
			}else{
				ruta = MensajeHelper.obtenerPropiedadSpring(URL_BASE_IMAGENES_PROPERTY);
				resource = new UrlResource(ruta.concat(imagen));
				if(!resource.exists()){
					resource = null;	
				}
			}
		} catch (Exception e) {
			resource = null;
		}
		return resource;
	}	

}
