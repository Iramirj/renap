package mx.gob.segob.dgti.renap.comun.presentation.validator.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import mx.gob.segob.dgti.renap.comun.presentation.constants.ValidadorConstants;
import mx.gob.segob.dgti.renap.comun.presentation.validator.annotation.Alfabeto_Espacio_MX;
import mx.gob.segob.dgti.infraestructure.base.presentation.helper.MensajeHelper;


public class AlfabetoEspacioMXConstraintValidator implements
		ValidadorConstants, ConstraintValidator<Alfabeto_Espacio_MX, String> {
	private final String caracteres = MensajeHelper
			.obtenerMensajeValidacion(CARACTERES_VALIDOS_ALFABETO);

	@Override
	public void initialize(Alfabeto_Espacio_MX arg0) {

	}

	@Override
	public boolean isValid(String valor, ConstraintValidatorContext cxt) {

		String mensaje = "";

		boolean valid = true;

		if (valor != null && !valor.isEmpty()) {
			boolean cadenaValida = valor.matches("[A-Za-z\\s" + caracteres
					+ "]+");
			boolean cadenaEspacioInicio = valor.matches("^[A-Za-z" + caracteres
					+ "][A-Za-z" + caracteres + "\\s]*");
			boolean cadenaEspacioFin = valor.matches("^[A-Za-z " + caracteres
					+ "\\s]*[A-Za-z" + caracteres + "]$");

			if (!cadenaValida) {
				mensaje = MensajeHelper.obtenerMensajeValidacion(VALIDACION_SOLO_LETRAS);
			} else if (!cadenaEspacioInicio) {
				mensaje = MensajeHelper
						.obtenerMensajeValidacion(VALIDACION_ESPACIOS_INICIO);

			} else if (!cadenaEspacioFin) {
				mensaje = MensajeHelper.obtenerMensajeValidacion(VALIDACION_ESPACIOS_FIN);

			}

		}

		if (!mensaje.isEmpty()) {
			valid = false;
			cxt.disableDefaultConstraintViolation();
			cxt.buildConstraintViolationWithTemplate(mensaje)
					.addConstraintViolation();
		}
		return valid;
	}

}
