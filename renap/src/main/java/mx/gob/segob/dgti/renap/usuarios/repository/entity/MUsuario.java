package mx.gob.segob.dgti.renap.usuarios.repository.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="M_Usuario")
public class MUsuario implements Serializable {
	
	
	
	/****/
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_M_USUARIO")
	private Integer idMUsuario;
	
	@Column(name="USUARIO")
	private String usuario;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="LOGUEADO")
	private String logueado;
	
	@Column(name="ULTIMO_ACCESO")
	private Date ultimoAceso;
	
	@Column(name="NUMERO_INTENTOS")
	private Integer numeroIntentos;
	
	@Column(name="BLOQUEADO")
	private String bloqueado;
	
	@Column(name="FECHA_BLOQUEO")
	private Date fechaBloqueo;
	
	@Column(name="PRIMERA_VEZ")
	private String primeraVez;
	
	@Column(name="TIPO_NAC")
	private String tipoNac;
	
	@Column(name="ID_USUARIO_ALTA")
	private Integer idUsuarioAlta;
	
	@Column(name="ID_USUARIO_MOD")
	private Integer idUsuarioMod;
	
	@Column(name="ID_USUARIO_BAJA")
	private Integer idUsuarioBaja;
	
	@Column(name="FECHA_ALTA")
	private Date fechaAlta;
	
	@Column(name="FECHA_MOD")
	private Date fechaMod;
	
	@Column(name="FECHA_BAJA")
	private Date fechaBaja;
	
	@Column(name="ST_REGISTRO")
	private String stRegistro;

	public Integer getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getLogueado() {
		return logueado;
	}

	public void setLogueado(String logueado) {
		this.logueado = logueado;
	}

	public Date getUltimoAceso() {
		return ultimoAceso;
	}

	public void setUltimoAceso(Date ultimoAceso) {
		this.ultimoAceso = ultimoAceso;
	}

	public Integer getNumeroIntentos() {
		return numeroIntentos;
	}

	public void setNumeroIntentos(Integer numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Date getFechaBloqueo() {
		return fechaBloqueo;
	}

	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}

	public String getPrimeraVez() {
		return primeraVez;
	}

	public void setPrimeraVez(String primeraVez) {
		this.primeraVez = primeraVez;
	}

	public String getTipoNac() {
		return tipoNac;
	}

	public void setTipoNac(String tipoNac) {
		this.tipoNac = tipoNac;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}	
}
