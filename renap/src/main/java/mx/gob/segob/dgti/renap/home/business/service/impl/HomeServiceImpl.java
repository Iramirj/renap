package mx.gob.segob.dgti.renap.home.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.home.business.service.HomeService;
import mx.gob.segob.dgti.renap.home.business.vo.UserVo;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Service
public class HomeServiceImpl implements HomeService{
	
	@Autowired
	private HomeDao flHomeDao;
	
	@Override
	@Transactional
	public Usuario getDetalleById(Integer flIdUsuario){
		return flHomeDao.getDetalleById(flIdUsuario);
	}
	
	@Override
	@Transactional
	public Boolean changePasswordById(Usuario objUsuario){
		return flHomeDao.changePasswordById(objUsuario);
	}
}
