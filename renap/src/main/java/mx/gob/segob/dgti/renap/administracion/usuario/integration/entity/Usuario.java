package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "m_usuario")
public class Usuario implements java.io.Serializable {

	private static final long serialVersionUID = -1135401759275997572L;
	private Integer idUsuario;
	private String usuario;
	private String password;
	private String estatus;
	private String logueado;
	private Date ultimoAcceso;
	private int numeroIntentos;
	private String bloqueado;
	private Date fechaBloqueo;
	private String primeraVez;
	private String tipoNacionalidad;
	private Integer idUsuarioAlta;
	private Date fechaAlta;
	private Integer idUsuarioMod;
	private Date fechaMod;
	private Integer idUsuarioBaja;
	private Date fechaBaja;
	private String stRegistro;
	private List<Perfil> listaPerfiles;
	private List<UsuarioPermiso> listaUsuarioPermiso;
	private UsuarioDatos usuarioDatos;

	public Usuario() {
	}
	
	public Usuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Id
	@Column(name = "id_m_usuario", unique = true, nullable = false, length = 64)
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Column(name = "usuario", nullable = false)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "password", nullable = false)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "estatus", nullable = false)
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Column(name = "logueado", nullable = false)
	public String getLogueado() {
		return this.logueado;
	}

	public void setLogueado(String logueado) {
		this.logueado = logueado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ultimo_acceso", length = 19)
	public Date getUltimoAcceso() {
		return this.ultimoAcceso;
	}

	public void setUltimoAcceso(Date ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}

	@Column(name = "numero_intentos", nullable = false)
	public int getNumeroIntentos() {
		return this.numeroIntentos;
	}

	public void setNumeroIntentos(int numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	@Column(name = "bloqueado", nullable = false)
	public String getBloqueado() {
		return this.bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_bloqueo", length = 19)
	
	public Date getFechaBloqueo() {
		return this.fechaBloqueo;
	}

	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}

	@Column(name = "primera_vez", nullable = false)
	public String getPrimeraVez() {
		return primeraVez;
	}

	public void setPrimeraVez(String primeraVez) {
		this.primeraVez = primeraVez;
	}

	@Column(name = "tipo_nac", nullable = false)
	public String getTipoNacionalidad() {
		return tipoNacionalidad;
	}

	public void setTipoNacionalidad(String tipoNacionalidad) {
		this.tipoNacionalidad = tipoNacionalidad;
	}

	@Column(name = "id_usuario_alta", nullable = false)
	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_alta", length = 19)
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Column(name = "id_Usuario_Mod", nullable = false)
	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_mod", length = 19)
	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	@Column(name = "id_Usuario_Baja", nullable = false)
	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_baja", length = 19)
	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	@Column(name = "st_registro", nullable = false)
	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "d_usuario_perfil", joinColumns = { @JoinColumn(name = "id_m_usuario", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "id_c_perfil", nullable = false, updatable = false) })
	public List<Perfil> getListaPerfiles() {
		return listaPerfiles;
	}

	public void setListaPerfiles(List<Perfil> listaPerfiles) {
		this.listaPerfiles = listaPerfiles;
	}

	
	@OneToOne(mappedBy = "idMUsuario", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	public UsuarioDatos getUsuarioDatos() {
		return usuarioDatos;
	}
	
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="usuarioPermisoPK.usuario", cascade=CascadeType.ALL)
	public List<UsuarioPermiso> getListaUsuarioPermiso() {
		return listaUsuarioPermiso;
	}

	public void setListaUsuarioPermiso(List<UsuarioPermiso> listaUsuarioPermiso) {
		this.listaUsuarioPermiso = listaUsuarioPermiso;
	}

	/**
	 * @param usuarioDatos
	 *            the usuarioDatos to set
	 */
	public void setUsuarioDatos(UsuarioDatos usuarioDatos) {
		this.usuarioDatos = usuarioDatos;
	}

}
