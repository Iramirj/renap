package mx.gob.segob.dgti.infraestructure.auditoria.business.builder;



import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.auditoria.vo.BitacoraVO;

public class BitacoraBuilder {
	
	public BitacoraSistema convertirVOaEntidad(BitacoraVO vo ){
		BitacoraSistema entidad = null;
		if(vo != null){
			entidad = new BitacoraSistema();
			entidad.setIdHBitacora(vo.getIdHBitacora());
			entidad.setFecha(vo.getFecha());
			entidad.setHora(vo.getHora());
			entidad.setIdMUsuario(vo.getIdMUsuario());
			entidad.setModulo(vo.getModulo());
		}
		return entidad;
	}
}
