package mx.gob.segob.dgti.infraestructure.base.business.vo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mx.gob.segob.dgti.infraestructure.base.business.constants.EstatusRequestEnum;



public class MensajeRespuestaVO {

	private EstatusRequestEnum estatus;
	private Set<String> erroresGlobales;
	private Map<String, Set<String>> erroresCampo;
	private Map<String, String> errores;
	private String mensaje;
	
	public MensajeRespuestaVO() {
		erroresGlobales = new HashSet<String>(0);
		erroresCampo = new HashMap<String, Set<String>>(0);
	}
	
	public MensajeRespuestaVO(EstatusRequestEnum estatus) {
		this.estatus = estatus;
		erroresGlobales = new HashSet<String>(0);
		erroresCampo = new HashMap<String, Set<String>>(0);
	}
	
	public void setEstatus(EstatusRequestEnum estatus){
		this.estatus = estatus;
	}
	
	public void addErrorGlobal(String error){
		erroresGlobales.add(error);
	}
	
	public void addErrorCampo(String campo, String error){
		Set<String> errorCampo = erroresCampo.get(campo);
		if(errorCampo == null){
			errorCampo = new HashSet<String>(0);			
		}
		errorCampo.add(error);
		erroresCampo.put(campo, errorCampo);
	}
	
	public EstatusRequestEnum getEstatus(){
		return this.estatus;
	}
	
	public String[] getErroresGlobales(){
		return erroresGlobales.toArray(new String[0]);		
	}
	
	public Map<String, Set<String>> getErroresCampo(){
		return erroresCampo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Map<String, String> getErrores() {
		return errores;
	}

	public void setErrores(Map<String, String> errores) {
		this.errores = errores;
	}
	public void addAllErrors(MensajeRespuestaVO respuesta){
		if(respuesta != null){estatus = respuesta.getEstatus();
			for(String error : respuesta.getErroresGlobales()){erroresGlobales.add(error);	
			}							
			erroresCampo.putAll(respuesta.getErroresCampo());
		}		
	}
	public boolean hasErrors(){
		if(erroresGlobales.size() != 0 || erroresCampo.size() != 0){
			return true;	
		} else {
			return false;
		}
		 
	}
	
}
