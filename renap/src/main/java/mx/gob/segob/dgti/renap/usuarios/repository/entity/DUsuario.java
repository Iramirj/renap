package mx.gob.segob.dgti.renap.usuarios.repository.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="D_Usuario")
public class DUsuario implements Serializable{

	/****/
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_D_USUARIO")
	private Integer idDUsuario;
	
	@Column(name="ID_M_USUARIO")
	private Integer idMUsuario;
	
	@Column(name="ID_C_CARGO")
	private Integer idCCargo;
	
	@Column(name="ID_C_TITULO")
	private Integer idCTitulo;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="A_PATERNO")
	private String aPaterno;
	
	@Column(name="A_MATERNO")
	private String aMaterno;
	
	@Column(name="CURP")
	private String crup;
	
	@Column(name="CORREO")
	private String correo;
	
	@Column(name="ID_USUARIO_ALTA")
	private Integer idUsuarioAlta;
	
	@Column(name="ID_USUARIO_MOD")
	private Integer idUsuarioMod;
	
	@Column(name="ID_USUARIO_BAJA")
	private Integer idUsuarioBaja;
	
	@Column(name="FECHA_ALTA")
	private Date fechaAlta;
	
	@Column(name="FECHA_MOD")
	private Date fechaMod;
	
	@Column(name="FECHA_BAJA")
	private Date fechaBaja;
	
	@Column(name="ST_REGISTRO")
	private String stRegistro;

	public Integer getIdDUsuario() {
		return idDUsuario;
	}

	public void setIdDUsuario(Integer idDUsuario) {
		this.idDUsuario = idDUsuario;
	}

	public Integer getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public Integer getIdCCargo() {
		return idCCargo;
	}

	public void setIdCCargo(Integer idCCargo) {
		this.idCCargo = idCCargo;
	}

	public Integer getIdCTitulo() {
		return idCTitulo;
	}

	public void setIdCTitulo(Integer idCTitulo) {
		this.idCTitulo = idCTitulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getaPaterno() {
		return aPaterno;
	}

	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}

	public String getaMaterno() {
		return aMaterno;
	}

	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}

	public String getCrup() {
		return crup;
	}

	public void setCrup(String crup) {
		this.crup = crup;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

}