package mx.gob.segob.dgti.renap.bitacora.presentation.controller;

import java.io.IOException;
import java.util.List;
import mx.gob.segob.dgti.renap.administracion.usuario.business.service.UsuarioService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.bitacora.business.services.BitacoraService;
import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;
import mx.gob.segob.dgti.renap.catalogos.business.service.CatalogosService;
import mx.gob.segob.dgti.renap.catalogos.business.service.UbicacionService;
import mx.gob.segob.dgti.renap.comun.presentation.constants.NavegacionConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class BitacoraController {

	@Autowired
	private BitacoraService bitacoraService;
	@Autowired
	private UbicacionService flUbicacionService;
	@Autowired
	private CatalogosService flCatalogosService;
	@Autowired
	private UsuarioService usuarioService;
	
	public static final ObjectMapper flJsonMapper = new ObjectMapper();

	@RequestMapping ("/modules/bitacora/inicial/{idUsuario}")
	public String inicial(ModelMap model, @PathVariable("idUsuario") Integer idUsuario){
		System.out.println(idUsuario);
		Integer idperfilUsuario = usuarioService.getPerfilByIdUsuario(idUsuario);
		BitacoraVO flBitacoraVO = new BitacoraVO();
		flBitacoraVO.setIdUsuarioAlta(1);
		model.addAttribute("lstUsuarios", usuarioService.getListUserByIdUserLogeo(idUsuario));
		model.addAttribute("lstEntidad", flUbicacionService.getListByIdPais(1));
		model.addAttribute("lstTipoAccion", flCatalogosService.getListTipoAccion());
		model.addAttribute("lstPerfil", flCatalogosService.getLstPerfByIdPerf(idperfilUsuario, "B"));
		model.addAttribute("lstBitacora", bitacoraService.consultarBitacora(flBitacoraVO));
		return NavegacionConstants.BITACORA;
	}

	@RequestMapping("/modules/bitacora/insertar")
	public String insertarBitacora(BitacoraVO bitacora) {
		bitacoraService.insertarBitacora(bitacora);
		return null;
	}

	@RequestMapping(value = "/modules/bitacora/getListFilter", method = RequestMethod.POST)
	public @ResponseBody
	String getListFilter(@RequestBody String flDatos)
			throws JsonParseException, JsonMappingException, IOException {
		String resultJson = null;
		
		BitacoraVO flBitacoraVO = flJsonMapper.readValue(flDatos, BitacoraVO.class);
		List <BitacoraVO> result = bitacoraService.consultarBitacora(flBitacoraVO);
		
		if (result != null) {
			resultJson = flJsonMapper.writeValueAsString(result);
		}
		return resultJson;
	}

	@RequestMapping("/modules/bitacora/detalle/{idRegistro}")
	public String detalleRegsitro(Model model, @PathVariable("idRegistro") Integer idRegistro){
		BitacoraVO flBitacoraVO = new BitacoraVO();
		flBitacoraVO.setIdHBitacora(idRegistro);
		List<BitacoraVO> lBitacora = bitacoraService.consultarBitacora(flBitacoraVO);
		
		BitacoraVO registro = new BitacoraVO();
		
		for (BitacoraVO item : lBitacora) {
			registro = item;
		}
		
		model.addAttribute("bitacora", registro );
		return NavegacionConstants.DETALLE_REGISTRO_BITACORA;
	}
}
