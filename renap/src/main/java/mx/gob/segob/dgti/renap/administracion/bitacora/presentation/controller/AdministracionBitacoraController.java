package mx.gob.segob.dgti.renap.administracion.bitacora.presentation.controller;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.service.AdministracionBitacoraService;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.ConsultaBitacoraVO;
import mx.gob.segob.dgti.renap.comun.presentation.constants.NavegacionConstants;
import mx.gob.segob.dgti.infraestructure.base.business.constants.EstatusRequestEnum;
import mx.gob.segob.dgti.infraestructure.base.business.exception.BusinessException;
import mx.gob.segob.dgti.infraestructure.base.business.vo.DataTableResult;
import mx.gob.segob.dgti.infraestructure.base.business.vo.DataTableSearch;
import mx.gob.segob.dgti.infraestructure.base.business.vo.MensajeRespuestaVO;
import mx.gob.segob.dgti.infraestructure.base.presentation.controller.BaseController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("administracion/bitacora")
public class AdministracionBitacoraController extends BaseController{
	@Autowired
	private AdministracionBitacoraService bitacoraService;

	@RequestMapping
	public String principal() {
		return NavegacionConstants.ADMINISTRACION_BITACORA;
	}

	@RequestMapping(value="buscar", method = RequestMethod.POST)
	public @ResponseBody
		ResponseEntity<DataTableResult<ConsultaBitacoraVO>> buscar(
				@ModelAttribute BusquedaBitacoraVO busqueda,
				@ModelAttribute DataTableSearch parametrosBusqueda) {
		ResponseEntity<DataTableResult<ConsultaBitacoraVO>> response = null;
		DataTableResult<ConsultaBitacoraVO> resultados = null;
		List<ConsultaBitacoraVO> dataList = null;
		Integer totalResultados = 0;

		try {
			totalResultados = bitacoraService.obtenerTotalBitacoras(busqueda);
			dataList = bitacoraService.obtenerBitacoras(busqueda,
											parametrosBusqueda.getStart(),
											parametrosBusqueda.getLength());

			resultados = new DataTableResult<ConsultaBitacoraVO>(parametrosBusqueda.getDraw());
			resultados.setRecordsTotal(totalResultados);
			resultados.setRecordsFiltered(totalResultados);
			resultados.setData(dataList.toArray(new ConsultaBitacoraVO[0]));

			response = new ResponseEntity<DataTableResult<ConsultaBitacoraVO>>(resultados, HttpStatus.OK);
		} catch (BusinessException e) {
			logger.error(e.getMessage(),e);
			response = new ResponseEntity<DataTableResult<ConsultaBitacoraVO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	

	@RequestMapping(value="validarParametroBusqueda", method = RequestMethod.POST)
	public @ResponseBody
		ResponseEntity<MensajeRespuestaVO> validarParametroBusqueda(
			@ModelAttribute BusquedaBitacoraVO busqueda ){
		
		ResponseEntity<MensajeRespuestaVO> response = null;
		MensajeRespuestaVO respuesta = null;
		
		
		respuesta = validar(busqueda);
		if(respuesta.hasErrors()){
			response = new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			respuesta = new MensajeRespuestaVO(EstatusRequestEnum.SUCESSFUL);
			response = new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.OK);
		}	
		
		return response;		
	}

}
