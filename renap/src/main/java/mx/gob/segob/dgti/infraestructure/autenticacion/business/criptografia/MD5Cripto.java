package mx.gob.segob.dgti.infraestructure.autenticacion.business.criptografia;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MD5Cripto implements Criptografia {
	
	private static final String DIGEST = "MD5";
	
	public String hash(String message) throws NoSuchAlgorithmException{
		MessageDigest m = MessageDigest.getInstance(DIGEST);
		
		m.reset();
		m.update(message.getBytes());
		byte[] digest = m.digest();
		
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
 
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}

		return hashtext;
	}
}
