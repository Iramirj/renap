package mx.gob.segob.dgti.renap.administracion.usuario.business.builder;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PermisoVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Permiso;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class PermisoBuilder extends BaseBuilder<PermisoVO,Permiso> {

	
	@Override
	public Permiso convertirVOAEntidad(PermisoVO vo) {
		Permiso entidad=null;
		if(vo!=null){
			entidad=new Permiso();
			entidad.setDescripcion(vo.getDescripcion());
			entidad.setIdPermiso(vo.getIdPermiso());
			
		}
		return entidad;
	}

	@Override
	public PermisoVO convertirEntidadAVO(Permiso entidad) {
		PermisoVO vo=null;
		if(entidad!=null){
			vo=new PermisoVO();
			vo.setDescripcion(vo.getDescripcion());
			vo.setIdPermiso(entidad.getIdPermiso());
			
		}
		return vo;
	}

}
