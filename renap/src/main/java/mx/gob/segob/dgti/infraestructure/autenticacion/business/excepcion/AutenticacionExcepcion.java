package mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.AutenticacionMotivoException;

public class AutenticacionExcepcion extends Exception {

	/***/
	private static final long serialVersionUID = 1L;
	
	private AutenticacionMotivoException motivo;
	
	public AutenticacionExcepcion(AutenticacionMotivoException motivo){
		super(motivo.getCodigoMensaje());
	}
	
	public AutenticacionExcepcion(AutenticacionMotivoException motivo, Exception cause){
		super(motivo.getCodigoMensaje(), cause);		
	}
	
	public AutenticacionMotivoException getMotivo(){
		return motivo;
	}
	
}
