package mx.gob.segob.dgti.renap.administracion.usuario.business.builder;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioDatosVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.UsuarioDatos;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class UsuarioDatosBuilder extends
		BaseBuilder<UsuarioDatosVO, UsuarioDatos> {

	@Override
	public UsuarioDatos convertirVOAEntidad(UsuarioDatosVO vo) {
		UsuarioDatos entidad = null;
		if (vo != null) {
			entidad = new UsuarioDatos();
			entidad.setaMaterno(vo.getaMaterno());
			entidad.setaPaterno(vo.getaPaterno());
			entidad.setCorreo(vo.getCorreo());
			entidad.setNombre(vo.getNombre());
			if (vo.getIdDUsuario() != null) {
				entidad.setIdDUsuario(vo.getIdDUsuario());
			}

		}
		return entidad;
	}

	@Override
	public UsuarioDatosVO convertirEntidadAVO(UsuarioDatos entidad) {
		UsuarioDatosVO vo = null;
		String nombreCompleto = "";
		if (entidad != null) {
			vo = new UsuarioDatosVO();
			if (entidad.getNombre() != null) {
				vo.setNombre(entidad.getNombre());
				nombreCompleto += entidad.getNombre();
			}
			if (entidad.getaPaterno() != null) {
				vo.setaPaterno(entidad.getaPaterno());
				nombreCompleto += " " + entidad.getaPaterno();
			}
			if (entidad.getaMaterno() != null) {
				vo.setaMaterno(entidad.getaMaterno());
				nombreCompleto += " " + entidad.getaMaterno();
			}
			vo.setCorreo(entidad.getCorreo());
			vo.setIdDUsuario(entidad.getIdDUsuario());
			vo.setNombreCompleto(nombreCompleto);
		}

		return vo;
	}

}
