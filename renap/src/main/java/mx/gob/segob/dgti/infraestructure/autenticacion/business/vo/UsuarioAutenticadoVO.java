package mx.gob.segob.dgti.infraestructure.autenticacion.business.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class UsuarioAutenticadoVO implements Serializable {

	private static final long serialVersionUID = 7353394670798954048L;

	private Integer idUsuario;
	private String usuario;
	private String password;
	private String stRegistro;
	private String bloqueado;
	private String inSession;
	private Date ultimoAcceso;
	private Date fechaBloqueo;
	private Integer numeroIntentos;
	private List<String> permisos;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

	public String isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String isInSession() {
		return inSession;
	}

	public void setInSession(String inSession) {
		this.inSession = inSession;
	}

	public Date getUltimoAcceso() {
		return ultimoAcceso;
	}

	public void setUltimoAcceso(Date ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}

	public List<String> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<String> permisos) {
		this.permisos = permisos;
	}

	public Date getFechaBloqueo() {
		return fechaBloqueo;
	}

	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}

	public Integer getNumeroIntentos() {
		return numeroIntentos;
	}

	public void setNumeroIntentos(Integer numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	public String toString() {
		return usuario;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public String getInSession() {
		return inSession;
	}
}
