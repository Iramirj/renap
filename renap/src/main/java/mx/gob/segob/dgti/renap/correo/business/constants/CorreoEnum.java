package mx.gob.segob.dgti.renap.correo.business.constants;

public enum CorreoEnum {

	/** Notificacion para crear nuevo uuario */
	NUEVO_USUARIO("USUARIO_NUEVO_ASUNTO",
			"USUARIO_NUEVO_MENSAJE"),

	/** Notificacion al reiniciar contrasenia */
	REINICIO_CONTRASENIA_USUARIO("REINICIO_CONTRASENIA_ASUNTO",
			"REINICIO_CONTRASENIA_MENSAJE");
	
	/** asunto. */
	private String asunto;

	/** descripcion. */
	private String descripcion;

	/**
	 * el constructor del enum.
	 * 
	 * @param asunto
	 *            asunto
	 * @param descripcion
	 *            descripcion
	 */
	private CorreoEnum(String asunto, String descripcion) {
		this.setAsunto(asunto);
		this.setDescripcion(descripcion);
	}

	/**
	 * obtiene la descripcion.
	 * 
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * establece la descripcion.
	 * 
	 * @param descripcion
	 *            el nuevo descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * obtiene el asunto.
	 * 
	 * @return asunto
	 */
	public String getAsunto() {
		return asunto;
	}

	/**
	 * establece el asunto.
	 * 
	 * @param asunto
	 *            el nuevo asunto
	 */
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
}
