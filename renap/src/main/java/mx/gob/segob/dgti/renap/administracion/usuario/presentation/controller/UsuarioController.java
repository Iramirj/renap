package mx.gob.segob.dgti.renap.administracion.usuario.presentation.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import mx.gob.segob.dgti.renap.administracion.usuario.business.constants.UsuarioEstatusEnum;
import mx.gob.segob.dgti.renap.administracion.usuario.business.service.PerfilService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.service.UsuarioService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioDatosVO;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.comun.business.constants.EstatusEnum;
import mx.gob.segob.dgti.renap.comun.presentation.constants.MensajesValidaciones;
import mx.gob.segob.dgti.renap.comun.presentation.constants.NavegacionConstants;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.Busqueda;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.GuardarUsuario;
import mx.gob.segob.dgti.infraestructure.base.business.constants.EstatusRequestEnum;
import mx.gob.segob.dgti.infraestructure.base.business.vo.DataTableResult;
import mx.gob.segob.dgti.infraestructure.base.business.vo.DataTableSearch;
import mx.gob.segob.dgti.infraestructure.base.business.vo.MensajeRespuestaVO;
import mx.gob.segob.dgti.infraestructure.base.presentation.controller.BaseController;
import mx.gob.segob.dgti.infraestructure.base.presentation.helper.MensajeHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("modules/administracion/usuarios/consultaUsuarios")
public class UsuarioController extends BaseController implements
		BusquedaController<UsuarioVO, DataTableSearch>,
		NavegacionConstants,MensajesValidaciones {

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private PerfilService perfilService;
	

	@RequestMapping
	public String principal() {
		return ADMINISTRACION_USUARIO;
	}



	@Override
	@RequestMapping(value = "buscar", method = RequestMethod.POST)
	public @ResponseBody
	DataTableResult<UsuarioVO> buscar(@ModelAttribute UsuarioVO busqueda,
			@ModelAttribute DataTableSearch parametrosBusqueda) {
		DataTableResult<UsuarioVO> resultados = new DataTableResult<UsuarioVO>(
				parametrosBusqueda.getDraw());

		Integer totalResultados = usuarioService.buscarTotal(busqueda);
		List<UsuarioVO> dataList = usuarioService.buscar(busqueda,
				parametrosBusqueda.getStart(), parametrosBusqueda.getLength());

		resultados.setRecordsTotal(totalResultados);
		resultados.setRecordsFiltered(totalResultados);
		resultados.setData(dataList.toArray(new UsuarioVO[0]));
		return resultados;
	}

	

	@RequestMapping(value = "cambiarEstatus", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<MensajeRespuestaVO> cambiarEstatus(
			@Validated @ModelAttribute UsuarioVO usuarioVO,
			BindingResult resultado) {
		MensajeRespuestaVO respuesta = new MensajeRespuestaVO();


			try {
				UsuarioVO usuario = usuarioService.obtenerPorId(usuarioVO
						.getUsuario());
				if (usuario.getStRegistro().equals("I")) {
					usuario.setStRegistro("A");
				} else if (usuario.getStRegistro().equals("A")) {
					usuario.setStRegistro("I");;
				}

				usuarioService.guardar(usuario);
				respuesta.setEstatus(EstatusRequestEnum.SUCESSFUL);
				respuesta.setMensaje(MensajeHelper.obtenerMensaje(MENSAJE_CAMBIAR_ESTATUS_EXITO));
			} catch (Exception exception) {
				respuesta.setEstatus(EstatusRequestEnum.ERROR);
				respuesta.addErrorGlobal(exception.getMessage());
			}


		return new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.OK);

	}

	@RequestMapping(value = "guardar", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<MensajeRespuestaVO> guardar(
			@ModelAttribute UsuarioVO usuarioVO, BindingResult resultado) {

		MensajeRespuestaVO respuesta = new MensajeRespuestaVO();
		respuesta= validar(usuarioVO,GuardarUsuario.class);
		if (respuesta.hasErrors()) {
			return new ResponseEntity<MensajeRespuestaVO>(respuesta,
					HttpStatus.BAD_REQUEST);
		} else {


			if (usuarioVO.getIdUsuario() == null &&  usuarioService.existe(usuarioVO)
					&& !(usuarioVO.getStRegistro().equals("I"))) {
				respuesta.setEstatus(EstatusRequestEnum.ERROR);
				respuesta.addErrorGlobal(USUARIO_EXISTE);
				return new ResponseEntity<MensajeRespuestaVO>(respuesta,
						HttpStatus.BAD_REQUEST);
			} else {
				try {

					if (usuarioVO.getIdUsuario() !=null) {
						usuarioService.editar(usuarioVO);
						respuesta.setEstatus(EstatusRequestEnum.SUCESSFUL);
					} else {
						usuarioVO.setStRegistro("A");
						usuarioService.guardarNuevo(usuarioVO);
						respuesta.setEstatus(EstatusRequestEnum.SUCESSFUL);
					}
				} catch (Exception exception) {
					return new ResponseEntity<MensajeRespuestaVO>(respuesta,
							HttpStatus.BAD_REQUEST);
				}
			}
		}

		return new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.OK);

	}

	@Override
	@RequestMapping(value = "validar", method = RequestMethod.POST)
	public ResponseEntity<MensajeRespuestaVO> validar(
			@ModelAttribute UsuarioVO usuarioVO) {
		MensajeRespuestaVO respuesta = new MensajeRespuestaVO();

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<UsuarioVO>> constraintViolations = validator
				.validate(usuarioVO, Busqueda.class);

		Map<String, String> errores = obtenerMensajes(constraintViolations);
		if (errores.size() > 0) {

			return regresarErrores(errores, respuesta);
		}
		return new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.OK);
	}



	@RequestMapping(value = "reiniciarContrasenia", method = RequestMethod.POST)
	public ResponseEntity<MensajeRespuestaVO> reiniciarPassword(
			@ModelAttribute UsuarioVO usuarioVO) {
		MensajeRespuestaVO mensajeRespuesta = new MensajeRespuestaVO();
		if (usuarioVO.getIdUsuario() != null) {

			UsuarioVO usuario = usuarioService.obtenerPorId(usuarioVO
					.getUsuario());

			if (usuario != null) {

				try {
					usuarioService.reiniciarContrasena(usuario);
					mensajeRespuesta.setMensaje(MensajeHelper.obtenerMensaje(MENSAJE_REINICIAR_CONTRASENA_EXITO));
					return new ResponseEntity<MensajeRespuestaVO>(
							mensajeRespuesta, HttpStatus.OK);

				} catch (Exception exception) {
					mensajeRespuesta.setEstatus(EstatusRequestEnum.ERROR);
					return new ResponseEntity<MensajeRespuestaVO>(
							mensajeRespuesta, HttpStatus.BAD_REQUEST);
				}
			}

		}

		return new ResponseEntity<MensajeRespuestaVO>(mensajeRespuesta,
				HttpStatus.BAD_REQUEST);

	}

	@RequestMapping(value = "desbloquear", method = RequestMethod.POST)
	public ResponseEntity<MensajeRespuestaVO> desbloquear(
			@ModelAttribute UsuarioVO usuarioVO) {
		MensajeRespuestaVO respuesta = new MensajeRespuestaVO();

		if (usuarioVO != null && usuarioVO.getIdUsuario() != null) {

			UsuarioVO usuario = usuarioService.obtenerPorId(usuarioVO
					.getUsuario());
			if (usuario.getBloqueado().equals("B")) {
				usuario.setBloqueado("A");

				usuarioService.guardar(usuario);
				respuesta.setMensaje(MensajeHelper.obtenerMensaje(MENSAJE_DESBLOQUEAR_EXITO));
			}
		}
		return new ResponseEntity<MensajeRespuestaVO>(respuesta, HttpStatus.OK);
	}

	@RequestMapping(value = "registroUsuario" , method = RequestMethod.POST)
	public String registrarUsuario(@RequestParam(value = "idUsuario", required=false) String idUsuario,ModelMap model){
		UsuarioVO user=null;
		if(idUsuario==null){
			user=new UsuarioVO();
			user.setUsuarioDatosVO(new UsuarioDatosVO());
			model.put("usuario", user);
		}
		else{
		user = usuarioService.obtenerPorId(idUsuario);
		
		model.put("usuario", user);

		}
		return ALTA_EDICION_USUARIO;
	}

	
	@ModelAttribute("listaCatalogoPerfiles")
	public List<PerfilVO> obtenerPerfiles() {
		List<PerfilVO> listaPerfiles = null;
		PerfilVO perfil=new PerfilVO();
		perfil.setEstatus(EstatusEnum.ACTIVO);
		listaPerfiles = perfilService.buscarPerfil(perfil);
		return listaPerfiles;
	}

	@ModelAttribute("listaEstatusUsuario")
	public List<UsuarioEstatusEnum> obtenerListaEstatus() {
		List<UsuarioEstatusEnum> listaEstatus = new ArrayList<UsuarioEstatusEnum>();
		listaEstatus.add(UsuarioEstatusEnum.ACTIVO);
		listaEstatus.add(UsuarioEstatusEnum.INACTIVO);
		return listaEstatus;
	}

	

}
