package mx.gob.segob.dgti.infraestructure.base.presentation.security.csrf.requestDataValueProcessor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.servlet.support.RequestDataValueProcessor;
import mx.gob.segob.dgti.infraestructure.base.presentation.security.csrf.manager.CSRFTokenManager;

/**
 * Clase para integrar thymeleaf con la protecci�n de CSRF de spring, agrega el hidden necesario a cada petici�n de th:action con el token.
 * @author Praxis
 *
 */
public class CSRFRequestDataValueProcessor implements RequestDataValueProcessor {

	public Map<String, String> getExtraHiddenFields(HttpServletRequest request) {
		Map<String, String> hiddenFields = new HashMap<String, String>();

		CsrfToken token = CSRFTokenManager.getTokenForSession(request);
		hiddenFields.put(token.getParameterName(),token.getToken());
		return hiddenFields;
	}

	public String processAction(HttpServletRequest request, String action) {
		return action;
	}

	public String processFormFieldValue(HttpServletRequest request,
			String name, String value, String type) {
		return value;
	}

	public String processUrl(HttpServletRequest request, String url) {
		return url;
	}

}
