
package mx.gob.segob.dgti.renap.comun.business.constants;

import mx.gob.segob.dgti.renap.comun.presentation.serializer.EnumSerializer;
import mx.gob.segob.dgti.infraestructure.base.presentation.converter.EnumConverter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;




@JsonSerialize(using=EnumSerializer.class)
public enum PermisoEnum implements EnumConverter{
	
	//Administracion
	
		ADMINISTRADOR(1,"as");

		private Integer id;
		private String modulo;
		
		private PermisoEnum(Integer id, String modulo){
			this.id=id;
			this.modulo=modulo;
			
		}
		
		public Integer getId() {
			return id;
		}


		public void setId(Integer id) {
			this.id = id;
		}
	
		public String getModulo() {
			return modulo;
		}


		public void setModulo(String modulo) {
			this.modulo = modulo;
		}

		@Override
		public String getDescripcionConverter() {
			return modulo;
		}


		


		
	}
