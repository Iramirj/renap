package mx.gob.segob.dgti.renap.administracion.usuario.business.exception;

import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;
import mx.gob.segob.dgti.infraestructure.base.business.exception.BusinessException;




public class AdministracionException extends BusinessException {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 5995832614031762681L;

	/** La constante CATEGORY. */
	private static final String CATEGORY = "administracion";
	
	public AdministracionException(ExcepcionCausaEnum situacion, Throwable cause) {
		super(CATEGORY, situacion, cause);			
	}
	
	public AdministracionException(String modulo,ExcepcionCausaEnum situacion, Throwable cause) {
		super(modulo, situacion, cause);			
	}

}
