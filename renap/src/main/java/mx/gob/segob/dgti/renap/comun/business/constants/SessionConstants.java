package mx.gob.segob.dgti.renap.comun.business.constants;

public interface SessionConstants {
	String USUARIO = "USUARIO";
	String USUARIO_SESSION_ID = "USUARIO_SESSION_ID";
	String USUARIO_ID = "USUARIO_ID";
	String NOMBRE_COMPLETO_USUARIO = "NOMBRE_COMPLETO";
	String PRIMERA_VEZ = "PRIMERA_VEZ";
	String TIPO_NAC = "TIPO_NAC";
	String NOMBRE_DEPENDECIA = "NOMBRE_DEPENDECIA";
}
