package mx.gob.segob.dgti.renap.administracion.bitacora.business.vo;


public class ConsultaBitacoraVO {
	private Integer idHBitacora;
	private Integer idMUsuario;
	private String fecha;
	private String accion;
	private String modulo;

	public Integer getIdHBitacora() {
		return idHBitacora;
	}

	public void setIdHBitacora(Integer idHBitacora) {
		this.idHBitacora = idHBitacora;
	}

	public Integer getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

}
