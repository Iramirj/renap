package mx.gob.segob.dgti.renap.administracion.usuario.business.vo;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.gob.segob.dgti.renap.comun.presentation.constants.MensajesValidaciones;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.GuardarUsuario;

import org.hibernate.validator.constraints.NotEmpty;

public class UsuarioVO implements MensajesValidaciones {

	private Integer idUsuario;
	private String usuario;
	private String password;
	private String nombreUsuario;
	private String estatus;
	private String logueado;
	private Date ultimoAcceso;
	private int numeroIntentos;
	private String bloqueado;
	private Date fechaBloqueo;
	private String primeraVez;
	private String tipoNacionalidad;
	private Integer idUsuarioAlta;
	private Date fechaAlta;
	private Integer idUsuarioMod;
	private Date fechaMod;
	private Integer idUsuarioBaja;
	private Date fechaBaja;
	private String stRegistro;

	/** lista perfiles. */
	private List<PerfilVO> listaPerfiles;

	private List<Integer> listaPerfilesId;

	/** lista usuario permiso. */
	private List<UsuarioPermisoVO> listaUsuarioPermiso;

	/** usuario datos vo. */
	@Valid
	private UsuarioDatosVO usuarioDatosVO;

	private String nombreCompleto;

	/** Perfil utilizado para la consulta mediante filtros */
	private PerfilVO perfilVO;

	/*
	 * Obtiene id usuario.
	 * @return id usuario
	 */

	

	public List<UsuarioPermisoVO> getListaUsuarioPermiso() {
		return listaUsuarioPermiso;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getLogueado() {
		return logueado;
	}

	public void setLogueado(String logueado) {
		this.logueado = logueado;
	}

	public Date getUltimoAcceso() {
		return ultimoAcceso;
	}

	public void setUltimoAcceso(Date ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}

	public int getNumeroIntentos() {
		return numeroIntentos;
	}

	public void setNumeroIntentos(int numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Date getFechaBloqueo() {
		return fechaBloqueo;
	}

	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}

	public String getPrimeraVez() {
		return primeraVez;
	}

	public void setPrimeraVez(String primeraVez) {
		this.primeraVez = primeraVez;
	}

	public String getTipoNacionalidad() {
		return tipoNacionalidad;
	}

	public void setTipoNacionalidad(String tipoNacionalidad) {
		this.tipoNacionalidad = tipoNacionalidad;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

	public void setListaUsuarioPermiso(
			List<UsuarioPermisoVO> listaUsuarioPermiso) {
		this.listaUsuarioPermiso = listaUsuarioPermiso;
	}

	@NotEmpty(message = PERFIL_NO_VACIO, groups = { GuardarUsuario.class })
	@NotNull(message = PERFIL_NO_VACIO, groups = { GuardarUsuario.class })
	public List<Integer> getListaPerfilesId() {
		return listaPerfilesId;
	}

	public void setListaPerfilesId(List<Integer> listaPerfilesId) {
		this.listaPerfilesId = listaPerfilesId;
	}

	@Valid
	public UsuarioDatosVO getUsuarioDatosVO() {
		return usuarioDatosVO;
	}

	public void setUsuarioDatosVO(UsuarioDatosVO usuarioDatosVO) {
		this.usuarioDatosVO = usuarioDatosVO;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	@NotEmpty(message = NOMBRE_REQUERIDO, groups = { GuardarUsuario.class })
	@NotNull(message = NOMBRE_REQUERIDO, groups = { GuardarUsuario.class })
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<PerfilVO> getListaPerfiles() {
		return listaPerfiles;
	}

	public void setListaPerfiles(List<PerfilVO> listaPerfiles) {
		this.listaPerfiles = listaPerfiles;
	}

	public PerfilVO getPerfilVO() {
		return perfilVO;
	}

	public void setPerfilVO(PerfilVO perfilVO) {
		this.perfilVO = perfilVO;
	}

}
