package mx.gob.segob.dgti.renap.catalogos.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.catalogos.business.service.CatalogosService;
import mx.gob.segob.dgti.renap.catalogos.business.service.UbicacionService;
import mx.gob.segob.dgti.renap.catalogos.business.vo.PerfilesPorPerfilesVO;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPerfil;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CTipoAccion;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.CatalogosDao;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.UbicacionDao;
import mx.gob.segob.dgti.renap.home.business.service.HomeService;
import mx.gob.segob.dgti.renap.home.business.vo.UserVo;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Service
public class CatalogosServiceImpl implements CatalogosService {

	@Autowired
	private CatalogosDao flCatalogosDao;

	@Override
	@Transactional
	public List<CTipoAccion> getListTipoAccion(){
		return flCatalogosDao.getListTipoAccion();
	}

	@Override
	@Transactional
	public List<CPerfil> getListPerfiles() {
		return flCatalogosDao.getListPerfiles();
	};
	
	@Override
	@Transactional
	public List<PerfilesPorPerfilesVO> getLstPerfByIdPerf(Integer idPerfil, String tipoFiltro){
		return flCatalogosDao.getLstPerfByIdPerf(idPerfil, tipoFiltro);
	}
}
