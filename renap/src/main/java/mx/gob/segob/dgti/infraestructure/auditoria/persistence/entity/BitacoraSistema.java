package mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "h_bitacora")
public class BitacoraSistema implements Serializable {

	/***/
	private static final long serialVersionUID = -3606849069115824669L;

	@Id
	@Column(name = "id_h_bitacora")
	private Integer idHBitacora;
	@Column(name = "id_m_usuario")
	private Integer idMUsuario;
	@Column(name = "id_c_ent_fed")
	private Integer idCEntFed;
	@Column(name = "id_c_accion")
	private Integer idCAccion;
	@Column(name = "accion")
	private String accion;
	@Column(name = "fecha")
	private String fecha;
	@Column(name = "hora")
	private String hora;
	@Column(name = "moldulo")
	private String modulo;
	@Column(name = "resumen")
	private String resumen;
	@Column(name = "consulta")
	private String consulta;
	@Column(name = "id_m_poder")
	private Integer idMPoder;
	@Column(name = "id_usuario_alta")
	private Integer idUsuarioAlta;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	@Column(name = "id_usuario_mod")
	private Integer idUsuarioMod;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_mod")
	private Date fechaMod;
	@Column(name = "id_usuario_baja")
	private Integer idUsuarioBaja;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_baja")
	private Date fechaBaja;
	@Column(name = "st_registro")
	private String stRegistro;

	public Integer getIdHBitacora() {
		return idHBitacora;
	}

	public void setIdHBitacora(Integer idHBitacora) {
		this.idHBitacora = idHBitacora;
	}

	public Integer getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public Integer getIdCEntFed() {
		return idCEntFed;
	}

	public void setIdCEntFed(Integer idCEntFed) {
		this.idCEntFed = idCEntFed;
	}

	public Integer getIdCAccion() {
		return idCAccion;
	}

	public void setIdCAccion(Integer idCAccion) {
		this.idCAccion = idCAccion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getResumen() {
		return resumen;
	}

	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public Integer getIdMPoder() {
		return idMPoder;
	}

	public void setIdMPoder(Integer idMPoder) {
		this.idMPoder = idMPoder;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
}
