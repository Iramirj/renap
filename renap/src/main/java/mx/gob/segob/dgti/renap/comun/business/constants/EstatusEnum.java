package mx.gob.segob.dgti.renap.comun.business.constants;

import com.fasterxml.jackson.annotation.JsonFormat;

import mx.gob.segob.dgti.infraestructure.base.presentation.converter.EnumConverter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EstatusEnum implements EnumConverter {

	ACTIVO("Activo",true), INACTIVO("Inactivo",false);

	private String descripcion;
	private boolean activo;

	private EstatusEnum(String descripcion,boolean activo) {
		this.setDescripcion(descripcion);
		this.setActivo(activo);
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Override
	public String getDescripcionConverter() {
		return this.getDescripcion();
	}
}
