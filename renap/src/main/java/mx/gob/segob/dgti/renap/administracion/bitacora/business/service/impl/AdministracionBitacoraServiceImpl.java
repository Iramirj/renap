package mx.gob.segob.dgti.renap.administracion.bitacora.business.service.impl;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.builder.AdministracionBitacoraBuilder;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.service.AdministracionBitacoraService;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.ConsultaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.repository.BitacoraSistemaRepository;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;
import mx.gob.segob.dgti.infraestructure.base.business.exception.BusinessException;
import mx.gob.segob.dgti.infraestructure.base.business.service.impl.BaseServiceImpl;
import mx.gob.segob.dgti.infraestructure.base.integration.exception.RepositoryException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministracionBitacoraServiceImpl extends BaseServiceImpl 
	implements AdministracionBitacoraService {

	@Autowired
	private BitacoraSistemaRepository bitacoraSistemaRepository;
	
	private static AdministracionBitacoraBuilder bitacoraBuilder = 
				new AdministracionBitacoraBuilder();
	
	@Override
	public List<ConsultaBitacoraVO> obtenerBitacoras(
			BusquedaBitacoraVO parametroBusqueda, Integer registroInicio,
			Integer numeroRegistros) throws BusinessException {
		List<BitacoraSistema> bitacorasPO = null;
		List<ConsultaBitacoraVO> bitacorasVO = null;
		
		try {
			bitacorasPO = bitacoraSistemaRepository.obtenerBitacoras(parametroBusqueda, 
						registroInicio, numeroRegistros);
		} catch (RepositoryException e) {
			throw new BusinessException(ExcepcionCausaEnum.COMUN_ERROR_CONSULTAR);
		}
		bitacorasVO = bitacoraBuilder.convertirListaEntidad(bitacorasPO);
		return bitacorasVO;
	}

	@Override
	public Integer obtenerTotalBitacoras(BusquedaBitacoraVO parametroBusqueda)
			throws BusinessException {
		Integer total = null;
		try {
			total = bitacoraSistemaRepository.obtenerTotalBitacoras(parametroBusqueda);
		} catch (RepositoryException e) {
			throw new BusinessException(ExcepcionCausaEnum.COMUN_ERROR_CONSULTAR);
		}
		return total;
	}

}
