package mx.gob.segob.dgti.renap.catalogos.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.catalogos.business.service.UbicacionService;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.UbicacionDao;
import mx.gob.segob.dgti.renap.home.business.service.HomeService;
import mx.gob.segob.dgti.renap.home.business.vo.UserVo;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Service
public class UbicacionServiceImpl implements UbicacionService {

	@Autowired
	private UbicacionDao flUbicacionDao;

	@Override
	@Transactional
	public List<CPais> getList() {
		return flUbicacionDao.getList();
	}

	@Override
	@Transactional
	public List<CEntFed> getListByIdPais(Integer idPais) {
		return flUbicacionDao.getListByIdPais(idPais);
	};

	@Override
	@Transactional
	public List<CMunicipio> getListCMunicipioByIdEntifdad(Integer idEntidad) {
		return flUbicacionDao.getListCMunicipioByIdEntifdad(idEntidad);
	};
}
