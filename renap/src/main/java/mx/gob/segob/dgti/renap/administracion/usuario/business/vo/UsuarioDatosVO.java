package mx.gob.segob.dgti.renap.administracion.usuario.business.vo;

import java.util.Date;
import mx.gob.segob.dgti.renap.comun.presentation.constants.MensajesValidaciones;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.GuardarUsuario;

import org.hibernate.validator.constraints.NotEmpty;

public class UsuarioDatosVO implements MensajesValidaciones {

	private Integer idDUsuario;
	private UsuarioVO objMUsuarioVo;
	private Integer idCCargo;
	private Integer idCTitulo;
	private String nombre;
	private String aPaterno;
	private String aMaterno;
	private String curp;
	private String correo;
	private Integer idUsuarioAlta;
	private Date fechaAlta;
	private Integer idUsuarioMod;
	private Date fechaMod;
	private Integer idUsuarioBaja;
	private Date fechaBaja;
	private String stRegistro;
	private String nombreCompleto;

	public Integer getIdDUsuario() {
		return idDUsuario;
	}

	public void setIdDUsuario(Integer idDUsuario) {
		this.idDUsuario = idDUsuario;
	}

	public UsuarioVO getObjMUsuarioVo() {
		return objMUsuarioVo;
	}

	public void setObjMUsuarioVo(UsuarioVO objMUsuarioVo) {
		this.objMUsuarioVo = objMUsuarioVo;
	}

	public Integer getIdCCargo() {
		return idCCargo;
	}

	public void setIdCCargo(Integer idCCargo) {
		this.idCCargo = idCCargo;
	}

	public Integer getIdCTitulo() {
		return idCTitulo;
	}

	public void setIdCTitulo(Integer idCTitulo) {
		this.idCTitulo = idCTitulo;
	}

	@NotEmpty(message = NOMBRE_USUARIO, groups = { GuardarUsuario.class })
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getaPaterno() {
		return aPaterno;
	}

	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}

	public String getaMaterno() {
		return aMaterno;
	}

	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	@NotEmpty(message = CORREO_REQUERIDO, groups = { GuardarUsuario.class })
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getDescripcionConverter() {
		return null;
	}
}
