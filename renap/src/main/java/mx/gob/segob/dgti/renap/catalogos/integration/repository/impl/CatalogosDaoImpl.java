package mx.gob.segob.dgti.renap.catalogos.integration.repository.impl;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.catalogos.business.vo.PerfilesPorPerfilesVO;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPerfil;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CTipoAccion;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.CatalogosDao;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.UbicacionDao;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Repository
public class CatalogosDaoImpl implements CatalogosDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CTipoAccion> getListTipoAccion() {
		@SuppressWarnings("unchecked")
		List<CTipoAccion> list = sessionFactory.getCurrentSession()
				.createQuery("from CTipoAccion where stRegistro = 'A'").list();
		return list.size() > 0 ? list : null;
	}

	@Override
	public List<CPerfil> getListPerfiles() {
		@SuppressWarnings("unchecked")
		List<CPerfil> list = sessionFactory.getCurrentSession()
				.createQuery("from CPerfil where stRegistro = 'A'").list();
		return list.size() > 0 ? list : null;
	};
	
	@Override
	public List<PerfilesPorPerfilesVO> getLstPerfByIdPerf(Integer idPerfil, String tipoFiltro){
		
		List<PerfilesPorPerfilesVO> lPerfiles = new ArrayList<PerfilesPorPerfilesVO>();
		PerfilesPorPerfilesVO perfil;
		
		try {
			CallableStatement cStmt2 = sessionFactory.openStatelessSession().connection()
					.prepareCall("{call PKG_RENAP.MUESTRA_D_PER_PER(?,?,?)}");
			
			cStmt2.setInt(1, idPerfil);
			cStmt2.setString(2, tipoFiltro);
			cStmt2.registerOutParameter(3, Types.VARCHAR);
			
			cStmt2.execute();
			
			ResultSet rset = (ResultSet) cStmt2.getObject(3);
			
			while(rset.next()){
				perfil = new PerfilesPorPerfilesVO();
				perfil.setIdRegistro(rset.getInt("id_d_per_per")!= 0 ? Integer.parseInt(rset.getString("id_d_per_per")):0);
				perfil.setIdPerfilA(rset.getInt("id_c_perfil_a") != 0 ? Integer.parseInt(rset.getString("id_c_perfil_a")):0);
				perfil.setIdPerfilB(rset.getInt("id_c_perfil_b") != 0 ? Integer.parseInt(rset.getString("id_c_perfil_b")):0);
				perfil.setPerfilA(rset.getString("perfil_a") != null ? rset.getString("perfil_a"): "Sin Resgitro");
				perfil.setPerfilB(rset.getString("perfil_b") != null ? rset.getString("perfil_b") : "Sin Registro");
				perfil.setTipoFiltro(rset.getString("tipo_filtro") != null ? rset.getString("tipo_filtro"): "Sin Registro");
				lPerfiles.add(perfil);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lPerfiles.size()>0 ? lPerfiles :  null;
	}
}
