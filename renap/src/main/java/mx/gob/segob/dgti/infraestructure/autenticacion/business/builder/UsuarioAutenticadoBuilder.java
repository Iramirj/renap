package mx.gob.segob.dgti.infraestructure.autenticacion.business.builder;


import java.util.List;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.entity.UsuarioAutenticado;


public class UsuarioAutenticadoBuilder {

	public UsuarioAutenticadoVO convierteEntidadVo(UsuarioAutenticado usuario, List<String> roles){
		UsuarioAutenticadoVO vo = null;
		if(usuario != null){
			vo = new UsuarioAutenticadoVO();
			vo.setIdUsuario(usuario.getUsuarioId());
			
			vo.setUsuario(usuario.getUsuario());

			vo.setStRegistro("I");
			if(usuario.getStRegistro() != null){
				vo.setStRegistro(usuario.getStRegistro());
			}
			vo.setBloqueado("B");
			if(usuario.getBloqueado() != null){
				vo.setBloqueado(usuario.getBloqueado());
			}

			vo.setFechaBloqueo(usuario.getFechaBloqueo());

			vo.setInSession("N");
			if(usuario.getLogueado() != null){
				vo.setInSession(usuario.getLogueado());
			}

			vo.setNumeroIntentos(usuario.getNumeroIntentos());
			vo.setPassword(usuario.getPassword());
			vo.setUltimoAcceso(usuario.getUltimoAcceso());

			vo.setPermisos(roles);

		}
		return vo;
	}
}
