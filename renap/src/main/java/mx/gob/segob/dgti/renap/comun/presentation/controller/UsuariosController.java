package mx.gob.segob.dgti.renap.comun.presentation.controller;
import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.service.UsuarioService;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.catalogos.business.service.UbicacionService;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
public class UsuariosController {

	@Autowired
	private UsuarioService usuarioService;

	public static final ObjectMapper flJsonMapper = new ObjectMapper();

}
