package mx.gob.segob.dgti.renap.administracion.usuario.business.builder;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioPermisoVO;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.UsuarioPermiso;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class UsuarioPermisoBuilder extends
		BaseBuilder<UsuarioPermisoVO, UsuarioPermiso> {

	/** permiso builder. */
	PermisoBuilder permisoBuilder = new PermisoBuilder();

	@Override
	public UsuarioPermiso convertirVOAEntidad(UsuarioPermisoVO vo) {
		UsuarioPermiso entidad = null;
		if (vo != null) {
			entidad = new UsuarioPermiso();
			entidad.setPermiso(permisoBuilder.convertirVOAEntidad(vo
					.getPermiso()));
			Usuario usr = new Usuario();
			if (vo.getUsuario() != null) {
				usr.setIdUsuario(vo.getUsuario().getIdUsuario());
				entidad.setUsuario(usr);
			}
			entidad.setTipoPermiso(vo.getTipoPermiso());

		}
		return entidad;
	}

	@Override
	public UsuarioPermisoVO convertirEntidadAVO(UsuarioPermiso entidad) {
		UsuarioPermisoVO vo = null;
		if (entidad != null) {
			vo = new UsuarioPermisoVO();
			vo.setPermiso(permisoBuilder.convertirEntidadAVO(entidad
					.getPermiso()));
			if (entidad.getUsuario() != null) {
				UsuarioVO usrVO = new UsuarioVO();
				usrVO.setIdUsuario(entidad.getUsuario().getIdUsuario());
				vo.setUsuario(usrVO);
			}
			vo.setTipoPermiso(entidad.getTipoPermiso());
		}
		return vo;
	}

}
