package mx.gob.segob.dgti.renap.administracion.bitacora.business.service;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.ConsultaBitacoraVO;
import mx.gob.segob.dgti.infraestructure.base.business.exception.BusinessException;

public interface AdministracionBitacoraService {
	List<ConsultaBitacoraVO> obtenerBitacoras(BusquedaBitacoraVO parametroBusqueda,
			Integer registroInicio, Integer numeroRegistros) throws BusinessException;
	
	Integer obtenerTotalBitacoras(BusquedaBitacoraVO parametroBusqueda)
			throws  BusinessException;
}
