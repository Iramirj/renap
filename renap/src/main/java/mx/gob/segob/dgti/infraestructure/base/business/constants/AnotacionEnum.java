package mx.gob.segob.dgti.infraestructure.base.business.constants;

import mx.gob.segob.dgti.infraestructure.base.presentation.converter.EnumConverter;




public enum AnotacionEnum implements EnumConverter {

			NOT_EMPTY("org.hibernate.validator.constraints.NotEmpty", 1), 
			NOT_NULL("javax.validation.constraints.NotNull", 2),
			SIZE("javax.validation.constraints.Size", 3),
			LENGHT("org.hibernate.validator.constraints.Length", 4),
			MAX("javax.validation.constraints.Max", 5),
			MIN("javax.validation.constraints.Min", 6),
			RANGE("org.hibernate.validator.constraints.Range", 7),
			SAFE_HTML("org.hibernate.validator.constraints.SafeHtml", 8), 
			ALFABETO_ESPACIO_MX("mx.gob.segob.dgti.siped.comun.presentation.validator.annotation.Alfabeto_Espacio_MX",9),
			ALFANUMERICA_ESPACIO_MX("mx.gob.segob.dgti.siped.comun.presentation.validator.annotation.AlfaNumerico_Espacio_MX",10),
			ALFABETO_SIN_ESPACIO_MX("mx.gob.segob.dgti.siped.comun.presentation.validator.annotation.AlfabetoSinEspacio_MX",11),
			PATTERN("javax.validation.constraints.Pattern", 12),
			CONTRASENA("mx.gob.segob.dgti.siped.comun.presentation.validator.annotation.Contrasenia",13);

	private String descripcion;

	private int secuencia;

	private AnotacionEnum(String descripcion, int secuencia) {
		this.setDescripcion(descripcion);
		this.setSecuencia(secuencia);
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String getDescripcionConverter() {
		return this.getDescripcion();
	}

	public int getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}

	public static AnotacionEnum obtener(String descripcion) {
		for (AnotacionEnum anotacion : AnotacionEnum.values()) {
			if (anotacion.getDescripcion().contains(descripcion)) {
				return anotacion;
			}
		}
		return null;
	}
}
