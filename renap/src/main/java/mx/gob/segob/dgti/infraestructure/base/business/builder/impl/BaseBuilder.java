package mx.gob.segob.dgti.infraestructure.base.business.builder.impl;

import java.util.ArrayList;
import java.util.List;

import mx.gob.segob.dgti.infraestructure.base.business.builder.Builder;

public abstract class BaseBuilder<V, E> implements Builder<V, E> {
	/**
	 * Convertir Lista de Entidad a Lista de VO. Convierte una lista de
	 * entidades a una lista de VO's
	 * 
	 * @param lista
	 *            la lista de entidades
	 * @return una lista de VO's
	 */
	public final List<V> convertirListaEntidad(final List<E> lista) {
		List<V> retorno = null;
		if (lista != null) {
			retorno = new ArrayList<V>(lista.size());
			for (E elemento : lista) {
				retorno.add(convertirEntidadAVO(elemento));
			}
		}
		return retorno;
	}

	/**
	 * Convertir lista de VO a lista Entidad. Convierte una lista de VO's a una
	 * lista de entidades
	 * 
	 * @param lista
	 *            la lista de VO's
	 * @return una lista de entidades
	 */
	public final List<E> convertirListaVO(final List<V> lista) {
		List<E> retorno = null;
		if (lista != null) {
			retorno = new ArrayList<E>(lista.size());
			for (V elemento : lista) {
				retorno.add(convertirVOAEntidad(elemento));
			}
		}
		return retorno;
	}
	
}
