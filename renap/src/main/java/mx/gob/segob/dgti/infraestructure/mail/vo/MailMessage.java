package mx.gob.segob.dgti.infraestructure.mail.vo;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.springframework.core.io.Resource;

public class MailMessage  {


	private String from;
	private String to;
	private String subject;
	private String body;

	private Collection<MailAttachment> attachments = new LinkedList<MailAttachment>();
	private Map<String, Resource > resourcesInLine = new HashMap<String,Resource>(0);

	public MailMessage(String from, String to, String subject) {
		setFrom(from);
		setTo(to);
		setSubject(subject);
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}


	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Collection<MailAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Collection<MailAttachment> attachments) {
		this.attachments = attachments;
	}

	public Map<String, Resource> getResourcesInLine() {
		return resourcesInLine;
	}

	public void setResourcesInLine(Map<String, Resource> resourcesInLine) {
		this.resourcesInLine = resourcesInLine;
	}

}
