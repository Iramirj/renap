package mx.gob.segob.dgti.infraestructure.autenticacion.business.constants;

import com.fasterxml.jackson.annotation.JsonFormat;

import mx.gob.segob.dgti.infraestructure.base.presentation.converter.EnumConverter;
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoPermisoEnum implements EnumConverter  {
	
	ADICIONAL("ADICIONAL","Permiso adicional al Perfil"), 
	DENEGADO("DENEGADO","Permiso que se restinge al perfil");
	
	private String id;
	private String descripcion;
	
	private TipoPermisoEnum(String id,String descripcion){
		this.id=id;
		this.descripcion=descripcion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String getDescripcionConverter() {
		// TODO Auto-generated method stub
		return this.id;
	} 
	
}
