package mx.gob.segob.dgti.renap.administracion.usuario.business.service;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.exception.AdministracionException;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;


public interface UsuarioService {


	public List<UsuarioVO> buscar(UsuarioVO usuarioVO, Integer primerRegistro,int ultimoRegistro);

	public Integer buscarTotal(UsuarioVO usuarioVO);

	public UsuarioVO obtenerPorId(String usuarioId);

	public void editar(UsuarioVO usuarioVO);
	
	public boolean existe(UsuarioVO usuarioVO);

	public void guardarNuevo(UsuarioVO usuarioVO) throws AdministracionException;

	public void guardar(UsuarioVO usuarioVO);

	//public void actualizaPassword(String usuarioId, String password) throws Exception;
	
	public void reiniciarContrasena(UsuarioVO usuarioVO) throws AdministracionException;
	
	public List<String> obtenerCorreosUsuarios(UsuarioVO usuarioVO);

	public UsuarioVO obtenerPorIdSession(String usuarioId);
	
	public List<Usuario> getListUserByIdUserLogeo(Integer idUsuario);
	
	public Integer getPerfilByIdUsuario(Integer idIUsuario); 
}
