package mx.gob.segob.dgti.infraestructure.autenticacion.business.service;


import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

public interface AutenticacionService {
	
	UsuarioAutenticadoVO getUsuario(String nombreUsuario) throws AutenticacionExcepcion;
	
	void inicializarUsuarios();
	
	void registrarUsuario(String nombreUsuario);
	
	void anularUsuarioSession(String nombreUsuario);
	
	void registraIntentoSession(String nombreUsuario, Integer numeroIntento, String bloqueo);	
}
