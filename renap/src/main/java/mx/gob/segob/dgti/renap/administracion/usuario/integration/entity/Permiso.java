package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.gob.segob.dgti.renap.comun.business.constants.PermisoEnum;

@Entity
@Table(name = "c_permiso", uniqueConstraints = @UniqueConstraint(columnNames = "id_permiso"))
public class Permiso implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PermisoEnum idPermiso;
	private String descripcion;
	private List<UsuarioPermiso> listaUsuarioPermiso;
	private boolean activo;

	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "id_permiso", unique = true, nullable = false)
	public PermisoEnum getIdPermiso() {
		return idPermiso;
	}

	public void setIdPermiso(PermisoEnum idPermiso) {
		this.idPermiso = idPermiso;
	}

	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuarioPermisoPK.permiso")
	public List<UsuarioPermiso> getListaUsuarioPermiso() {
		return listaUsuarioPermiso;
	}

	public void setListaUsuarioPermiso(List<UsuarioPermiso> listaUsuarioPermiso) {
		this.listaUsuarioPermiso = listaUsuarioPermiso;
	}

	@Column(name = "activo")
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

}
