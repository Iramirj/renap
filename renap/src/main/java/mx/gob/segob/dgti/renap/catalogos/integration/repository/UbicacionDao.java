package mx.gob.segob.dgti.renap.catalogos.integration.repository;

import java.util.List;

import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPerfil;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CTipoAccion;

public interface UbicacionDao {
	public List<CPais> getList();
	public List<CEntFed> getListByIdPais(Integer idPais);
	public List<CMunicipio> getListCMunicipioByIdEntifdad(Integer idEntidad);
}
