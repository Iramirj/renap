package mx.gob.segob.dgti.renap.administracion.usuario.business.builder;

import java.util.ArrayList;
import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioPermisoVO;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Perfil;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.UsuarioPermiso;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class UsuarioBuilder extends BaseBuilder<UsuarioVO, Usuario> {

	UsuarioDatosBuilder usuarioDatosBuilder = new UsuarioDatosBuilder();
	UsuarioPermisoBuilder usuarioPermisoBuilder = new UsuarioPermisoBuilder();
	PerfilBuilder perfilBuilder = new PerfilBuilder();

	@Override
	public Usuario convertirVOAEntidad(UsuarioVO vo) {
		Usuario entidad = null;
		if (vo != null) {
			entidad = new Usuario();
			entidad.setIdUsuario(vo.getIdUsuario());
			entidad.setUsuario(vo.getUsuario());
			entidad.setPassword(vo.getPassword());
			entidad.setEstatus(vo.getEstatus());
			entidad.setLogueado(vo.getLogueado());
			entidad.setUltimoAcceso(vo.getUltimoAcceso());
			entidad.setNumeroIntentos(vo.getNumeroIntentos());
			entidad.setBloqueado(vo.getBloqueado());
			entidad.setFechaBloqueo(vo.getFechaBloqueo());
			entidad.setPrimeraVez(vo.getPrimeraVez());
			entidad.setTipoNacionalidad(vo.getTipoNacionalidad());
			entidad.setIdUsuarioAlta(vo.getIdUsuarioAlta());
			entidad.setFechaAlta(vo.getFechaAlta());
			entidad.setIdUsuarioMod(vo.getIdUsuarioMod());
			entidad.setFechaMod(vo.getFechaMod());
			entidad.setIdUsuarioBaja(vo.getIdUsuarioBaja());
			entidad.setFechaBaja(vo.getFechaBaja());
			entidad.setStRegistro(vo.getStRegistro());

			if (vo.getUsuarioDatosVO() != null) {

				entidad.setUsuarioDatos(usuarioDatosBuilder
						.convertirVOAEntidad(vo.getUsuarioDatosVO()));
				entidad.getUsuarioDatos().setIdMUsuario(entidad);
			}

			if (vo.getListaPerfiles() != null
					&& !vo.getListaPerfiles().isEmpty()) {
				List<Perfil> perfiles = new ArrayList<Perfil>();
				perfiles = perfilBuilder
						.convertirListaVO(vo.getListaPerfiles());
				entidad.setListaPerfiles(perfiles);
			}

			if (vo.getListaPerfilesId() != null
					&& !vo.getListaPerfilesId().isEmpty()) {
				List<Perfil> perfiles = new ArrayList<Perfil>();
				for (Integer id : vo.getListaPerfilesId()) {
					Perfil perfil = new Perfil();
					perfil.setIdPerfil(id);
					perfiles.add(perfil);
				}
				entidad.setListaPerfiles(perfiles);
			}

			if (vo.getListaUsuarioPermiso() != null) {
				List<UsuarioPermiso> permisos = usuarioPermisoBuilder
						.convertirListaVO(vo.getListaUsuarioPermiso());
				entidad.setListaUsuarioPermiso(permisos);
			}
		}
		return entidad;
	}

	@Override
	public UsuarioVO convertirEntidadAVO(Usuario entidad) {
		UsuarioVO vo = null;
		if (entidad != null) {
			vo = new UsuarioVO();
			vo.setIdUsuario(entidad.getIdUsuario());
			vo.setUsuario(entidad.getUsuario());
			vo.setPassword(entidad.getPassword());
			vo.setEstatus(entidad.getEstatus());
			vo.setLogueado(entidad.getLogueado());
			vo.setUltimoAcceso(entidad.getUltimoAcceso());
			vo.setNumeroIntentos(entidad.getNumeroIntentos());
			vo.setBloqueado(entidad.getBloqueado());
			vo.setFechaBloqueo(entidad.getFechaBloqueo());
			vo.setPrimeraVez(entidad.getPrimeraVez());
			vo.setTipoNacionalidad(entidad.getTipoNacionalidad());
			vo.setIdUsuarioAlta(entidad.getIdUsuarioAlta());
			vo.setFechaAlta(entidad.getFechaAlta());
			vo.setIdUsuarioMod(entidad.getIdUsuarioMod());
			vo.setFechaMod(entidad.getFechaMod());
			vo.setIdUsuarioBaja(entidad.getIdUsuarioBaja());
			vo.setFechaBaja(entidad.getFechaBaja());
			vo.setStRegistro(entidad.getStRegistro());

			if (entidad.getUsuarioDatos() != null) {
				vo.setUsuarioDatosVO(usuarioDatosBuilder
						.convertirEntidadAVO(entidad.getUsuarioDatos()));

				if (entidad.getUsuarioDatos() != null) {
					vo.setNombreCompleto(entidad.getUsuarioDatos().getNombre()
							+ " "
							+ entidad.getUsuarioDatos().getaPaterno()
							+ " "
							+ entidad.getUsuarioDatos().getaMaterno());
				}
			}

			if (entidad.getListaPerfiles() != null) {
				List<PerfilVO> perfiles = perfilBuilder
						.convertirListaEntidad(entidad.getListaPerfiles());
				vo.setListaPerfiles(perfiles);
				List<Integer> idPerfiles = perfilBuilder
						.convertirEntidadesAId(entidad.getListaPerfiles());
				vo.setListaPerfilesId(idPerfiles);
			}
			if (entidad.getListaUsuarioPermiso() != null) {
				List<UsuarioPermisoVO> permisos = usuarioPermisoBuilder
						.convertirListaEntidad(entidad.getListaUsuarioPermiso());
				vo.setListaUsuarioPermiso(permisos);

			}
		}
		return vo;
	}

	public UsuarioVO convertirUsuarioSession(Usuario entidad) {
		UsuarioVO vo = null;
		if (entidad != null) {
			vo = new UsuarioVO();
			vo.setIdUsuario(entidad.getIdUsuario());
			vo.setNombreUsuario(entidad.getUsuario());
			vo.setUltimoAcceso(entidad.getUltimoAcceso());
			vo.setPrimeraVez(entidad.getPrimeraVez());
			vo.setTipoNacionalidad(entidad.getTipoNacionalidad());

			if (entidad.getUsuarioDatos() != null) {
				vo.setUsuarioDatosVO(usuarioDatosBuilder
						.convertirEntidadAVO(entidad.getUsuarioDatos()));

				if (entidad.getUsuarioDatos() != null) {
					vo.setNombreCompleto(entidad.getUsuarioDatos().getNombre()
							+ " "
							+ entidad.getUsuarioDatos().getaPaterno()
							+ " "
							+ entidad.getUsuarioDatos().getaMaterno());

				}
			}
		}
		return vo;
	}
}
