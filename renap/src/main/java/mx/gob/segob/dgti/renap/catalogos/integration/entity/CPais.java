package mx.gob.segob.dgti.renap.catalogos.integration.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "c_pais")
public class CPais implements java.io.Serializable {

	private static final long serialVersionUID = 6862492579137664963L;

	@Id
	@Column(name = "ID_C_PAIS", unique = true, nullable = false, length = 64)
	private Integer idCPais;
	@Column(name = "PAIS")
	private String pais;
	@Column(name = "PAIS_SIGLAS")
	private String paisSiglas;
	@Column(name = "id_usuario_alta")
	private Integer idUsuarioAlta;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	@Column(name = "id_usuario_mod")
	private Integer idUsuarioMod;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_mod")
	private Date fechaMod;
	@Column(name = "id_usuario_baja")
	private Integer idUsuarioBaja;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_baja")
	private Date fechaBaja;
	@Column(name = "st_registro")
	private String stRegistro;

	public CPais() {

	}

	public CPais(Integer idCPais) {
		this.idCPais = idCPais;
	}

	public Integer getIdCPais() {
		return idCPais;
	}

	public void setIdCPais(Integer idCPais) {
		this.idCPais = idCPais;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPaisSiglas() {
		return paisSiglas;
	}

	public void setPaisSiglas(String paisSiglas) {
		this.paisSiglas = paisSiglas;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

}
