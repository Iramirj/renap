package mx.gob.segob.dgti.renap.correo.business.service;

import java.util.List;
import java.util.Map;

import mx.gob.segob.dgti.renap.correo.business.constants.CorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.constants.ParametrosCorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.helper.MensajeCorreoHelper;
import mx.gob.segob.dgti.infraestructure.base.presentation.helper.MensajeHelper;


public interface NotificacionService {

	/** nombre imagen encabezado. */
	String NOMBRE_IMAGEN_ENCABEZADO = "/recursos/imagenes/SEGOB_logo.png";
	String NOMBRE_IMAGEN_PIE_PAGINA = "";
	
	String NOTIFICACION_SERVICE = "notificacionService";

	String URL_BASE_IMAGENES_PROPERTY = "mail.urlBase.imagenes";
	/** correo remitente. */
	String CORREO_REMITENTE = MensajeHelper
			.obtenerPropiedadSpring("mail.correo");

	
	/** pie pagina. */
	String PIE_PAGINA = "<img src='cid:piePagina'>";
	/** encabezado. */
	String ENCABEZADO = "<img src='cid:encabezado'>";
	/*Nombre del sistema */
	String NOMBRE_DEL_SISTEMA="<p>SEGOB - Arquitectura Base</p>";
	/** html. */
	String HTML = "<html><body>"+ENCABEZADO+"<br/> @contenido@ <br/>"
			+ "<p> Favor de no responder al remitente ya que es una respuesta autom&aacute;tica generada por el sistema, "
			+ "en caso de tener alguna duda o comentario "
			+ "favor de contactar al Administrador del Sistema. </p>"
			+NOMBRE_DEL_SISTEMA+PIE_PAGINA+"</body></html>";

	/** remplazo. */
	String REMPLAZO = "@contenido@";

	

	/** caracteres acento actual. */
	String CARACTERES_ACENTO_ACTUAL = "CARACTERES_ACENTO_ACTUAL";

	/** caracteres acento remplazar. */
	String CARACTERES_ACENTO_REMPLAZAR = "CARACTERES_ACENTO_REMPLAZAR";

	/** con acento. */
	char[] CON_ACENTO = MensajeCorreoHelper.obtenerMensajeResorceCorreo(
			CARACTERES_ACENTO_ACTUAL).toCharArray();

	/** cid pie pagina. */
	String CID_PIE_PAGINA = "piePagina";

	/** cid encabezdo. */
	String CID_ENCABEZDO = "encabezado";
	
	
	/** sin acento. */
	String[] SIN_ACENTO = MensajeCorreoHelper.obtenerMensajeResorceCorreo(
			CARACTERES_ACENTO_REMPLAZAR).split(",");

	/** cuantos. */
	int CUANTOS = CON_ACENTO.length;

	/** link aplication. */
	String LINK_APLICATION = "https://arquitecturaBase.segob.gob.mx";

	void notificarLista(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			List<String> destinatarios);

	void notificar(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			String destinatario);

	void notificarLista(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			List<String> destinatarios, boolean mostrarPiePagina);

	void notificar(CorreoEnum notificacionEnum,
			Map<ParametrosCorreoEnum, String> parametros,
			String destinatario, boolean mostrarPiePagina);
}
