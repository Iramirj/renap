package mx.gob.segob.dgti.infraestructure.auditoria.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.segob.dgti.infraestructure.auditoria.business.builder.BitacoraBuilder;
import mx.gob.segob.dgti.infraestructure.auditoria.business.service.AuditoriaService;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.repository.BitacoraRepository;
import mx.gob.segob.dgti.infraestructure.auditoria.vo.BitacoraVO;

@Service(value="auditoriaService")
public class AuditoriaServiceImpl implements AuditoriaService {

	@Autowired
	BitacoraRepository bitacoraRepository;
	
	@Override	
	public void guardarBitacora(BitacoraVO bitacoraVO) throws Exception {
		BitacoraBuilder bitacoraBuilder = new BitacoraBuilder();
		BitacoraSistema bitacora = bitacoraBuilder.convertirVOaEntidad(bitacoraVO);
		bitacoraRepository.guardarBitacora(bitacora);
	}
}
