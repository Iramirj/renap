package mx.gob.segob.dgti.renap.catalogos.business.service;

import java.util.List;

import mx.gob.segob.dgti.renap.catalogos.business.vo.PerfilesPorPerfilesVO;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPerfil;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CTipoAccion;

public interface CatalogosService {
	public List<CTipoAccion> getListTipoAccion();
	public List<CPerfil> getListPerfiles();
	public List<PerfilesPorPerfilesVO> getLstPerfByIdPerf(Integer idPerfil, String tipoFiltro);
}
