package mx.gob.segob.dgti.infraestructure.autenticacion.persistence.repository;

import java.util.List;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.entity.UsuarioAutenticado;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.BaseRepository;


public interface AutenticacionRepository extends BaseRepository {	
	
	UsuarioAutenticado getUsuario(String nombreUsuario) throws AutenticacionExcepcion;
	
	List<String> getRoles(String nombreUsuario) throws AutenticacionExcepcion;
	
	void inicializarUsuarios();
	
	void registrarUsuario(String usuarioId);
	
	void anularUsuarioSession(String usuarioId);
	
	void registraIntentoSession(String usuarioId, Integer numeroIntento, String bloqueo);
	
	List<String> getPermisosPerfil(String nombreUsuario) throws AutenticacionExcepcion;
	
	List<String> getPermisosUsuario(String nombreUsuario, TipoPermisoEnum tipoPermiso)  throws AutenticacionExcepcion;
}
