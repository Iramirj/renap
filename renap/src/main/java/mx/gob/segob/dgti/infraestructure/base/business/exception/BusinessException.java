package mx.gob.segob.dgti.infraestructure.base.business.exception;



import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;

import org.apache.log4j.Logger;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final Logger logger = Logger.getLogger(getClass().getPackage()
			.getName());
	
	
	private ExcepcionCausaEnum  causaEnum;

	public BusinessException( ExcepcionCausaEnum causaEnum) {
		super(causaEnum.getDescripcion());		
	}

	
	public BusinessException( ExcepcionCausaEnum causaEnum, Throwable cause) {
		super(causaEnum.getDescripcion(), cause);
	}
	
	public BusinessException( String module,ExcepcionCausaEnum causaEnum, Throwable cause) {
		super("Modulo- "+module+":"+causaEnum.getDescripcion(), cause);
	}


	public ExcepcionCausaEnum getCausaEnum() {
		return causaEnum;
	}

	
}
