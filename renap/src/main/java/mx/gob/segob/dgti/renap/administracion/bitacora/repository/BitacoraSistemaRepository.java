package mx.gob.segob.dgti.renap.administracion.bitacora.repository;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.base.integration.exception.RepositoryException;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.BaseRepository;

public interface BitacoraSistemaRepository extends BaseRepository {
	
	String ID_BITACORA_COLUMNA = "idBitacora";
	String ID_USUARIO_COLUMNA = "idUsuario";
	String FECHA_HORA_COLUMNA = "fechaHora";
	

	List<BitacoraSistema> obtenerBitacoras(BusquedaBitacoraVO parametroBusqueda,
			Integer registroInicio, Integer numeroRegistros)
			throws RepositoryException;


	Integer obtenerTotalBitacoras(BusquedaBitacoraVO parametroBusqueda)
			throws RepositoryException;
}
