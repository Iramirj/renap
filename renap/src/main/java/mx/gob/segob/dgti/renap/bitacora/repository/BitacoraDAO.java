package mx.gob.segob.dgti.renap.bitacora.repository;

import java.util.List;

import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;

public interface BitacoraDAO {

	public String insertarBitacora (BitacoraVO bitacora);
	public List<BitacoraVO> consultarBitacora(BitacoraVO bitacora);
	
	
}
