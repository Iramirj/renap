package mx.gob.segob.dgti.infraestructure.mail.vo;

public class MailAttachment  {


	private String name;
	private String contentType;
	private byte[] content;

	public MailAttachment(String name, String contentType, byte[] content) {
		this.name = name;
		this.contentType = contentType;
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

}
