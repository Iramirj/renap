package mx.gob.segob.dgti.renap.administracion.usuario.presentation.controller;



import mx.gob.segob.dgti.infraestructure.base.business.vo.DataTableResult;
import mx.gob.segob.dgti.infraestructure.base.business.vo.MensajeRespuestaVO;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

public interface BusquedaController<V, T> {

	public @ResponseBody
	ResponseEntity<MensajeRespuestaVO> validar(@ModelAttribute V vo);

	DataTableResult<V> buscar(@ModelAttribute V busqueda,
			@ModelAttribute T parametrosBusqueda);
}
