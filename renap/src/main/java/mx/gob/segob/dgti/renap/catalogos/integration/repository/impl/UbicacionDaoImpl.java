package mx.gob.segob.dgti.renap.catalogos.integration.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;
import mx.gob.segob.dgti.renap.catalogos.integration.repository.UbicacionDao;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Repository
public class UbicacionDaoImpl implements UbicacionDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CPais> getList() {
		@SuppressWarnings("unchecked")
		List<CPais> list = sessionFactory
				.getCurrentSession()
				.createQuery(
						"from CPais where stRegistro = 'A'").list();
		return list.size() > 0 ? list : null;
	}

	@Override
	public List<CEntFed> getListByIdPais(Integer idPais) {
		@SuppressWarnings("unchecked")
		List<CEntFed> list = sessionFactory
				.getCurrentSession()
				.createQuery(
						"from CEntFed where  idCPais = ? and stRegistro = 'A'")
				.setParameter(0, idPais).list();
		return list.size() > 0 ? list : null;
	};

	@Override
	public List<CMunicipio> getListCMunicipioByIdEntifdad(Integer idEntidad) {
		@SuppressWarnings("unchecked")
		List<CMunicipio> list = sessionFactory
				.getCurrentSession()
				.createQuery(
						"from CMunicipio where  idCEntFed = ? and stRegistro = 'A'")
				.setParameter(0, idEntidad).list();
		return list.size() > 0 ? list : null;
	};
}
