package mx.gob.segob.dgti.renap.home.presentation.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.home.business.service.HomeService;
import mx.gob.segob.dgti.renap.home.business.vo.UserVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
public class HomeController {

	@Autowired
	private HomeService flHomeService;

	public static final ObjectMapper flJsonMapper = new ObjectMapper();

	@RequestMapping(value = "/modules/home/returnDetalle/{flIdUsuario}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody
	String returnDetalle(@PathVariable(value = "flIdUsuario") Integer flIdUsuario)
			throws JsonProcessingException {
		String resultJson = null;
		Usuario flUsuario = flHomeService.getDetalleById(flIdUsuario);
		if (flUsuario.getIdUsuario() != null) {
			resultJson = flJsonMapper.writeValueAsString(flUsuario);
		}
		return resultJson;
	}

	@RequestMapping(value = "/modules/home/changePassword", method = RequestMethod.POST)
	public @ResponseBody
	String changePassword(@RequestBody String flDatos)
			throws JsonParseException, JsonMappingException, IOException {
		String resultJson = null;
		
		UserVo flUser = flJsonMapper.readValue(flDatos, UserVo.class);
		Usuario flUsuario = flHomeService.getDetalleById(flUser
				.getFlIdUsuario());
		flUsuario.setPassword(flUser.getFlPassword());
		flUsuario.setFechaMod(new Date());
		flUsuario.setPrimeraVez("N");
		flUsuario.setStRegistro(flUsuario.getStRegistro());
		Boolean flResult = flHomeService.changePasswordById(flUsuario);
		if (flResult) {
			resultJson = flJsonMapper.writeValueAsString("successRegister");
		} else {
			resultJson = flJsonMapper.writeValueAsString("errorRegister");
		}
		return resultJson;
	}

	@RequestMapping(value = "/modules/home/returnName/{flName}", method = RequestMethod.GET, headers = "Accept=applocation/json")
	public @ResponseBody
	String returnName(@PathVariable(value = "flName") String flName) {
		System.out.println("flName: " + flName);
		return flName;
	}

	@RequestMapping(value = "/modules/home/returnFecha/{flIdUsuario}", method = RequestMethod.GET)
	public @ResponseBody
	String returnFecha(@PathVariable(value = "flIdUsuario") Integer flIdUsuario) {
		Usuario flUsuario = flHomeService.getDetalleById(flIdUsuario);
		return flUsuario.getUltimoAcceso().toString();
	}

	@RequestMapping("/resource")
	public Map<String, Object> home() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", UUID.randomUUID().toString());
		model.put("content", "Hello World");
		return model;
	}
}
