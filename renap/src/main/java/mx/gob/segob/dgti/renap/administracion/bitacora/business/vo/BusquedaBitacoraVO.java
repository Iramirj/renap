package mx.gob.segob.dgti.renap.administracion.bitacora.business.vo;

import java.util.Date;

import mx.gob.segob.dgti.renap.comun.presentation.validator.annotation.Alfabeto_Espacio_MX;

public class BusquedaBitacoraVO {
	private String idUsuario;
	private Date fechaInicio;
	private Date fechaFin;
	
	@Alfabeto_Espacio_MX
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
}
