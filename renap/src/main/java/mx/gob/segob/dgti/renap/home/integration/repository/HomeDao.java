package mx.gob.segob.dgti.renap.home.integration.repository;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;

public interface HomeDao {
	public Usuario getDetalleById(Integer flIdUsuario);
	public Boolean changePasswordById(Usuario objUsuario);
}
