package mx.gob.segob.dgti.infraestructure.auditoria.persistence.repository;

import java.util.List;

import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.BaseRepository;

public interface BitacoraRepository extends BaseRepository {
	List<BitacoraSistema> getBitacora(Integer userId);

	void guardarBitacora(BitacoraSistema entidad) throws Exception;
}
