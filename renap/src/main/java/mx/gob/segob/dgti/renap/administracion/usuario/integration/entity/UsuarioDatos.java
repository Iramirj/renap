package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "d_usuario")
public class UsuarioDatos implements java.io.Serializable {

	private static final long serialVersionUID = 6862492579137664963L;

	@Id
	@Column(name = "id_d_usuario", unique = true, nullable = false, length = 64)
	private Integer idDUsuario;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_m_usuario")
	private Usuario idMUsuario;
	@Column(name = "id_c_cargo")
	private Integer idCCargo;
	@Column(name = "id_c_titulo")
	private Integer idCTitulo;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "a_paterno")
	private String aPaterno;
	@Column(name = "a_materno")
	private String aMaterno;
	@Column(name = "curp")
	private String curp;
	@Column(name = "correo")
	private String correo;
	@Column(name = "id_usuario_alta")
	private Integer idUsuarioAlta;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	@Column(name = "id_usuario_mod")
	private Integer idUsuarioMod;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_mod")
	private Date fechaMod;
	@Column(name = "id_usuario_baja")
	private Integer idUsuarioBaja;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_baja")
	private Date fechaBaja;
	@Column(name = "st_registro")
	private String stRegistro;

	public UsuarioDatos() {
	}

	public UsuarioDatos(Integer idDUsuario) {
		this.idDUsuario = idDUsuario;
	}

	public Integer getIdDUsuario() {
		return idDUsuario;
	}

	public void setIdDUsuario(Integer idDUsuario) {
		this.idDUsuario = idDUsuario;
	}

	public Usuario getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Usuario idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public Integer getIdCCargo() {
		return idCCargo;
	}

	public void setIdCCargo(Integer idCCargo) {
		this.idCCargo = idCCargo;
	}

	public Integer getIdCTitulo() {
		return idCTitulo;
	}

	public void setIdCTitulo(Integer idCTitulo) {
		this.idCTitulo = idCTitulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getaPaterno() {
		return aPaterno;
	}

	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}

	public String getaMaterno() {
		return aMaterno;
	}

	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

}
