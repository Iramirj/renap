package mx.gob.segob.dgti.infraestructure.autenticacion.persistence.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.entity.UsuarioAutenticado;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.repository.AutenticacionRepository;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.impl.BaseRepositoryImpl;

@Repository
public class AutenticacionRepositoryImpl extends BaseRepositoryImpl 
						implements AutenticacionRepository { 

	
	public UsuarioAutenticado getUsuario(String nombreUsuario)
			throws AutenticacionExcepcion {
		UsuarioAutenticado usuarioAutenticado = null;
   	    StringBuilder sbQuery = new StringBuilder();
   	 	sbQuery.append("SELECT id_m_usuario, usuario, password, st_registro, logueado, ultimo_acceso, numero_intentos, bloqueado, fecha_bloqueo ");
   	 	sbQuery.append("FROM m_usuario ");
   	 	sbQuery.append("WHERE usuario = :usuarioNombre");
   	
   		SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
   		query.setString("usuarioNombre", nombreUsuario);
   		query.addEntity(UsuarioAutenticado.class);

   		usuarioAutenticado = (UsuarioAutenticado) query.uniqueResult();
   		
		return usuarioAutenticado;
	}

	
	public void inicializarUsuarios() {
	    StringBuilder sbQuery = new StringBuilder();
	    sbQuery.append("UPDATE m_usuario ");
	    sbQuery.append("SET logueado = 'N'");

	    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());

	    query.executeUpdate();
	}

	
	public void registrarUsuario(String nombreUsuario) {
		   	StringBuilder sbQuery = new StringBuilder();
		    sbQuery.append("UPDATE m_usuario ");
		    sbQuery.append("SET logueado = 'S', ");
		    sbQuery.append("ultimo_acceso = :ultimoAcceso, ");
		    sbQuery.append("numero_intentos = 0, ");
		    sbQuery.append("bloqueado = 'D', ");
		    sbQuery.append("fecha_bloqueo = NULL ");
		    sbQuery.append("WHERE usuario = :usuarioNombre");

		    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
		    query.setTimestamp("ultimoAcceso", new Date());
		    query.setString("usuarioNombre", nombreUsuario);

		    query.executeUpdate();
		
	}

	
	public void anularUsuarioSession(String nombreUsuario) {
		 StringBuilder sbQuery = new StringBuilder();
		 sbQuery.append("UPDATE m_usuario ");
		 sbQuery.append("SET en_sesion = 0 ");
		 sbQuery.append("WHERE usuario = :usuarioNombre");

		 SQLQuery query = getSession().createSQLQuery(sbQuery.toString());

		 query.setParameter("usuarioNombre", nombreUsuario);

		 query.executeUpdate();
	}

	
	public void registraIntentoSession(String nombreUsuario, Integer numeroIntento,
			String bloqueo) {
		 	StringBuilder sbQuery = new StringBuilder();
		    sbQuery.append("UPDATE m_usuario ");
		    sbQuery.append("SET numero_intentos = :numeroIntentos, ");
		    sbQuery.append("	fecha_bloqueo = :fechaBloqueo, ");
		    sbQuery.append(" 	 bloqueado = :bloqueado ");
		    sbQuery.append("WHERE usuario = :usuarioNombre");

		    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
		    query.setInteger("numeroIntentos", numeroIntento);
	        query.setString("bloqueado", bloqueo == "B"?"B":"D");
		    query.setTimestamp("fechaBloqueo", new Date());
		    query.setString("usuarioNombre", nombreUsuario);

		    query.executeUpdate();
		
	}

	@SuppressWarnings("unchecked")
	
	public List<String> getRoles(String nombreUsuario)
			throws AutenticacionExcepcion {
			  
	  	StringBuilder sbQuery = new StringBuilder();
	  	
	  	sbQuery.append("SELECT DISTINCT(c.rol) ");
	  	sbQuery.append("FROM d_usuario_perfil a, ");
	  	sbQuery.append(" 	 c_perfil b, ");
	  	sbQuery.append("	 d_perfil c ");
    	sbQuery.append("WHERE a.id_m_usuario = :usuarioNombre ");
    	sbQuery.append("AND a.id_c_perfil = b.id_c_perfil ");
    	sbQuery.append("AND c.id_c_perfil = b.id_c_perfil ");
     	
	    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
	    query.setParameter("usuarioNombre", nombreUsuario);
	    
	    List<String> listObject = query.list();			  

		return listObject;
	}
	
	@SuppressWarnings("unchecked")
	
	public List<String> getPermisosPerfil(String nombreUsuario)
			throws AutenticacionExcepcion {
			  
	  	StringBuilder sbQuery = new StringBuilder();
	  	
	  	sbQuery.append("SELECT DISTINCT(p.permiso) ");
	  	sbQuery.append("FROM m_usuario u, ");
	  	sbQuery.append(" 	 d_usuario_perfil   a, ");
	  	sbQuery.append(" 	 c_perfil 			b, ");
	  	sbQuery.append("	 d_perfil_permiso 	c ");
	  	sbQuery.append("LEFT OUTER JOIN c_permiso p ");
	  	sbQuery.append("ON c.id_c_permiso = p.id_c_permiso and p.st_registro='A' ");
    	sbQuery.append("WHERE u.usuario = :usuarioNombre ");
    	sbQuery.append("AND a.id_m_usuario = u.id_m_usuario ");
    	sbQuery.append("AND a.id_c_perfil = b.id_c_perfil ");
    	sbQuery.append("AND c.id_c_perfil = b.id_c_perfil ");
     	
	    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
	    query.setParameter("usuarioNombre", nombreUsuario);
	    
	    List<String> listObject = query.list();			  

		return listObject;
	}

	@SuppressWarnings("unchecked")
	public List<String> getPermisosUsuario(String nombreUsuario, TipoPermisoEnum tipoPermiso)
			throws AutenticacionExcepcion {
		
		StringBuilder sbQuery = new StringBuilder();
	  	
	  	sbQuery.append("SELECT DISTINCT(a.id_c_permiso) ");
	  	sbQuery.append("FROM d_usuario_permiso a ");
    	sbQuery.append("WHERE a.id_m_usuario = :usuarioNombre ");
      	sbQuery.append("AND a.tipo_permiso= :tipoPermiso");
     	
	    SQLQuery query = getSession().createSQLQuery(sbQuery.toString());
	    query.setParameter("usuarioNombre", nombreUsuario);
	    query.setParameter("tipoPermiso", tipoPermiso.getId());
	    
	    
	    List<String> listObject = query.list();			  

		return listObject;
	}

}
