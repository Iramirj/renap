package mx.gob.segob.dgti.infraestructure.auditoria.business.service;

import mx.gob.segob.dgti.infraestructure.auditoria.vo.BitacoraVO;


public interface AuditoriaService {
	public void guardarBitacora(BitacoraVO bitacoraVO) throws Exception;
}
