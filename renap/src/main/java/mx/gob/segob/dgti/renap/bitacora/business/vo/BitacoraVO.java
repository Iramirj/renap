package mx.gob.segob.dgti.renap.bitacora.business.vo;

import java.util.Date;

public class BitacoraVO {

	private Integer idHBitacora;
	private Integer idMUsuario;
	private Integer idCEntFed;
	private Integer idCAccion;
	private Integer idMPoder;
	private Integer idUsuarioAlta;
	private Integer idUsuarioMod;
	private Integer idUsuarioBaja;
	private Integer idPerfil;
	private Date fechaAlta;
	private Date fechaMod;
	private Date fechaBaja;
	private String fechaDesde;
	private String fechaHasta;
	private String usuario;
	private String accion;
	private String fecha;
	private String hora;
	private String modulo;
	private String consulta;
	private String perfil;
	private String entidadFederativa;
	private String stRegistro;
	private String nombre;
	private String aPaterno;
	private String aMaterno;
	private String idNombreAccion;
	
	public Integer getIdHBitacora() {
		return idHBitacora;
	}
	public void setIdHBitacora(Integer idHBitacora) {
		this.idHBitacora = idHBitacora;
	}
	public Integer getIdMUsuario() {
		return idMUsuario;
	}
	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}
	public Integer getIdCEntFed() {
		return idCEntFed;
	}
	public void setIdCEntFed(Integer idCEntFed) {
		this.idCEntFed = idCEntFed;
	}
	public Integer getIdCAccion() {
		return idCAccion;
	}
	public void setIdCAccion(Integer idCAccion) {
		this.idCAccion = idCAccion;
	}
	public Integer getIdMPoder() {
		return idMPoder;
	}
	public void setIdMPoder(Integer idMPoder) {
		this.idMPoder = idMPoder;
	}
	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}
	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}
	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}
	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}
	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}
	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Date getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public String getConsulta() {
		return consulta;
	}
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}
	public String getStRegistro() {
		return stRegistro;
	}
	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
	public String getIdNombreAccion() {
		return idNombreAccion;
	}
	public void setIdNombreAccion(String idNombreAccion) {
		this.idNombreAccion = idNombreAccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getaPaterno() {
		return aPaterno;
	}
	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}
	public String getaMaterno() {
		return aMaterno;
	}
	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}
}
