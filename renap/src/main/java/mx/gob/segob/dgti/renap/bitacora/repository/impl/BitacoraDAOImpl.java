package mx.gob.segob.dgti.renap.bitacora.repository.impl;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.internal.OracleTypes;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;
import mx.gob.segob.dgti.renap.bitacora.repository.BitacoraDAO;

@Repository
public class BitacoraDAOImpl implements BitacoraDAO{

	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String insertarBitacora(BitacoraVO bitacora) {
	String mensajeError = "";
	String result = "";
	Integer resulId = null;
		try {
			CallableStatement cStmt2 = sessionFactory.openStatelessSession().connection()
				.prepareCall("{call PKG_RENAP.INS_H_BITACORA(?,?,?,?,?,?,?,?,?)}");
				
			cStmt2.setInt(1, bitacora.getIdMUsuario());
			cStmt2.setInt(2, bitacora.getIdCEntFed());
			cStmt2.setInt(3, bitacora.getIdCAccion());
			cStmt2.setString(4, bitacora.getAccion());
			cStmt2.setString(5, bitacora.getModulo());
			cStmt2.setString(6, bitacora.getConsulta());
			cStmt2.setInt(7, bitacora.getIdUsuarioAlta());
			
			cStmt2.registerOutParameter(8, Types.INTEGER);
			cStmt2.registerOutParameter(9, Types.VARCHAR);
				
			cStmt2.execute();

			resulId = cStmt2.getInt(8);
			mensajeError = cStmt2.getNString(9);
			
			cStmt2.close();
			
		} catch (Exception e) {
		e.printStackTrace();
		}
		if(resulId != null){
			result = resulId.toString();
		}else{
			result = "error";
		}
		return resulId.toString();
	}

	@Override
	public List<BitacoraVO> consultarBitacora(BitacoraVO bitacora) {
		
		List<BitacoraVO> lRegistros = new ArrayList<BitacoraVO>();
		BitacoraVO registro;
			
		try {
			CallableStatement cStmt2 = sessionFactory.openStatelessSession().connection()
					.prepareCall("{call PKG_RENAP.MUESTRA_H_BITACORA(?,?,?,?,?,?,?,?)}");
	
			if(bitacora.getIdMUsuario()!= null){
				cStmt2.setLong(1, bitacora.getIdMUsuario());
			}else{
				cStmt2.setNull(1, java.sql.Types.INTEGER);
			}
			
			if(bitacora.getIdPerfil()!= null){
				cStmt2.setLong(2, bitacora.getIdPerfil());
			}else{
				cStmt2.setNull(2, java.sql.Types.INTEGER);
			}
			
			if(bitacora.getIdCEntFed() != null){
				cStmt2.setLong(3, bitacora.getIdCEntFed());
			}else{
				cStmt2.setNull(3, java.sql.Types.INTEGER);
			}
			
			if(bitacora.getIdCAccion() !=  null){
				cStmt2.setLong(4, bitacora.getIdCAccion());
			}else{
				cStmt2.setNull(4, java.sql.Types.INTEGER);
			}
			
			if(bitacora.getFechaDesde()!= null){
				cStmt2.setString(5, bitacora.getFechaDesde());
			}else{
				cStmt2.setNull(5, java.sql.Types.VARCHAR);
			}
			
			if(bitacora.getFechaHasta()!= null){
				cStmt2.setString(6, bitacora.getFechaHasta());
			}else{
				cStmt2.setNull(6, java.sql.Types.VARCHAR);
			}
			
			cStmt2.registerOutParameter(7, OracleTypes.CURSOR); // REF CURSOR
			
			if(bitacora.getIdHBitacora()!= null){
				cStmt2.setInt(8, bitacora.getIdHBitacora());
			}else{
				cStmt2.setNull(8, java.sql.Types.INTEGER);
			}
			
			cStmt2.execute();

			ResultSet rset = (ResultSet) cStmt2.getObject(7);
			
			while (rset.next()) {
				registro = new BitacoraVO();
				registro.setIdHBitacora(rset.getInt("id_h_bitacora") != 0 ? Integer.parseInt(rset.getString("id_h_bitacora")):0);
				registro.setIdMUsuario(rset.getInt("id_m_usuario") != 0 ? Integer.parseInt(rset.getString("id_m_usuario")):0);
				registro.setUsuario(rset.getString("usuario") != null ? rset.getString("Usuario"): "Sin registro");
				registro.setIdPerfil(rset.getInt("id_c_perfil") != 0 ? Integer.parseInt(rset.getString("id_c_perfil")):0);
				registro.setPerfil(rset.getString("perfil") != null ? rset.getString("perfil") : "Sin registro");
				registro.setIdCEntFed(rset.getInt("id_c_ent_fed") != 0 ? Integer.parseInt(rset.getString("id_c_ent_fed")):0);
				registro.setEntidadFederativa(rset.getString("ent_fed") != null ? rset.getString("ent_fed"):"Sin registro");
				registro.setIdCAccion(rset.getInt("id_c_tipo_accion") != 0 ? Integer.parseInt(rset.getString("id_c_tipo_accion")):0);
				registro.setIdNombreAccion(rset.getString("tipo_accion") != null ? rset.getString("tipo_accion"): "Sin registro");
				registro.setAccion(rset.getString("accion") != null ? rset.getString("accion") : "Sin registro");
				registro.setFecha(rset.getString("fecha") != null ? rset.getString("fecha") : "Sin registro");
				registro.setHora(rset.getString("hora") != null ? rset.getString("hora"): "Sin registro");
				registro.setModulo(rset.getString("modulo") != null ? rset.getString("modulo"):"Sin registro");
				registro.setStRegistro(rset.getString("st_registro") != null ? rset.getString("st_registro"):"Sin registro");
				registro.setNombre(rset.getString("nombre") != null ? rset.getString("nombre") + " " + rset.getString("a_paterno") + " " + rset.getString("a_materno") :"Sin registro");
				lRegistros.add(registro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lRegistros.size() >0 ? lRegistros : null;
	}
}
