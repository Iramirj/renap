package mx.gob.segob.dgti.infraestructure.auditoria.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class BitacoraVO {

	private Integer idHBitacora;
	private Integer idMUsuario;
	private Integer idCEntFed;
	private Integer idCAccion;
	private String accion;
	private String fecha;
	private String hora;
	private String modulo;
	private String consulta;
	private Integer idMPoder;
	private Integer idUsuarioAlta;
	private Date fechaAlta;
	private Integer idUsuarioMod;
	private Date fechaMod;
	private Integer idUsuarioBaja;
	private Date fechaBaja;
	private String stRegistro;

	public Integer getIdHBitacora() {
		return idHBitacora;
	}

	public void setIdHBitacora(Integer idHBitacora) {
		this.idHBitacora = idHBitacora;
	}

	public Integer getIdMUsuario() {
		return idMUsuario;
	}

	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}

	public Integer getIdCEntFed() {
		return idCEntFed;
	}

	public void setIdCEntFed(Integer idCEntFed) {
		this.idCEntFed = idCEntFed;
	}

	public Integer getIdCAccion() {
		return idCAccion;
	}

	public void setIdCAccion(Integer idCAccion) {
		this.idCAccion = idCAccion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public Integer getIdMPoder() {
		return idMPoder;
	}

	public void setIdMPoder(Integer idMPoder) {
		this.idMPoder = idMPoder;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
}
