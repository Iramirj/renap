package mx.gob.segob.dgti.renap.correo.business.helper;

import java.util.ResourceBundle;


public class MensajeCorreoHelper {

	/** La constante rbCorreo. */
	private static final ResourceBundle rbCorreo = ResourceBundle
			.getBundle("bundles/views/UICorreo_es");

	/**
	 * Obtener mensaje resorce correo.
	 * 
	 * @param llaveMensaje
	 *            el llave mensaje
	 * @return el string
	 */
	public static String obtenerMensajeResorceCorreo(String llaveMensaje) {
		String mensaje = llaveMensaje;

		if (rbCorreo.containsKey(llaveMensaje)) {
			mensaje = rbCorreo.getString(llaveMensaje);
		}

		return mensaje;
	}
}
