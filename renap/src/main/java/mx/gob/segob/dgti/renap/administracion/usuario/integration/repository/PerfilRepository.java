
package mx.gob.segob.dgti.renap.administracion.usuario.integration.repository;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Perfil;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.BaseRepository;




public interface PerfilRepository extends BaseRepository{
	
	String ID_PERFIL ="idPerfil";
	String CLAVE_PERFIL="clavePerfil";
	String DESCRIPCION_PERFIL ="descripcion";
	String ESTATUS ="activo";
	
	List<Perfil> consultarPorPaginacion(PerfilVO perfilVO, Integer primerRegistro,
			Integer ultimoRegistro);

	Integer consultarTotal(PerfilVO perfilVO);

	List<Perfil> consultarPerfil(PerfilVO perfilVO);
	
	Perfil consultarPorIdPerfil(Integer idPerfil);
	
	boolean existe(Integer idPerfil, String nombre);

}
