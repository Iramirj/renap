package mx.gob.segob.dgti.infraestructure.autenticacion.business.constants;

public interface AutorizacionConstants {
	  public static final String ROL_NO_NECESARIO = "ROL_NO_NECESARIO";
	  public static final String TODOS_LOS_ROLES = "TODOS";
	  public static final String NINGUN_ROL = "NINGUNO";
}
