package mx.gob.segob.dgti.infraestructure.base.business.vo;

import java.util.HashMap;

public class DataTableSearch {
	private Integer draw;
	private Integer start;
	private Integer length;
	//Obtiene el numero de columna y si es ascendente o descendente
	private HashMap<String,Object>[] order;
	//Le indica al order el nombre de las columnas en la consulta hql
	//El posicionamiento debe coincidir con el definido en el datatable.
	private String[] columnas;
	
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public HashMap<String,Object>[] getOrder() { 
		return order;
	}
	public void setOrder(HashMap<String,Object>[] order) {
		this.order = order;
	}
	public String[] getColumnas() {
		return columnas;
	}
	public void setColumnas(String[] columnas) {
		this.columnas = columnas;
	}
	

}
