package mx.gob.segob.dgti.infraestructure.base.business.service.impl;


import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED)
public class BaseServiceImpl{
	protected final Logger logger = Logger.getLogger(getClass().getPackage().getName());
	
}
