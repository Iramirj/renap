package mx.gob.segob.dgti.infraestructure.autenticacion.business.constants;

public enum AutenticacionMotivoException {
	USUARIO_NO_VALIDO("USUARIO_NO_VALIDO"),
	USUARIO_INACTIVO("USUARIO_INACTIVO"),
	USUARIO_BLOQUEADO("USUARIO_BLOQUEADO"),
	USUARIO_EN_SESSION("USUARIO_SESSION"),
	USUARIO_NO_EXISTE("USUARIO_NO_EXISTE"),
	CONVERSION_PASSWORD("ERROR_CONVERSION_PASSWORD");
	
	private String codigoMensaje;
	
	AutenticacionMotivoException(String codigoMensaje){
		this.codigoMensaje = codigoMensaje;
	}
	
	public String getCodigoMensaje(){
		return this.codigoMensaje;
	}
	
}
