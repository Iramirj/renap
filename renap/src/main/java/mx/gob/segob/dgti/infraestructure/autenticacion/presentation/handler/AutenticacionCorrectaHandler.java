package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.handler;


import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.autenticate.AutenticacionUtil;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

public class AutenticacionCorrectaHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {
	
	@Autowired
	private AutenticacionService autentificacionService;
	@Autowired
	private AutenticacionUtil autenticacionUtil;
	
	
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {
		
		subirDetallesSession(request, authentication);
		registraUsuario(authentication);		
		
		getRedirectStrategy().sendRedirect(request, response, getDefaultTargetUrl());
		
	}
	
	
	private void subirDetallesSession(HttpServletRequest request, Authentication authentication) throws ServletException{
		UsuarioAutenticadoVO usuario = (UsuarioAutenticadoVO)authentication.getPrincipal();
		HttpSession session = request.getSession();
		
		if(autenticacionUtil == null)
		{
			throw new ServletException("Falta implementar bean que implementa autenticacionUtil ");
		}
		
		Map<String, Object> detalles = autenticacionUtil.obtenerDetallesPorSubirASession(usuario.getUsuario());
		if(detalles != null){
			Iterator<String> it =  detalles.keySet().iterator();
			while(it.hasNext()){
				String clave = it.next();
				session.setAttribute(clave, detalles.get(clave));
			}
		}
	}
	
	private void registraUsuario(Authentication authentication){
		UsuarioAutenticadoVO usuario = (UsuarioAutenticadoVO)authentication.getPrincipal();
		String usuarioId = usuario.getUsuario();
		autentificacionService.registrarUsuario(usuarioId);
	}
	
}
