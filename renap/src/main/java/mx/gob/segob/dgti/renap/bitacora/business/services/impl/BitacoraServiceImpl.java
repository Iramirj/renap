package mx.gob.segob.dgti.renap.bitacora.business.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;
import mx.gob.segob.dgti.renap.bitacora.business.services.BitacoraService;
import mx.gob.segob.dgti.renap.bitacora.repository.BitacoraDAO;

@Service
public class BitacoraServiceImpl implements BitacoraService{
	
	@Autowired 
	public BitacoraDAO bitacoraDAO;

	@Override
	@Transactional
	public String insertarBitacora(BitacoraVO bitacora) {
		return bitacoraDAO.insertarBitacora(bitacora);
	}

	@Override
	@Transactional
	public List<BitacoraVO> consultarBitacora(BitacoraVO bitacora) {
		return bitacoraDAO.consultarBitacora(bitacora);
	}
}
