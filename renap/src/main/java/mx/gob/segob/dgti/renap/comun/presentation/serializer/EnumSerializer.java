package mx.gob.segob.dgti.renap.comun.presentation.serializer;

import java.io.IOException;

import mx.gob.segob.dgti.infraestructure.base.presentation.converter.EnumConverter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class EnumSerializer extends JsonSerializer<Enum<? extends EnumConverter>> {

	@SuppressWarnings("rawtypes")
	@Override
	public void serialize( 	Enum value, 
							JsonGenerator jsonGenerator,
							SerializerProvider serializerProvider) 
		throws IOException,JsonProcessingException {
		EnumConverter conversionVO = (EnumConverter)value;
		if(value != null){
			jsonGenerator.writeStartObject();
			jsonGenerator.writeStringField("valor", value.toString());
			jsonGenerator.writeStringField("descripcionConverter", conversionVO.getDescripcionConverter());
			jsonGenerator.writeEndObject();	
		}		
	}

}
