package mx.gob.segob.dgti.infraestructure.auditoria.persistence.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.repository.BitacoraRepository;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.impl.BaseRepositoryImpl;

@Repository
public class BitacoraRepositoryImpl extends BaseRepositoryImpl implements
		BitacoraRepository {

	@Override
	public void guardarBitacora(BitacoraSistema entidad) throws Exception {
		StringBuilder query = new StringBuilder();
		query.append(" INSERT INTO h_bitacora ( ");
		query.append("id_m_usuario, ID_C_ENT_FED, ID_C_TIPO_ACCION, accion, fecha, modulo, fecha_alta, st_registro ");
		query.append(" ) VALUES (");
		query.append("  ?, ?, ?, ?, ?, ?, ?, ? ");
		query.append(" )");

		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter(0, entidad.getIdMUsuario());
		sqlQuery.setParameter(1, entidad.getIdCEntFed());
		sqlQuery.setParameter(2, entidad.getIdCAccion());
		sqlQuery.setParameter(3, entidad.getAccion());
		sqlQuery.setParameter(4, entidad.getFecha());
		sqlQuery.setParameter(5, entidad.getModulo());
		sqlQuery.setParameter(6, entidad.getFechaAlta());
		sqlQuery.setParameter(7, entidad.getStRegistro());

		sqlQuery.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<BitacoraSistema> getBitacora(Integer usuarioId) {
		Criteria crt = getSession().createCriteria(BitacoraSistema.class);

		List<BitacoraSistema> lista = crt.add(
				Restrictions.eq("idHBitacora", usuarioId)).list();
		return lista;
	}
}
