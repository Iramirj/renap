
package mx.gob.segob.dgti.infraestructure.base.presentation.helper;
import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * Clase para obtener los mensajes del ResourceBundle "mensajes".

 */

public class MensajeHelper {

	/** La constante rb. */

	private static final ResourceBundle rb = ResourceBundle
			.getBundle("bundles.views.UIMessages");
	
	/** La constante rbProp. */
	private static final ResourceBundle rbProp = ResourceBundle
			.getBundle("config.properties.configuracion");
	
	/** La constante rbValidacion. */
	private static final ResourceBundle rbValidacion = ResourceBundle
			.getBundle("bundles.views.UIValidation");


	/**
	 * Crear el mensaje segun MessageFormat.format(rb.getString(clave), args),
	 * donde rb es el ResourceBundle
	 * 
	 * @param clave
	 *            el clave
	 * @param args
	 *            el args
	 * @return el string modificado
	 */
	public static String crearMensaje(String clave, Object... args) {
		String msj = rb.getString(clave);
		return MessageFormat.format(msj, args);
	}

	/**
	 * <p>
	 * Obtener el mensaje del ResourceBundle con clave llaveMensaje
	 * <p>
	 * En caso de no haber ningun mensaje, retorna el original (llaveMensaje).
	 * 
	 * @param llaveMensaje
	 *            el llave mensaje
	 * @return el String correspondiente al mensaje llaveMensaje
	 */
	public static String obtenerMensajeResorce(String llaveMensaje) {
		String mensaje = llaveMensaje;

		if (rb.containsKey(llaveMensaje)) {
			mensaje = rb.getString(llaveMensaje);
		}

		return mensaje;
	}

	/**
	 * Obtener el mensaje correspondiente a la clave llaveMensaje y sustituir
	 * los mensajes correspondientes a las claves de parametros en este, segun
	 * el indice (parametros[i] corresponde al indice i, {0} se sustituye por
	 * obtenerMensajeResorce(parametros[0]), etc ).
	 * 
	 * @param llaveMensaje
	 *            la clave del mensaje
	 * @param parametros
	 *            array variable de los parametros
	 * @return el mensaje
	 */
	public static String obtenerMensaje(String llaveMensaje,
			String... parametros) {
		String mensajeFinal = obtenerMensajeResorce(llaveMensaje);

		if (!mensajeFinal.isEmpty() && parametros != null) {
			String parametro;
			for (int indice = 0; indice < parametros.length; indice++) {
				parametro = obtenerMensajeResorce(parametros[indice]);
				mensajeFinal = mensajeFinal.replace("{" + indice + "}",
						parametro);
			}
		}

		return mensajeFinal;
	}

	/**
	 * Obtener el mensaje correspondiente a la clave llaveMensaje y sustituir
	 * los parametros en este, segun el indice (parametros[i] corresponde al
	 * indice i, {0} se sustituye por parametros[0], etc ).
	 * 
	 * @param llaveMensaje
	 *            la clave del mensaje
	 * @param parametros
	 *            array variable de los parametros
	 * @return el mensaje
	 */
	public static String obtenerMensajeNoLlaves(String llaveMensaje,
			String... parametros) {
		String mensajeFinal = obtenerMensajeResorce(llaveMensaje);

		if (!mensajeFinal.isEmpty() && parametros != null) {
			for (int indice = 0; indice < parametros.length; indice++) {
				mensajeFinal = mensajeFinal.replace("{" + indice + "}",
						parametros[indice]);
			}
		}

		return mensajeFinal;
	}

	/**
	 * Obtiene el mensaje correspondiente a la llaveMensaje del archivo
	 * config.properties de configuracion de Spring
	 * 
	 * @param llaveMensaje
	 *            llave mensaje
	 * @return string
	 */
	public static String obtenerPropiedadSpring(String llaveMensaje) {
		String mensaje = llaveMensaje;
		if (rbProp.containsKey(llaveMensaje)) {
			mensaje = rbProp.getString(llaveMensaje);
		}
		return mensaje;
	}
	
	/**
	 * <p>
	 * Obtener el mensaje del ResourceBundle de Validacion con clave llaveValidacion
	 * <p>
	 * En caso de no haber ningun mensaje, retorna el original (llaveValidacion).
	 * 
	 * @param llaveMensaje
	 *            el llave mensaje
	 * @return el String correspondiente al mensaje llaveMensaje
	 */
	public static String obtenerMensajeValidacion(String llaveValidacion) {
		String mensaje = llaveValidacion;

		if (rbValidacion.containsKey(llaveValidacion)) {
			mensaje = rbValidacion.getString(llaveValidacion);
		}

		return mensaje;
	}
	
	/**
	 * Obtener el mensaje correspondiente a la clave llaveMensaje y sustituir
	 * los mensajes correspondientes a las claves de parametros en este, segun
	 * el indice (parametros[i] corresponde al indice i, {0} se sustituye por
	 * obtenerMensajeResorce(parametros[0]), etc ).
	 * 
	 * @param llaveMensaje
	 *            la clave del mensaje
	 * @param parametros
	 *            array variable de los parametros
	 * @return el mensaje
	 */
	public static String obtenerMensajeValidacion(String llaveMensaje,
			String... parametros) {
		String mensajeFinal = obtenerMensajeValidacion(llaveMensaje);

		if (!mensajeFinal.isEmpty() && parametros != null) {
			String parametro;
			for (int indice = 0; indice < parametros.length; indice++) {
				parametro = obtenerMensajeValidacion(parametros[indice]);
				mensajeFinal = mensajeFinal.replace("{" + indice + "}",
						parametro);
			}
		}

		return mensajeFinal;
	}
}
