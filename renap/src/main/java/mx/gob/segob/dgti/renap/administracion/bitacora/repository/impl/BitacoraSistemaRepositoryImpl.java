package mx.gob.segob.dgti.renap.administracion.bitacora.repository.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.BusquedaBitacoraVO;
import mx.gob.segob.dgti.renap.administracion.bitacora.repository.BitacoraSistemaRepository;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.base.integration.exception.RepositoryException;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.impl.BaseRepositoryImpl;


@Repository
public class BitacoraSistemaRepositoryImpl extends BaseRepositoryImpl implements BitacoraSistemaRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<BitacoraSistema> obtenerBitacoras(BusquedaBitacoraVO parametroBusqueda,
			Integer registroInicio, Integer numeroRegistros)
			throws RepositoryException {
		List<BitacoraSistema> bitacorasSistema = null;
		Criteria criteria = null;
		
		criteria = generaCriteriaBusqueda(parametroBusqueda);
		criteria.setFirstResult(registroInicio);
		criteria.setMaxResults(numeroRegistros);		
		
		bitacorasSistema = criteria.list();
		
		return bitacorasSistema;
	}
	
	@Override
	public Integer obtenerTotalBitacoras(BusquedaBitacoraVO parametroBusqueda)
			throws RepositoryException {
		Long total = null;
		Criteria criteria = null;
		
		criteria = generaCriteriaBusqueda(parametroBusqueda);
		criteria.setProjection(Projections.count(ID_BITACORA_COLUMNA));
		
		total = (Long)criteria.uniqueResult();
		
		return total.intValue();
	}
	
	public Criteria generaCriteriaBusqueda(BusquedaBitacoraVO parametroBusqueda){
		Criteria criteria = null;
		Calendar fecha = null;
		Date fechaInicio = null;
		Date fechaFin = null;
		
		criteria = getSession().createCriteria(BitacoraSistema.class);
		if(parametroBusqueda != null) {
			if(StringUtils.isNotEmpty(parametroBusqueda.getIdUsuario())){
				criteria.add(Restrictions.ilike(ID_USUARIO_COLUMNA, 
							parametroBusqueda.getIdUsuario().toUpperCase(), 
							MatchMode.ANYWHERE));
			}
			
			if(parametroBusqueda.getFechaInicio() != null) {
				fecha = Calendar.getInstance();
				fecha.setTime(parametroBusqueda.getFechaInicio());
				fecha.set(Calendar.HOUR, 0);
				fecha.set(Calendar.MINUTE, 0);
				fecha.set(Calendar.MILLISECOND, 0);
				fechaInicio = fecha.getTime();	
				
				criteria.add(Restrictions.ge(FECHA_HORA_COLUMNA, fechaInicio));
			}	
			
			if(parametroBusqueda.getFechaFin() != null) {
				fecha = Calendar.getInstance();
				fecha.setTime(parametroBusqueda.getFechaFin());
				fecha.set(Calendar.HOUR, 23);
				fecha.set(Calendar.MINUTE, 59);
				fecha.set(Calendar.MILLISECOND,59);
				fechaFin = fecha.getTime();	
				
				criteria.add(Restrictions.ge(FECHA_HORA_COLUMNA, fechaFin));
			}				
		}
		
		return criteria;
	}

}
