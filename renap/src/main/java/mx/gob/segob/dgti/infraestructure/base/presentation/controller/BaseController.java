package mx.gob.segob.dgti.infraestructure.base.presentation.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import mx.gob.segob.dgti.infraestructure.base.business.constants.AnotacionEnum;
import mx.gob.segob.dgti.infraestructure.base.business.constants.EstatusRequestEnum;
import mx.gob.segob.dgti.infraestructure.base.business.vo.MensajeRespuestaVO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseController {

	protected Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private Validator validator;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MensajeRespuestaVO validar(Object objectoAValidar,
			Class... claseValidacion) {
		MensajeRespuestaVO mensajeRespuesta = new MensajeRespuestaVO();
		mensajeRespuesta.setEstatus(EstatusRequestEnum.SUCESSFUL);
		Set erroresConstraint;
		if(claseValidacion!=null){
			erroresConstraint=validator.validate(objectoAValidar, claseValidacion);
		}else{
			erroresConstraint= validator.validate(objectoAValidar);
		}
		
		Map<String, String> errores = obtenerMensajes(erroresConstraint);
		if (!errores.isEmpty()) {
			mensajeRespuesta.setEstatus(EstatusRequestEnum.ERROR);
			for (Entry<String, String> error : errores.entrySet()) {
				mensajeRespuesta
						.addErrorCampo(error.getKey(), error.getValue());
			}
		}
		/*Se utiliza en el controller para decidir que mostrar en caso de que existan errores*/
		mensajeRespuesta.setErrores(errores);
		return mensajeRespuesta;
	}

	public <V> Map<String, String> obtenerMensajes(
			Set<ConstraintViolation<V>> constraintViolations) {
		Map<String, String> mapa = new HashMap<String, String>();
		Map<String, String> mapaAnotacion = new HashMap<String, String>();

		Iterator<ConstraintViolation<V>> iterator = constraintViolations
				.iterator();

		while (iterator.hasNext()) {
			ConstraintViolation<?> y = iterator.next();

			String mensaje = mapa.get(y.getPropertyPath().toString());
			String anotacion = mapaAnotacion
					.get(y.getPropertyPath().toString());
			String ultimoMensaje = null;
			String ultimaAnotacion = null;

			if (mensaje == null) {
				mensaje = y.getMessage().toString();
				anotacion = y.getConstraintDescriptor().getAnnotation()
						.annotationType().getName();
				ultimoMensaje = mensaje;
				ultimaAnotacion = anotacion;
			} else {
				ultimoMensaje = obtenerMensajeError(mensaje, y.getMessage()
						.toString(), anotacion, y.getConstraintDescriptor()
						.getAnnotation().annotationType().getName());
				ultimaAnotacion = obtenerAnotacion(anotacion, y
						.getConstraintDescriptor().getAnnotation()
						.annotationType().getName());
			}
			// erroles.add(y.getMessage().toString());
			mapa.put(y.getPropertyPath().toString(), ultimoMensaje);
			mapaAnotacion.put(y.getPropertyPath().toString(), ultimaAnotacion);

		}

		return mapa;
	}

	private String obtenerMensajeError(String errorAnterior, String errorNuevo,
			String anotacionAnterior, String anotacionNueva) {

		int secuenciaAnterior = AnotacionEnum.obtener(anotacionAnterior)
				.getSecuencia();
		int secuenciaNueva = AnotacionEnum.obtener(anotacionNueva)
				.getSecuencia();

		if (secuenciaAnterior < secuenciaNueva) {
			return errorAnterior;
		}

		return errorNuevo;
	}

	private String obtenerAnotacion(String anotacionAnterior,
			String anotacionNueva) {

		int secuenciaAnterior = AnotacionEnum.obtener(anotacionAnterior)
				.getSecuencia();
		int secuenciaNueva = AnotacionEnum.obtener(anotacionNueva)
				.getSecuencia();

		if (secuenciaAnterior < secuenciaNueva) {
			return anotacionAnterior;
		}

		return anotacionNueva;
	}

	public ResponseEntity<MensajeRespuestaVO> regresarErrores(
			Map<String, String> mapa, MensajeRespuestaVO respuesta) {

		respuesta.setEstatus(EstatusRequestEnum.ERROR);
		for (Entry<String, String> error : mapa.entrySet()) {
			respuesta.addErrorCampo(error.getKey(), error.getValue());
		}
		return new ResponseEntity<MensajeRespuestaVO>(respuesta,
				HttpStatus.BAD_REQUEST);

	}

}
