package mx.gob.segob.dgti.renap.administracion.usuario.business.service.impl;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.builder.PerfilBuilder;
import mx.gob.segob.dgti.renap.administracion.usuario.business.exception.AdministracionException;
import mx.gob.segob.dgti.renap.administracion.usuario.business.service.PerfilService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Perfil;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.PerfilRepository;
import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;
import mx.gob.segob.dgti.infraestructure.base.business.service.impl.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PerfilServiceImpl extends BaseServiceImpl implements PerfilService {

	@Autowired
	PerfilRepository perfilRepository;
	PerfilBuilder perfilBuilder = new PerfilBuilder();
	
	@Override
	public List<PerfilVO> consultarPorPaginancion(PerfilVO perfilVO,
			int primerRegistro, int ultimoRegistro) {
		List<PerfilVO> listaVO = null;
		List<Perfil> listaPerfil = null;
		listaPerfil = perfilRepository.consultarPorPaginacion(perfilVO, primerRegistro,
				ultimoRegistro);
		if (listaPerfil != null) {
			listaVO = perfilBuilder.convertirListaEntidad(listaPerfil);
		}
		return listaVO;
	}

	@Override
	public Integer consultarTotal(PerfilVO perfilVO) {
		int total = 0;
		total = perfilRepository.consultarTotal(perfilVO);
		return total;
	}

	@Override
	public void guardar(PerfilVO perfilVO) throws AdministracionException {
		Perfil perfil = perfilBuilder.convertirVOAEntidad(perfilVO);
		try {
			perfilRepository.guardarModificar(perfil);
		} catch (Exception exception) {
			throw new AdministracionException(
					ExcepcionCausaEnum.COMUN_ERROR_GUARDAR,
					exception.getCause());
		}

		
	}

	@Override
	public List<PerfilVO> buscarPerfil(PerfilVO perfilVO) {
		List<PerfilVO> listaVO = null;
		List<Perfil> listaEntidad = null;
		listaEntidad = perfilRepository.consultarPerfil(perfilVO);
		if (listaEntidad != null) {
			listaVO = perfilBuilder.convertirListaEntidad(listaEntidad);
		}
		return listaVO;	
	}
	
	@Override
	public boolean existe(Integer idPerfil, String nombre) {
		return perfilRepository.existe(idPerfil, nombre);
	}

	@Override
	public PerfilVO consultarPorId(Integer idPerfil) {
		PerfilVO perfilVO = null;
		Perfil entidad = null;
		entidad = perfilRepository.consultarPorIdPerfil(idPerfil);
		if (entidad != null) {
			perfilVO = perfilBuilder.convertirEntidadAVO(entidad);
		}
		return perfilVO;	
	}

}
