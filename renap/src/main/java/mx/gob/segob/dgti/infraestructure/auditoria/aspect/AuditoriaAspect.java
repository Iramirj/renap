package mx.gob.segob.dgti.infraestructure.auditoria.aspect;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import mx.gob.segob.dgti.infraestructure.auditoria.annotation.Auditable;
import mx.gob.segob.dgti.infraestructure.auditoria.business.service.AuditoriaService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;
import mx.gob.segob.dgti.renap.bitacora.business.services.BitacoraService;
import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;

@Aspect
public class AuditoriaAspect {

	private AuditoriaService auditoriaService;
	
	@Autowired
	private BitacoraService flBitacoraService;

	public AuditoriaService getAuditoriaService() {
		return auditoriaService;
	}

	public void setAuditoriaService(AuditoriaService auditoriaService) {
		this.auditoriaService = auditoriaService;
	}

	@Around(value = "execution(public * *(..)) && @annotation(mx.gob.segob.dgti.infraestructure.auditoria.annotation.Auditable)")
	public synchronized void audit(ProceedingJoinPoint pjp) throws Throwable {
		Object result = pjp.proceed();
		saveBitacora(pjp, result);
	}

	@SuppressWarnings("rawtypes")
	public void saveBitacora(JoinPoint joinPoint, Object result)
			throws SecurityException, NoSuchMethodException {

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String methodName = signature.getMethod().getName();
		Class[] parameterTypes = signature.getMethod().getParameterTypes();

		Method method = joinPoint.getTarget().getClass()
				.getMethod(methodName, parameterTypes);
		if (method.isAnnotationPresent(Auditable.class)) {
			Auditable auditable = (Auditable) method
					.getAnnotation(Auditable.class);
			String strModulo = auditable.modulo();
			UsuarioAutenticadoVO usuarioVO = getUsuarioAutenticado();

			BitacoraVO bitacoraVO = new BitacoraVO();
			String ipAddress = null;
			try {
				ipAddress = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}

			bitacoraVO.setIdMUsuario(usuarioVO.getIdUsuario());
			bitacoraVO.setIdCAccion(1); //Recuperar esta informacion del detalle del usuario
			bitacoraVO.setIdCEntFed(2); //Recuperar esta informacion del detalle del usuario
			bitacoraVO.setAccion("El usuario " + usuarioVO.getUsuario() + " entro al sistema desde la ip: " + ipAddress);
			bitacoraVO.setModulo(strModulo);
			bitacoraVO.setConsulta("empty");
			bitacoraVO.setIdUsuarioAlta(usuarioVO.getIdUsuario());

			try {
				System.out.println("prueba");
				flBitacoraService.insertarBitacora(bitacoraVO);
			} catch (Exception ae) {
				Logger logger = Logger.getLogger("ERROR");
				logger.error(ae, ae);
			}
		}
	}

	private UsuarioAutenticadoVO getUsuarioAutenticado() {
		UsuarioAutenticadoVO usuario = (UsuarioAutenticadoVO) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		return usuario;
	}
}
