package mx.gob.segob.dgti.infraestructure.base.presentation.converter;

import mx.gob.segob.dgti.renap.comun.presentation.serializer.EnumSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using=EnumSerializer.class)
public interface EnumConverter {
	String getDescripcionConverter();
}
