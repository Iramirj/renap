package mx.gob.segob.dgti.renap.catalogos.business.service;

import java.util.List;

import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CMunicipio;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CPais;

public interface UbicacionService {
	public List<CPais> getList();
	public List<CEntFed> getListByIdPais(Integer idPais);
	public List<CMunicipio> getListCMunicipioByIdEntifdad(Integer idEntidad);
}
