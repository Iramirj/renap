package mx.gob.segob.dgti.renap.bitacora.business.services;

import java.util.List;

import mx.gob.segob.dgti.renap.bitacora.business.vo.BitacoraVO;

public interface BitacoraService {
	public String insertarBitacora (BitacoraVO bitacora);
	public List<BitacoraVO> consultarBitacora(BitacoraVO bitacora);
}
