package mx.gob.segob.dgti.infraestructure.base.presentation.helper;

import javax.servlet.http.HttpServletRequest;

import mx.gob.segob.dgti.renap.comun.business.constants.SessionConstants;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class WebApplicationHelper extends AbstractBaseHelper implements
		SessionConstants {
	
	public static String realPathResource(String pathResource) {

		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		String realPath = servletRequestAttributes.getRequest().getSession()
				.getServletContext().getRealPath(pathResource);
		return realPath;
	}

	public static Object getObjectSession(String sessionId) {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		Object object = servletRequestAttributes.getRequest().getSession()
				.getAttribute(sessionId);
		return object;
	}

	public static void putObjectSession(String sessionId, Object value) {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		servletRequestAttributes.getRequest().getSession()
				.setAttribute(sessionId, value);
	}

	public static void removeObjectSession(String sessionId) {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		servletRequestAttributes.getRequest().getSession()
				.removeAttribute(sessionId);
	}
	
	public static HttpServletRequest getRequest(){
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		HttpServletRequest servletRequest = servletRequestAttributes.getRequest();
		return servletRequest;
	}
	
	public static UsuarioAutenticadoVO getUsuarioSession(){
		UsuarioAutenticadoVO usuario = (UsuarioAutenticadoVO) getObjectSession(
									WebApplicationHelper.USUARIO);
		return usuario;
	}
}
