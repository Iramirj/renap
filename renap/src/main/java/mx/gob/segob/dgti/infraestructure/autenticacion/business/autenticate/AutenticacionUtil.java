package mx.gob.segob.dgti.infraestructure.autenticacion.business.autenticate;

import java.util.Map;


public interface AutenticacionUtil {
	Map<String, Object>  obtenerDetallesPorSubirASession(String usuarioId);
}
