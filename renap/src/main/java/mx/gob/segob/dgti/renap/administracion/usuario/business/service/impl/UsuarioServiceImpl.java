package mx.gob.segob.dgti.renap.administracion.usuario.business.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.gob.segob.dgti.renap.administracion.usuario.business.builder.UsuarioBuilder;
import mx.gob.segob.dgti.renap.administracion.usuario.business.exception.AdministracionException;
import mx.gob.segob.dgti.renap.administracion.usuario.business.rule.ContraseniaRule;
import mx.gob.segob.dgti.renap.administracion.usuario.business.service.UsuarioService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.UsuarioRepository;
import mx.gob.segob.dgti.renap.correo.business.constants.CorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.constants.ParametrosCorreoEnum;
import mx.gob.segob.dgti.renap.correo.business.service.NotificacionService;
import mx.gob.segob.dgti.infraestructure.base.business.constants.ExcepcionCausaEnum;
import mx.gob.segob.dgti.infraestructure.base.business.service.impl.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioServiceImpl extends BaseServiceImpl implements
		UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private NotificacionService notificacionService;
	private final UsuarioBuilder usuarioBuilder = new UsuarioBuilder();

	@Override
	public List<UsuarioVO> buscar(UsuarioVO usuarioVO, Integer primerRegistro,
			int ultimoRegistro) {
		List<UsuarioVO> usuariosVO = null;
		List<Usuario> usuarios = usuarioRepository.buscar(usuarioVO,
				primerRegistro, ultimoRegistro);
		usuariosVO = usuarioBuilder.convertirListaEntidad(usuarios);
		return usuariosVO;
	}

	@Override
	public Integer buscarTotal(UsuarioVO usuarioVO) {
		int total = 0;
		total = usuarioRepository.buscarTotal(usuarioVO);
		return total;
	}

	@Override
	public UsuarioVO obtenerPorId(String usuarioId) {
		Usuario usuario = usuarioRepository.obtenerPorId(usuarioId);
		UsuarioVO usuarioVO = usuarioBuilder.convertirEntidadAVO(usuario);
		return usuarioVO;
	}

	@Override
	public void guardarNuevo(UsuarioVO usuarioVO)
			throws AdministracionException {
		String password = new String();
		password = ContraseniaRule.generarContrasenia();
		try {
			String hashPassword = ContraseniaRule.encriptar(password);
			usuarioVO.setPassword(hashPassword);
		} catch (NoSuchAlgorithmException excepcion) {
			throw new AdministracionException(
					ExcepcionCausaEnum.COMUN_ERROR_GENERAR_CONTRASENIA,
					excepcion.getCause());
		}

		try {
			Usuario usuario = usuarioBuilder.convertirVOAEntidad(usuarioVO);
			usuarioRepository.guardarModificar(usuario);
			// envio de correo al nuevo usuario
			enviarCorreoNuevoUsuario(usuarioVO);

		} catch (Exception exception) {

			exception.printStackTrace();
			throw new AdministracionException(
					ExcepcionCausaEnum.COMUN_ERROR_GUARDAR,
					exception.getCause());
		}

	}

	@Override
	public void editar(UsuarioVO usuarioVO) {
		Usuario entidad = usuarioBuilder.convertirVOAEntidad(usuarioVO);
		try {

			usuarioRepository.guardarModificar(entidad);
		} catch (Exception exception) {
			exception.printStackTrace();
		}

	}

	@Override
	public boolean existe(UsuarioVO usuarioVO) {
		return usuarioRepository.existe(usuarioVO);
	}

	@Override
	public void guardar(UsuarioVO usuarioVO) {

		Usuario usuario = usuarioBuilder.convertirVOAEntidad(usuarioVO);
		try {

			usuarioRepository.guardarModificar(usuario);

		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void reiniciarContrasena(UsuarioVO usuarioVO)
			throws AdministracionException {
		String password = new String();
		password = ContraseniaRule.generarContrasenia();
		try {
			String hashPassword = ContraseniaRule.encriptar(password);
			usuarioVO.setPassword(hashPassword);
		} catch (NoSuchAlgorithmException excepcion) {
			throw new AdministracionException(
					ExcepcionCausaEnum.COMUN_ERROR_GENERAR_CONTRASENIA,
					excepcion.getCause());
		}
		Usuario usuario = usuarioBuilder.convertirVOAEntidad(usuarioVO);
		try {
			usuarioRepository.guardarModificar(usuario);
			enviarCorreoCambioPass(usuarioVO, password);

		} catch (Exception exception) {
			throw new AdministracionException(
					ExcepcionCausaEnum.COMUN_ERROR_GUARDAR,
					exception.getCause());
		}

	}

	@Override
	public List<String> obtenerCorreosUsuarios(UsuarioVO usuarioVO) {

		Set<String> correos = null;
		List<Usuario> usuarios = usuarioRepository.buscar(usuarioVO);
		if (usuarios != null) {
			correos = new HashSet<String>();
			for (Usuario usuario : usuarios) {
				if (usuario.getUsuarioDatos() != null
						&& usuario.getUsuarioDatos().getCorreo() != null
						&& !usuario.getUsuarioDatos().getCorreo()
								.isEmpty()) {
					correos.add(usuario.getUsuarioDatos()
							.getCorreo().trim());
				}
			}
		}
		List<String> correosList = (correos != null) ? new ArrayList<String>(
				correos) : null;
		return correosList;
	}

	private void enviarCorreoNuevoUsuario(UsuarioVO usuarioVO) {

		// parametros para envio de correo
		Map<ParametrosCorreoEnum, String> parametros = new HashMap<ParametrosCorreoEnum, String>();
		CorreoEnum notificacionEnum = CorreoEnum.NUEVO_USUARIO;
		parametros.put(ParametrosCorreoEnum.USER_NAME,
				usuarioVO.getNombreUsuario());
		parametros.put(ParametrosCorreoEnum.USER_PASSWORD,
				usuarioVO.getPassword());

		notificacionService.notificar(notificacionEnum, parametros, usuarioVO
				.getUsuarioDatosVO().getCorreo());
	}

	private void enviarCorreoCambioPass(UsuarioVO usuarioVO, String password) {

		// parametros para envio de correo de nuevo password
		Map<ParametrosCorreoEnum, String> parametros = new HashMap<ParametrosCorreoEnum, String>();
		CorreoEnum notificacionEnum = CorreoEnum.REINICIO_CONTRASENIA_USUARIO;
		parametros
				.put(ParametrosCorreoEnum.USER_NAME, usuarioVO.getNombreUsuario());
		parametros.put(ParametrosCorreoEnum.USER_PASSWORD, password);

		notificacionService.notificar(notificacionEnum, parametros, usuarioVO
				.getUsuarioDatosVO().getCorreo());
	}

	public List<UsuarioVO> buscar(UsuarioVO usuarioVO) {
		List<Usuario> usuarios = usuarioRepository.buscar(usuarioVO);
		return usuarioBuilder.convertirListaEntidad(usuarios);
	}

	@Override
	public UsuarioVO obtenerPorIdSession(String usuarioId) {
		Usuario usuario = usuarioRepository.obtenerPorId(usuarioId);
		UsuarioVO usuarioVO = usuarioBuilder.convertirUsuarioSession(usuario);
		return usuarioVO;
	}
	
	@Override
	@Transactional
	public List<Usuario> getListUserByIdUserLogeo(Integer idUsuario){	
		return usuarioRepository.getListUserByIdUserLogeo(idUsuario);
	}
		@Override
	public Integer getPerfilByIdUsuario(Integer idUsuario){
		return usuarioRepository.getPerfilByIdUsuario(idUsuario);	}
}
