package mx.gob.segob.dgti.renap.administracion.usuario.business.vo;

import mx.gob.segob.dgti.renap.comun.business.constants.PermisoEnum;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;


public class PermisoVO {
	
	/** id permiso. */
	private PermisoEnum idPermiso;
	
	/** descripcion. */
	private String descripcion;
	
	/** tipo permiso. */
	private TipoPermisoEnum tipoPermiso;
	
	/**
	 * Obtiene id permiso.
	 *
	 * @return id permiso
	 */
	public PermisoEnum getIdPermiso() {
		return idPermiso;
	}
	
	/**
	 * Establece id permiso.
	 *
	 * @param idPermiso el nuevo id permiso
	 */
	public void setIdPermiso(PermisoEnum idPermiso) {
		this.idPermiso = idPermiso;
	}
	
	/**
	 * Obtiene descripcion.
	 *
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	/**
	 * Establece descripcion.
	 *
	 * @param descripcion el nuevo descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene tipo permiso.
	 *
	 * @return tipo permiso
	 */
	public TipoPermisoEnum getTipoPermiso() {
		return tipoPermiso;
	}

	/**
	 * Establece tipo permiso.
	 *
	 * @param tipoPermiso el nuevo tipo permiso
	 */
	public void setTipoPermiso(TipoPermisoEnum tipoPermiso) {
		this.tipoPermiso = tipoPermiso;
	}
	
	

}
