package mx.gob.segob.dgti.renap.home.integration.repository.impl;


import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.home.integration.repository.HomeDao;

@Repository
public class HomeDaoImpl implements HomeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Usuario getDetalleById(Integer flIdUsuario) {
		Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				Usuario.class);
		cr.add(Restrictions.eq("idUsuario", flIdUsuario));
		cr.setProjection(
				Projections
						.projectionList()
						.add(Projections.property("idUsuario"), "idUsuario")
						.add(Projections.property("usuario"), "usuario")
						.add(Projections.property("password"), "password")
						.add(Projections.property("estatus"), "estatus")
						.add(Projections.property("logueado"), "logueado")						
						.add(Projections.property("ultimoAcceso"), "ultimoAcceso")
						.add(Projections.property("numeroIntentos"), "numeroIntentos")	
						.add(Projections.property("bloqueado"), "bloqueado")
						.add(Projections.property("primeraVez"), "primeraVez")
						.add(Projections.property("tipoNacionalidad"), "tipoNacionalidad")
						.add(Projections.property("idUsuarioAlta"), "idUsuarioAlta")
						.add(Projections.property("fechaAlta"), "fechaAlta")
						.add(Projections.property("stRegistro"), "stRegistro"))
						.setResultTransformer(
				Transformers.aliasToBean(Usuario.class));

		return (Usuario) cr.uniqueResult();
	}

	@Override
	public Boolean changePasswordById(Usuario objUsuario) {
		if (objUsuario.getIdUsuario() != null) {
			sessionFactory.getCurrentSession().saveOrUpdate(objUsuario);
			return true;
		} else {
			return false;
		}
	}
}
