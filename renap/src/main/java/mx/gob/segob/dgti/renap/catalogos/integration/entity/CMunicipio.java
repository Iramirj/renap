package mx.gob.segob.dgti.renap.catalogos.integration.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "c_municipio")
public class CMunicipio implements java.io.Serializable{

	private static final long serialVersionUID = 6862492579137664963L;

	@Id
	@Column(name = "ID_C_MUNICIPIO", unique = true, nullable = false, length = 64)
	private Integer idCMunicipio;
	@Column(name = "MUNICIPIO")
	private String municipio;
	@Column(name = "MUNICIPIO_SIGLAS")
	private String municipioSiglas;
	@Column(name = "TIPO_MUN")
	private String tipoNum;
	@Column(name = "ID_C_ENT_FED")
	private Integer idCEntFed;
	@Column(name = "id_usuario_alta")
	private Integer idUsuarioAlta;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	@Column(name = "id_usuario_mod")
	private Integer idUsuarioMod;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_mod")
	private Date fechaMod;
	@Column(name = "id_usuario_baja")
	private Integer idUsuarioBaja;
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_baja")
	private Date fechaBaja;
	@Column(name = "st_registro")
	private String stRegistro;
	
	public CMunicipio() {

	}

	public CMunicipio(Integer idCMunicipio) {
		this.idCMunicipio = idCMunicipio;
	}
	
	public Integer getIdCMunicipio() {
		return idCMunicipio;
	}
	public void setIdCMunicipio(Integer idCMunicipio) {
		this.idCMunicipio = idCMunicipio;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getMunicipioSiglas() {
		return municipioSiglas;
	}
	public void setMunicipioSiglas(String municipioSiglas) {
		this.municipioSiglas = municipioSiglas;
	}
	public String getTipoNum() {
		return tipoNum;
	}
	public void setTipoNum(String tipoNum) {
		this.tipoNum = tipoNum;
	}
	public Integer getIdCEntFed() {
		return idCEntFed;
	}
	public void setIdCEntFed(Integer idCEntFed) {
		this.idCEntFed = idCEntFed;
	}
	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}
	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}
	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}
	public Date getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}
	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}
	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public String getStRegistro() {
		return stRegistro;
	}
	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

}
