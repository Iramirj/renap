package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.listener;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;


public class AutenticacionListener implements ApplicationListener<ApplicationEvent> {

	@Autowired
	AutenticacionService autenticacionService;
	
	
	public void onApplicationEvent(ApplicationEvent applicationEvent) {
		
		if(applicationEvent instanceof HttpSessionDestroyedEvent){ //Cuando se inahibilita los usuarios
			HttpSessionDestroyedEvent sessionDestroyed = (HttpSessionDestroyedEvent)applicationEvent;
			for(SecurityContext context : sessionDestroyed.getSecurityContexts()){
				Authentication autenticacion = context.getAuthentication();
				if(autenticacion instanceof UsernamePasswordAuthenticationToken 
						&& autenticacion != null){
					UsuarioAutenticadoVO usuarioVO = (UsuarioAutenticadoVO)autenticacion.getPrincipal();
					autenticacionService.anularUsuarioSession(usuarioVO.getUsuario());
				}
			}
	    }
	}
	
}