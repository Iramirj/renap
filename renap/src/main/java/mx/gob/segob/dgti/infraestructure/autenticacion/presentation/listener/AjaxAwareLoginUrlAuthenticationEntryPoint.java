package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.listener;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@SuppressWarnings("deprecation")
public class AjaxAwareLoginUrlAuthenticationEntryPoint extends
		LoginUrlAuthenticationEntryPoint {
	   
		private final String AJAX_REQUEST = "XMLHttpRequest"; 
		private final String HEADER_REQUEST = "X-Requested-With";
	
		public void commence(	final HttpServletRequest request, 
								final HttpServletResponse response, 
								final AuthenticationException authException) 
					throws IOException, ServletException {
			Boolean ajaxRequest = ( request.getHeader(HEADER_REQUEST) != null && 
									AJAX_REQUEST.equals(request.getHeader(HEADER_REQUEST)));
	        if (ajaxRequest) {
	            response.sendError(401, "Autenticacion Expirada");
	        } else {
	            super.commence(request, response, authException);
	        }
	    }
}
