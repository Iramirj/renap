package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.provider;



import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.AutenticacionMotivoException;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.AutorizacionConstants;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.criptografia.Criptografia;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

public class AutenticacionUsuarioPasswordForm 
		implements AuthenticationProvider {
	
	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private AutenticacionService autenticacionService;
	
	private Criptografia criptografia;
	
	private boolean multipleSession;
	
	private Integer minutosBloqueo;

	
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException  {
		
		logger.info("CustomAuthenticationProvider: authenticate ("+authentication.getName()+")");
		List<GrantedAuthority> grantedList = new ArrayList<GrantedAuthority>();
		if( !authentication.isAuthenticated() )
		{			
			String nombreUsuario = authentication.getName();
			String password = (String) authentication.getCredentials();
			if(criptografia != null ){
				try {				
					password = criptografia.hash(password);
				} catch (NoSuchAlgorithmException exception) {
					exception.printStackTrace();
					throw new AuthenticationServiceException(AutenticacionMotivoException.
																	CONVERSION_PASSWORD.getCodigoMensaje(), 
																  exception);
				}
			}
			
			
			UsuarioAutenticadoVO usuario;
			try {
				usuario = autenticacionService.getUsuario(nombreUsuario);
			} catch (AutenticacionExcepcion exception) {
				exception.printStackTrace();
				throw new AuthenticationServiceException(exception.getMessage(), 
					  			exception);
				
			}
			
			if(usuario == null) {
				throw new UsernameNotFoundException( AutenticacionMotivoException.
											USUARIO_NO_EXISTE.getCodigoMensaje());
			} else {
				if(usuario.getBloqueado().equals("B")){
					if(minutosBloqueo != null && minutosBloqueo > 0){
						Date fechaBloqueo = usuario.getFechaBloqueo();
						Calendar tiempoDesbloqueo = Calendar.getInstance();
						tiempoDesbloqueo.setTime(fechaBloqueo);
						tiempoDesbloqueo.add(Calendar.MINUTE, minutosBloqueo);
						Calendar tiempoActual = Calendar.getInstance();
						if(tiempoActual.after(tiempoDesbloqueo)){
							authentication = authenticate(authentication);
						}
					} else {
						throw new LockedException(AutenticacionMotivoException.
								USUARIO_BLOQUEADO.getCodigoMensaje());
					}
				} else if(!usuario.getStRegistro().equals("A")){
					throw new DisabledException(AutenticacionMotivoException.
							USUARIO_INACTIVO.getCodigoMensaje());
				} else {
					if(!usuario.getPassword().equals(password)){
						throw new BadCredentialsException( AutenticacionMotivoException.
								USUARIO_NO_VALIDO.getCodigoMensaje());
					} else if(usuario.getInSession().equals("S") && !multipleSession){
						throw new SessionAuthenticationException(AutenticacionMotivoException.
								USUARIO_EN_SESSION.getCodigoMensaje());
					}  else {
						List<String> roles = usuario.getPermisos();			
						for(String rol : roles){
							grantedList.add(new SimpleGrantedAuthority(rol));
						}
						
						grantedList.add(new SimpleGrantedAuthority(AutorizacionConstants.ROL_NO_NECESARIO));
						grantedList.add(new SimpleGrantedAuthority(AutorizacionConstants.TODOS_LOS_ROLES));
						authentication = new UsernamePasswordAuthenticationToken(usuario, password, grantedList);
					}
				}				 				
			}			
		}
		return authentication;
	}

	
	
	public boolean supports(Class<? extends Object> authenticate) {
		
		return true;
	}
	
	public AutenticacionService getAutenticacionService() {
		return autenticacionService;
	}

	public void setAutenticacionService(AutenticacionService autenticacionService) {
		this.autenticacionService = autenticacionService;
	}

	public Criptografia getCriptografia() {
		return criptografia;
	}

	public void setCriptografia(Criptografia criptografia) {
		this.criptografia = criptografia;
	}
	
	public boolean isMultipleSession() {
		return multipleSession;
	}

	public void setMultipleSession(boolean multipleSession) {
		this.multipleSession = multipleSession;
	}


	public Integer getMinutosBloqueo() {
		return minutosBloqueo;
	}


	public void setMinutosBloqueo(Integer minutosBloqueo) {
		this.minutosBloqueo = minutosBloqueo;
	}
}
