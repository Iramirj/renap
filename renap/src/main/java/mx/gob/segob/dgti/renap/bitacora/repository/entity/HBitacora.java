package mx.gob.segob.dgti.renap.bitacora.repository.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="H_Bitacora")
public class HBitacora implements Serializable{

	/****/
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_H_BITACORA")
	private Integer idHBitacora;
	@Column(name = "ID_M_USUARIO")
	private Integer idMUsuario;
	@Column(name = "ID_C_ENT_FED")
	private Integer idCEntFed;
	@Column(name = "ID_C_TIPO_ACCION")
	private Integer idCTipoAccion;
	@Column(name = "ACCION")
	private String accion;
	@Column(name = "FECHA")
	private String fecha;
	@Column(name = "HORA")
	private String hora;
	@Column(name = "MODULO")
	private String modulo;
	@Column(name = "RESUMEN")
	private String resumen;
	@Column(name = "CONSULTA")
	private String consulta;
	@Column(name = "ID_M_PODER")
	private Integer idMPoder;
	@Column(name = "ID_USUARIO_ALTA")
	private Integer idUsuarioAlta;
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ALTA")
	private Date fechaAlta;
	@Column(name = "ID_USUARIO_MOD")
	private Integer idUsuarioMod;
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MOD")
	private Date fechaMod;
	@Column(name = "ID_USUARIO_BAJA")
	private Integer idUsuarioBaja;
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_BAJA")
	private Date fechaBaja;
	@Column(name = "ST_REGISTRO")
	private String stRegistro;
	public Integer getIdHBitacora() {
		return idHBitacora;
	}
	public void setIdHBitacora(Integer idHBitacora) {
		this.idHBitacora = idHBitacora;
	}
	public Integer getIdMUsuario() {
		return idMUsuario;
	}
	public void setIdMUsuario(Integer idMUsuario) {
		this.idMUsuario = idMUsuario;
	}
	public Integer getIdCEntFed() {
		return idCEntFed;
	}
	public void setIdCEntFed(Integer idCEntFed) {
		this.idCEntFed = idCEntFed;
	}
	public Integer getIdCTipoAccion() {
		return idCTipoAccion;
	}
	public void setIdCTipoAccion(Integer idCTipoAccion) {
		this.idCTipoAccion = idCTipoAccion;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public String getResumen() {
		return resumen;
	}
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}
	public String getConsulta() {
		return consulta;
	}
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}
	public Integer getIdMPoder() {
		return idMPoder;
	}
	public void setIdMPoder(Integer idMPoder) {
		this.idMPoder = idMPoder;
	}
	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}
	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}
	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}
	public Date getFechaMod() {
		return fechaMod;
	}
	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}
	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}
	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public String getStRegistro() {
		return stRegistro;
	}
	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
