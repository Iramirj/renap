package mx.gob.segob.dgti.infraestructure.auditoria.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD})
@Inherited
public  @interface Auditable {
	public abstract String modulo();
}
