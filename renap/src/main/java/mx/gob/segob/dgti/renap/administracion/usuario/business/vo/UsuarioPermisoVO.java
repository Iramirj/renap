package mx.gob.segob.dgti.renap.administracion.usuario.business.vo;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;



public class UsuarioPermisoVO {
	
	private PermisoVO permiso;
	private UsuarioVO usuario;
	private TipoPermisoEnum tipoPermiso;
	
	public PermisoVO getPermiso() {
		return permiso;
	}
	public void setPermiso(PermisoVO permiso) {
		this.permiso = permiso;
	}
	public UsuarioVO getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}
	public TipoPermisoEnum getTipoPermiso() {
		return tipoPermiso;
	}
	public void setTipoPermiso(TipoPermisoEnum tipoPermiso) {
		this.tipoPermiso = tipoPermiso;
	}
	
}
