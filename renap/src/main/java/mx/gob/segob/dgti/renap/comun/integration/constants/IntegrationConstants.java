package mx.gob.segob.dgti.renap.comun.integration.constants;

public interface IntegrationConstants {

	/** nq consultar dit. */
	String NQ_CONSULTAR_DIT = "CONSULTAR_DIT";

	/** porcentaje. */
	String PORCENTAJE = "%";

	/** like. */
	String LIKE = " LIKE ";

	/** comilla simple. */
	String COMILLA_SIMPLE = "'";

	/** igual. */
	String IGUAL = " = ";

	/** and. */
	String AND = " AND ";

	/** select. */
	String SELECT = " SELECT ";

	/** count. */
	String COUNT = " COUNT(*) ";

	/** from. */
	String FROM = " FROM ";

	/** where. */
	String WHERE = " WHERE ";

	/** in. */
	String IN = " IN ";

	/** punto. */
	String PUNTO = ".";

	/** espacio. */
	String ESPACIO = " ";

	/** inner join. */
	String INNER_JOIN = " INNER JOIN ";

	/** parentesis abre. */
	String PARENTESIS_ABRE = " ( ";

	/** parentesis cierra. */
	String PARENTESIS_CIERRA = " ) ";

	/** sf translate function. */
	String SF_TRANSLATE_FUNCTION = "translate";

	/** signo interrogacion. */
	String SIGNO_INTERROGACION = "?";

	/** uno igual uno. */
	String UNO_IGUAL_UNO = " 1=1 ";

	/** Funcion a mayusculas. */
	String UPPER_FUNCTION = "UPPER";

	/** Caracter coma. */
	String COMA = ",";

	/** UIVocales. */
	String VOCALES = "COMUN_VOCALES";

	/** UI vocales acentuadas. */
	String VOCALES_ACENTUADAS = "COMUN_VOCALES_ACENTUADAS";

}
