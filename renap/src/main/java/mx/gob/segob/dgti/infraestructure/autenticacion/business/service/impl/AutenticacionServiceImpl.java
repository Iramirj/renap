package mx.gob.segob.dgti.infraestructure.autenticacion.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gob.segob.dgti.infraestructure.auditoria.annotation.Auditable;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.builder.UsuarioAutenticadoBuilder;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.entity.UsuarioAutenticado;
import mx.gob.segob.dgti.infraestructure.autenticacion.persistence.repository.AutenticacionRepository;
import mx.gob.segob.dgti.infraestructure.base.business.service.impl.BaseServiceImpl;


@Service("autenticacionService")
public class AutenticacionServiceImpl extends BaseServiceImpl 
		implements AutenticacionService {

	@Autowired
	AutenticacionRepository autenticacionRepository;
	
	
	@Override
	public UsuarioAutenticadoVO getUsuario(String nombreUsuario)
			throws AutenticacionExcepcion {
		UsuarioAutenticadoBuilder usuarioBuilder = new UsuarioAutenticadoBuilder();
		 
		UsuarioAutenticadoVO usuarioVO = null;
		List<String> permisosPerfil = null;
		Map<String,String> permisosMap=new HashMap<String,String>();
		
		List<String> permisosUsrConcedido = null;
		List<String> permisosUsrDenegado = null;
		
		UsuarioAutenticado usuario = autenticacionRepository.getUsuario(nombreUsuario);
		if(usuario != null){
			permisosPerfil = autenticacionRepository.getPermisosPerfil(nombreUsuario);
			administrarPermisos(permisosMap,permisosPerfil,TipoPermisoEnum.ADICIONAL);
			permisosUsrConcedido = autenticacionRepository.getPermisosUsuario(nombreUsuario,TipoPermisoEnum.ADICIONAL);
			administrarPermisos(permisosMap,permisosUsrConcedido,TipoPermisoEnum.ADICIONAL);
			permisosUsrDenegado=autenticacionRepository.getPermisosUsuario(nombreUsuario,TipoPermisoEnum.DENEGADO);
			administrarPermisos(permisosMap,permisosUsrDenegado,TipoPermisoEnum.DENEGADO);
		}
		
		usuarioVO = usuarioBuilder.convierteEntidadVo(usuario, new ArrayList<String>(permisosMap.keySet()));
		
		return usuarioVO;
	}
	
	
	private void  administrarPermisos(Map<String,String> mapa, List<String> listaPermisos,TipoPermisoEnum tipoPermiso){
		if(TipoPermisoEnum.ADICIONAL.equals(tipoPermiso)){
			for(String permiso: listaPermisos){
				mapa.put(permiso, permiso);
			}
		}
		else{
			for(String permiso: listaPermisos){
				mapa.remove(permiso);
			}
		}
	}

	@Override
	public void inicializarUsuarios() {
		autenticacionRepository.inicializarUsuarios();
	}

	@Override
	@Auditable(modulo="Registro de inicio de sesion")
	public void registrarUsuario(String usuarioId) {
		autenticacionRepository.registrarUsuario(usuarioId);
	}

	@Override
	public void anularUsuarioSession(String usuarioId) {
		autenticacionRepository.anularUsuarioSession(usuarioId);
	}

	@Override
	public void registraIntentoSession(String usuarioId, Integer numeroIntento,
			String bloqueo) {
		autenticacionRepository.registraIntentoSession(usuarioId, numeroIntento, bloqueo);
	}

}
