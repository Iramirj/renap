package mx.gob.segob.dgti.renap.administracion.usuario.business.vo;

import java.util.List;

import javax.validation.constraints.Size;

import mx.gob.segob.dgti.renap.comun.business.constants.EstatusEnum;
import mx.gob.segob.dgti.renap.comun.business.constants.PermisoEnum;
import mx.gob.segob.dgti.renap.comun.presentation.constants.MensajesValidaciones;
import mx.gob.segob.dgti.renap.comun.presentation.validator.annotation.Alfabeto_Espacio_MX;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.Busqueda;
import mx.gob.segob.dgti.renap.comun.presentation.validator.grups.Guardado;



import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class PerfilVO  implements  MensajesValidaciones {
	
	private Integer idPerfil;
	private String clavePerfil;
	private String descripcion;
	private List<PermisoEnum> listaPermiso;
	private EstatusEnum estatus;

	
	
	public Integer getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	
	@NotEmpty(message = CAMPO_REQUERIDO, groups = Guardado.class)
	@Size(min = 2, max = 250 , message = LONGITUD_MAXIMA_250, groups = Guardado.class)
	public String getClavePerfil() {
		return clavePerfil;
	}

	public void setClavePerfil(String clavePerfil) {
		this.clavePerfil = clavePerfil;
	}
    
	@Alfabeto_Espacio_MX(groups ={ Busqueda.class,Guardado.class})
	@NotEmpty(message =CAMPO_REQUERIDO, groups = Guardado.class)
	@Size(min = 3, max = 250, message = LONGITUD_MAXIMA_250, groups =Guardado.class)
	@Length( max = 250, message = LONGITUD_MAXIMA_250, groups = Busqueda.class)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<PermisoEnum> getListaPermiso() {
		return listaPermiso;
	}

	public void setListaPermiso(List<PermisoEnum> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}

	
	public EstatusEnum getEstatus() {
		return estatus;
	}

	public void setEstatus(EstatusEnum estatus) {
		this.estatus = estatus;
	}
	
	public String getDescripcionConverter() {
		return this.getDescripcion();
	}
    
	
	

}
