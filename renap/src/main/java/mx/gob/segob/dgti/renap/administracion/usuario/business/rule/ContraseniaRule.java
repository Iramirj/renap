package mx.gob.segob.dgti.renap.administracion.usuario.business.rule;

import java.security.NoSuchAlgorithmException;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.criptografia.MD5Cripto;

public class ContraseniaRule extends AbstractBaseRules {

	/** La constante BASE. */
	private final static String BASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * Generar contrasenia.
	 * 
	 * @return string
	 * @throws AdministracionExcepcion
	 *             administracion excepcion
	 */
	public static String generarContrasenia() {
		String password = new String();
		try {
			int longitud = 8;

			for (int i = 0; i < longitud; i++) {
				int numero = (int) (Math.random() * (BASE.length()));
				String caracter = BASE.substring(numero, numero + 1);
				password = password.concat(caracter);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return password;
	}

	/**
	 * Encriptar.
	 * 
	 * @param password
	 *            password
	 * @return string
	 * @throws NoSuchAlgorithmException
	 * @throws EncriptaException
	 *             encripta exception
	 */
	public static String encriptar(String password)
			throws NoSuchAlgorithmException {
		MD5Cripto md5Cripto = new MD5Cripto();
		String passwordHash = md5Cripto.hash(password);
		return passwordHash;
	}
}
