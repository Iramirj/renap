package mx.gob.segob.dgti.renap.administracion.usuario.business.rule;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractBaseRules {

	protected final Logger logger = Logger.getLogger(getClass().getPackage()
			.getName());

	public AbstractBaseRules() {
	}

}
