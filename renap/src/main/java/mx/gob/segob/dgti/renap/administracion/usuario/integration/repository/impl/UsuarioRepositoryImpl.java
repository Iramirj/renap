package mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.impl;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.DUsuarioPerfil;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.UsuarioRepository;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.impl.BaseRepositoryImpl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository
public class UsuarioRepositoryImpl extends BaseRepositoryImpl implements
		UsuarioRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Usuario> buscar(UsuarioVO usuarioVO, Integer primerRegistro,
			int ultimoRegistro) {
		List<Usuario> listaUsuarios = null;
		Criteria criteria = construirConsulta(usuarioVO);
		criteria.setFirstResult(primerRegistro);
		criteria.setMaxResults(ultimoRegistro);
		listaUsuarios = criteria.list();
		return listaUsuarios;
	}

	@Override
	public Integer buscarTotal(UsuarioVO usuarioVO) {
		Long total = Long.parseLong("0");
		Criteria criteria = construirConsulta(usuarioVO);
		total = (Long) criteria.setProjection(Projections.rowCount())
				.uniqueResult();
		return total.intValue();

	}

	@Override
	public Usuario obtenerPorId(String userName) {
		List<Usuario> lstUsuario = null;
		if (userName != null) {
			lstUsuario = sessionFactory.getCurrentSession()
					.createQuery("from Usuario where usuario =?")
					.setParameter(0, userName).list();
		}
		return lstUsuario.size() > 0 ? lstUsuario.get(0) : null;
	}

	@Override
	public boolean existe(UsuarioVO usuarioVO) {

		Long total = Long.parseLong("0");
		Criteria criteria = getSession().createCriteria(Usuario.class);
		if (usuarioVO != null && usuarioVO.getNombreUsuario() != null
				&& !usuarioVO.getNombreUsuario().isEmpty()) {
			criteria.add(Restrictions.eq(ID_USUARIO,
					usuarioVO.getNombreUsuario()));
		}
		total = (Long) criteria.setProjection(Projections.rowCount())
				.uniqueResult();
		if (total.intValue() == 0) {
			return false;
		}
		return true;

	}

	@Override
	public List<Usuario> buscar(UsuarioVO usuarioVO) {
		List<Usuario> listaUsuarios = null;
		Criteria criteria = construirConsulta(usuarioVO);
		listaUsuarios = criteria.list();
		return listaUsuarios;
	}

	private final Criteria construirConsulta(final UsuarioVO usuarioVO) {
		Criteria criteria = this.getSession().createCriteria(Usuario.class);
		if (usuarioVO != null) {
			if (usuarioVO.getStRegistro() != null) {
				criteria.add(Restrictions.eq(ACTIVO, usuarioVO.getStRegistro()));
			}
			if (usuarioVO.getNombreUsuario() != null
					&& !usuarioVO.getNombreUsuario().isEmpty()) {
				criteria.add(Restrictions.like(ID_USUARIO,
						usuarioVO.getNombreUsuario(), MatchMode.ANYWHERE));
			}

			if (usuarioVO.getUsuarioDatosVO() != null) {
				criteria.createAlias(USUARIO_DATOS, USUARIO_DATOS);
			}
			if (usuarioVO.getUsuarioDatosVO() != null) {
				if (usuarioVO.getUsuarioDatosVO().getNombre() != null
						&& !usuarioVO.getUsuarioDatosVO().getNombre().isEmpty()) {

					criteria.add(Restrictions.like(NOMBRE, usuarioVO
							.getUsuarioDatosVO().getNombre(),
							MatchMode.ANYWHERE));
				}
				if (usuarioVO.getUsuarioDatosVO().getCorreo() != null
						&& !usuarioVO.getUsuarioDatosVO().getCorreo().isEmpty()) {

					criteria.add(Restrictions.like(CORREO_ELECTRONICO,
							usuarioVO.getUsuarioDatosVO().getCorreo(),
							MatchMode.ANYWHERE));
				}
			}
			/*
			 * if (usuarioVO.getPerfilVO() != null &&
			 * usuarioVO.getPerfilVO().getIdPerfil() != null) {
			 * criteria.createAlias(LISTA_PERFILES, LISTA_PERFILES);
			 * criteria.add(Restrictions.eq(
			 * LISTA_PERFILES.concat(".").concat(ID_PERFIL), usuarioVO
			 * .getPerfilVO().getIdPerfil())); }
			 */

		}
		return criteria;
	}

	@Override
	public List<Usuario> getListUserByIdUserLogeo(Integer idUsuario) {
		@SuppressWarnings("unchecked")
		List<Usuario> list = sessionFactory
				.getCurrentSession()
				.createQuery(
						"SELECT u.idUsuario, u.usuario from Usuario u where u.stRegistro = 'A'")
				.list();
		return list.size() > 0 ? list : null;
	}

	@Override
	public Integer getPerfilByIdUsuario(Integer idUsuario) {
		DUsuarioPerfil list = (DUsuarioPerfil) sessionFactory.getCurrentSession()
				.createQuery("from DUsuarioPerfil where idMUsuario = ?")
				.setParameter(0, idUsuario).uniqueResult();
		return (Integer) (list != null ? list.getIdCPerfil() : null);
	}
}
