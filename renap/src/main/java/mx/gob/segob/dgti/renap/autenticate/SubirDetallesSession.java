package mx.gob.segob.dgti.renap.autenticate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import mx.gob.segob.dgti.renap.administracion.usuario.business.service.UsuarioService;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.UsuarioVO;
import mx.gob.segob.dgti.renap.comun.business.constants.SessionConstants;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.autenticate.AutenticacionUtil;

public class SubirDetallesSession implements AutenticacionUtil {

	@Autowired
	private UsuarioService flUsuarioService;

	public Map<String, Object> obtenerDetallesPorSubirASession(String usuarioId) {
		Map<String, Object> detalles = new HashMap<String, Object>();

		String nombreUsuario = "";

		UsuarioVO flUsuarioVO = flUsuarioService.obtenerPorIdSession(usuarioId);
		if (flUsuarioVO != null) {
			nombreUsuario = flUsuarioVO.getNombreCompleto();
		}

		System.out.println("user: " + flUsuarioVO.getNombreCompleto());

		detalles.put(SessionConstants.USUARIO_SESSION_ID, flUsuarioVO);
		detalles.put(SessionConstants.NOMBRE_COMPLETO_USUARIO, nombreUsuario);
		detalles.put(SessionConstants.USUARIO_ID, flUsuarioVO.getIdUsuario());
		detalles.put(SessionConstants.PRIMERA_VEZ, flUsuarioVO.getPrimeraVez());
		detalles.put(SessionConstants.NOMBRE_DEPENDECIA, "CDMX");
		detalles.put(SessionConstants.TIPO_NAC, flUsuarioVO.getTipoNacionalidad());
		return detalles;
	}

}
