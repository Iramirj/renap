package mx.gob.segob.dgti.renap.administracion.usuario.business.constants;

public enum UsuarioEstatusEnum {

	ACTIVO("Activo", 1), INACTIVO("Inactivo", 0);

	private String descripcion;
	private int activo;

	private UsuarioEstatusEnum(String descripcion, int activo) {
		this.descripcion = descripcion;
		this.activo = activo;

	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

}
