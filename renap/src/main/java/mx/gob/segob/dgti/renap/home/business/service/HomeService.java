package mx.gob.segob.dgti.renap.home.business.service;

import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Usuario;
import mx.gob.segob.dgti.renap.home.business.vo.UserVo;

public interface HomeService {
	public Usuario getDetalleById(Integer flIdUsuario);
	public Boolean changePasswordById(Usuario objUsuario);
}
