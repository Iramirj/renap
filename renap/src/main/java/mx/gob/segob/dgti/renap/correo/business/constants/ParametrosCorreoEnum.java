package mx.gob.segob.dgti.renap.correo.business.constants;

import java.io.Serializable;

public enum ParametrosCorreoEnum implements Serializable {

	/** nombre comercial. */
	USER_NAME("@user@"),
	/** user password. */
	USER_PASSWORD("@password@");
	
	/** parametro. */
	private String parametro;

	/**
	 * El constructor de parametros notificacion enum.
	 * 
	 * @param pparametro
	 *            pparametro
	 */
	ParametrosCorreoEnum(String pparametro) {
		this.parametro = pparametro;
	}

	/**
	 * Obtiene parametro.
	 * 
	 * @return parametro
	 */
	public String getParametro() {
		return parametro;
	}

	/**
	 * Establece parametro.
	 * 
	 * @param parametro
	 *            el nuevo parametro
	 */
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
}
