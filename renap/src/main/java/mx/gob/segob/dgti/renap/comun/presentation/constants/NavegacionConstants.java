package mx.gob.segob.dgti.renap.comun.presentation.constants;

public interface NavegacionConstants {
	//modulo de usuarios
	String ADMINISTRACION_USUARIO = "modules/administracion/usuarios/consultaUsuarios";
	String ALTA_EDICION_USUARIO="modules/administracion/usuarios/registroUsuario";
	
	//Bitacora
	String ADMINISTRACION_BITACORA="modules/administracion/bitacora/consultaBitacora";	
	String BITACORA = "modules/bitacora/bitacoraCons"; 
	String DETALLE_REGISTRO_BITACORA ="modules/bitacora/bitacoraDetalle"; 
	 
	//ModuloInicial
	String ADMINISTRACION_HOME = "/home/pruebaInicial/returnName/";
}
