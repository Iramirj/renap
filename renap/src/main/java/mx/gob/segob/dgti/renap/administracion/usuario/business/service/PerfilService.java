package mx.gob.segob.dgti.renap.administracion.usuario.business.service;

import java.util.List;

import mx.gob.segob.dgti.renap.administracion.usuario.business.exception.AdministracionException;
import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;



public interface PerfilService  {

	
	List<PerfilVO> consultarPorPaginancion(PerfilVO perfilVO, int primerRegistro,
			int ultimoRegistro);

	Integer consultarTotal(PerfilVO perfilVO);

	void guardar(PerfilVO perfilVO) throws AdministracionException;

	List<PerfilVO> buscarPerfil(PerfilVO perfilVO);
	
	PerfilVO consultarPorId(Integer idPerfil);
	
	boolean existe(Integer idPerfil, String nombre);

}
