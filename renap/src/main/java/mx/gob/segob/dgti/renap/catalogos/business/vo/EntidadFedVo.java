package mx.gob.segob.dgti.renap.catalogos.business.vo;

public class EntidadFedVo {
	
	private Integer flIdUsuario;
	private String flUsuario;
	private String flPassword;
	private String flTipoNac;

	public EntidadFedVo() {
	}

	public Integer getFlIdUsuario() {
		return flIdUsuario;
	}

	public void setFlIdUsuario(Integer flIdUsuario) {
		this.flIdUsuario = flIdUsuario;
	}

	public String getFlUsuario() {
		return flUsuario;
	}

	public void setFlUsuario(String flUsuario) {
		this.flUsuario = flUsuario;
	}

	public String getFlPassword() {
		return flPassword;
	}

	public void setFlPassword(String flPassword) {
		this.flPassword = flPassword;
	}

	public String getFlTipoNac() {
		return flTipoNac;
	}

	public void setFlTipoNac(String flTipoNac) {
		this.flTipoNac = flTipoNac;
	}
	
}
