package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/login", method = {RequestMethod.GET, RequestMethod.POST})
public class LoginController {

	@RequestMapping	
	public String redirectLoginView(HttpSession session){
		session.invalidate();
		
		return "login";
	}
}
