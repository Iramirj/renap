package mx.gob.segob.dgti.renap.administracion.bitacora.business.builder;

import mx.gob.segob.dgti.renap.administracion.bitacora.business.vo.ConsultaBitacoraVO;
import mx.gob.segob.dgti.infraestructure.auditoria.persistence.entity.BitacoraSistema;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class AdministracionBitacoraBuilder extends
		BaseBuilder<ConsultaBitacoraVO, BitacoraSistema> {

	@Override
	public BitacoraSistema convertirVOAEntidad(ConsultaBitacoraVO vo) {
		return null;
	}

	@Override
	public ConsultaBitacoraVO convertirEntidadAVO(BitacoraSistema entidad) {
		ConsultaBitacoraVO vo = null;

		if (entidad != null) {
			vo = new ConsultaBitacoraVO();
			vo.setIdHBitacora(entidad.getIdHBitacora());
			vo.setIdMUsuario(entidad.getIdMUsuario());
			vo.setModulo(entidad.getModulo());
			vo.setAccion(entidad.getAccion());
			vo.setFecha(entidad.getFecha());
		}
		return vo;
	}

}
