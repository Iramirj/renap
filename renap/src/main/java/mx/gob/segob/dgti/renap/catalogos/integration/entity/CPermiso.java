package mx.gob.segob.dgti.renap.catalogos.integration.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="C_Permiso")
public class CPermiso implements Serializable{
	
	/****/
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_C_PERMISO")
	private Integer idCPermiso;
	
	@Column(name="PERMISO")
	private String permiso;
	
	@Column(name="ID_USUARIO_ALTA")
	private Integer idUsuarioAlta;
	
	@Column(name="ID_USUARIO_MOD")
	private Integer idUsuarioMod;
	
	@Column(name="ID_USUARIO_BAJA")
	private Integer idUsuarioBaja;
	
	@Column(name="FECHA_ALTA")
	private Date fechaAlta;
	
	@Column(name="FECHA_MOD")
	private Date fechaMod;
	
	@Column(name="FECHA_BAJA")
	private Date fechaBaja;
	
	@Column(name="ST_REGISTRO")
	private String stRegistro;

	public Integer getIdCPermiso() {
		return idCPermiso;
	}

	public void setIdCPermiso(Integer idCPermiso) {
		this.idCPermiso = idCPermiso;
	}

	public String getPermiso() {
		return permiso;
	}

	public void setPermiso(String permiso) {
		this.permiso = permiso;
	}

	public Integer getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public void setIdUsuarioAlta(Integer idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public Integer getIdUsuarioMod() {
		return idUsuarioMod;
	}

	public void setIdUsuarioMod(Integer idUsuarioMod) {
		this.idUsuarioMod = idUsuarioMod;
	}

	public Integer getIdUsuarioBaja() {
		return idUsuarioBaja;
	}

	public void setIdUsuarioBaja(Integer idUsuarioBaja) {
		this.idUsuarioBaja = idUsuarioBaja;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Date fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}
}
