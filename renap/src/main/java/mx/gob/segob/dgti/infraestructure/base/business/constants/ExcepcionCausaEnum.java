package mx.gob.segob.dgti.infraestructure.base.business.constants;

import mx.gob.segob.dgti.infraestructure.base.presentation.helper.MensajeHelper;


public enum ExcepcionCausaEnum {

	/** comun error guardar. */
	COMUN_ERROR_CONSULTAR("COMUN_ERROR_CONSULTAR"),
	COMUN_ERROR_GUARDAR("COMUN_ERROR_GUARDAR"),
	COMUN_ERROR_DUPLICADO("COMUN_ERROR_DUPLICADO"),
	COMUN_ERROR_GENERAR_CONTRASENIA("COMUN_ERROR_GENERAR_CONTRASENIA"),
	COMUN_ERROR_CAPTURA("COMUN_ERROR_CAPTURA"),
	COMUN_DESCARGA_ARCHIVO("COMUN_DESCARGA_ARCHIVO"),
	COMUN_ERROR_CREAR_CARPETA("COMUN_ERROR_CREAR_CARPETA"),
	COMUN_ERROR_CREAR_ARCHIVO("COMUN_ERROR_CREAR_ARCHIVO"),
	COMUN_GUARDAR_ARCHIVO("COMUN_GUARDAR_ARCHIVO"),
	COMUN_ELIMINAR_ARCHIVO("COMUN_ELIMINAR_ARCHIVO");
	
	
	private String descripcion;
	
	private ExcepcionCausaEnum(String descripcion){
		this.descripcion = descripcion;
	}

	public String getDescripcion(String ... parametros) {
		return MensajeHelper.obtenerMensaje(descripcion, parametros);		 
	}
	
	public String getDescripcion() {
		return MensajeHelper.obtenerMensaje(descripcion);		 
	}
}
