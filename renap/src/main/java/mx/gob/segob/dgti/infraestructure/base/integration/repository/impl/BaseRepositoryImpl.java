package mx.gob.segob.dgti.infraestructure.base.integration.repository.impl;

import java.io.Serializable;




import mx.gob.segob.dgti.infraestructure.base.integration.repository.BaseRepository;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseRepositoryImpl implements BaseRepository{
	protected final Logger logger = Logger.getLogger(getClass().getPackage()
			.getName());

	@Autowired
	private SessionFactory sessionFactory;

	public final Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}

	/**
	 * Guardar.
	 * 
	 * @param entidad
	 * @throws Exception
	 *             the repository exception
	 */
	
	public void guardar(Serializable entidad) throws Exception {
		getSession().save(entidad);
	}

	/**
	 * Modificar.
	 * 
	 * @param entidad
	 * @throws Exception
	 *             the repository exception
	 */
	
	public void modificar(Serializable entidad) throws Exception {
		getSession().update(entidad);
	}

	/**
	 * Guardar u Modificar.
	 * 
	 * @param entidad
	 * @throws Exception
	 *             el repository exception
	 */
	
	public void guardarModificar(Serializable entidad) throws Exception {
		getSession().saveOrUpdate(entidad);
	}

	/**
	 * Borrar.
	 * 
	 * @param entidad
	 *            a borrar
	 * @throws Exception
	 *             el repository exception
	 */
	
	public void eliminar(Serializable entidad) throws Exception {
		getSession().delete(entidad);
	}

	/**
	 * Flush.
	 * 
	 * @throws Exception
	 *             the repository exception
	 */
	
	public void flush() throws Exception {
		getSession().flush();
	}

	/**
	 * Merge.
	 * 
	 * @param entidad
	 *            el ppo entidad
	 * @throws Exception
	 *             el repository exception
	 */
	
	public void merge(Serializable entidad) throws Exception {
		getSession().merge(entidad);
		getSession().flush();
	}

	
}
