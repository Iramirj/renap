package mx.gob.segob.dgti.renap.catalogos.business.vo;

public class PerfilesPorPerfilesVO {
	private Integer idRegistro;
	private Integer idPerfilA;
	private Integer idPerfilB;
	private String perfilA;
	private String perfilB;
	private String tipoFiltro;

	public Integer getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Integer idRegistro) {
		this.idRegistro = idRegistro;
	}

	public Integer getIdPerfilA() {
		return idPerfilA;
	}

	public void setIdPerfilA(Integer idPerfilA) {
		this.idPerfilA = idPerfilA;
	}

	public Integer getIdPerfilB() {
		return idPerfilB;
	}

	public void setIdPerfilB(Integer idPerfilB) {
		this.idPerfilB = idPerfilB;
	}

	public String getPerfilA() {
		return perfilA;
	}

	public void setPerfilA(String perfilA) {
		this.perfilA = perfilA;
	}

	public String getPerfilB() {
		return perfilB;
	}

	public void setPerfilB(String perfilB) {
		this.perfilB = perfilB;
	}

	public String getTipoFiltro() {
		return tipoFiltro;
	}

	public void setTipoFiltro(String tipoFiltro) {
		this.tipoFiltro = tipoFiltro;
	}
}
