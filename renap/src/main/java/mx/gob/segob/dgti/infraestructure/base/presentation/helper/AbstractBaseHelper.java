package mx.gob.segob.dgti.infraestructure.base.presentation.helper;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class AbstractBaseHelper {

	protected final Logger logger = Logger.getLogger(getClass().getPackage()
			.getName());

	public AbstractBaseHelper() {
	}

}
