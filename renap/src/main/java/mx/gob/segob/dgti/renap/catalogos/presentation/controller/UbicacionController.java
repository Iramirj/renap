package mx.gob.segob.dgti.renap.catalogos.presentation.controller;
import java.util.List;
import mx.gob.segob.dgti.renap.catalogos.business.service.UbicacionService;
import mx.gob.segob.dgti.renap.catalogos.integration.entity.CEntFed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@Scope("session")
public class UbicacionController {

	@Autowired
	private UbicacionService flUbicacionService;

	public static final ObjectMapper flJsonMapper = new ObjectMapper();

	@RequestMapping(value = "/modules/home/retrieveEntidadBitacora/{flIdCiudad}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody
	String returnEntidadFederativa(@PathVariable(value = "flIdCiudad") Integer flIdCiudad)
			throws JsonProcessingException {
		String resultJson = null;
		List<CEntFed> flEntidad = flUbicacionService.getListByIdPais(flIdCiudad);
		if (flEntidad != null) {
			resultJson = flJsonMapper.writeValueAsString(flEntidad);
		}
		return resultJson;
	}
}
