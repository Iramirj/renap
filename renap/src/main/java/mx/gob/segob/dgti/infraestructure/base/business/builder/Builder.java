package mx.gob.segob.dgti.infraestructure.base.business.builder;

public interface Builder<V, E> {
	/**
	 * Metodo abstracto para la conversion de un VO a PO.
	 * 
	 * @param vo
	 *            el objeto vo
	 * @return un PO
	 */
	E convertirVOAEntidad(V vo);

	/**
	 * Metodo abstracto para la conversion de un PO a VO.
	 * 
	 * @param entidad
	 *            entidad
	 * @return un VO
	 */
	V convertirEntidadAVO(E entidad);
}
