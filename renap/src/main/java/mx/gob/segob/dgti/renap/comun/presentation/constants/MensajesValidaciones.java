package mx.gob.segob.dgti.renap.comun.presentation.constants;

public interface MensajesValidaciones {
	
	
	//modulo general
	String CAMPO_REQUERIDO = "El valor del campo es requerido";
	String MENSAJE_CAMBIAR_ESTATUS_EXITO= "Se ha cambiado el estatus correctamente ";

	
	//modulo usuarios
	String LONGITUD_MAXIMA_250 = "La longitud del campo debe ser menor a 250";
	String NOMBRE_REQUERIDO="El nombre del usuario es requerido";
	String NOMBRE_USUARIO="El nombre es requerido";
	String APELLIDOP_REQUERIDO="El apellido paterno es requerido";
	String APELLIDOM_REQUERIDO="El apellido materno es requerido";
	String CORREO_REQUERIDO="El correo es requerido";
	String PERFIL_NO_VACIO="Debe seleccionar al menos un perfil de usuario";
	String USUARIO_EXISTE="El usuario ya existe";
	
	//modulo contrasena
	String LONGITUD_CONTRASENA_MAXIMA ="La longitud del campo debe ser entre 8 y 21";
	String LONGITUD_CONTRASENA = "La longitud debe ser menor a 21";
	String LONGITUD_CONTRASENA_MINIMA="La longitud debe ser minimo de 8"; 
	String CARACTER_INVALIDO_CONTRASENA="El caracter \u00f1 no es v\u00e1lido";
	String FORMATO_NO_VALIDO_CONTRASENA="Contrase\u00f1a  inv\u00e1lida debe contener contener N\u00fameros, May\u00fasculas y Min\u00fasculas";
	String MENSAJE_REINICIAR_CONTRASENA_EXITO="Se ha reiniciado la contraseña correctamente";
	String MENSAJE_DESBLOQUEAR_EXITO="Se ha desbloqueado el usuario correctamente";
	
	
		

}
