package mx.gob.segob.dgti.infraestructure.autenticacion.presentation.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.excepcion.AutenticacionExcepcion;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.service.AutenticacionService;
import mx.gob.segob.dgti.infraestructure.autenticacion.business.vo.UsuarioAutenticadoVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.web.bind.ServletRequestUtils;

public class AutenticacionFallidaHandler extends SimpleUrlAuthenticationFailureHandler  {
	
	private String USER_NAME_CAMPO = "j_username";
	
	@Autowired 
	private AutenticacionService autenticacionService;
	
	private Integer numeroIntentosPermitidos;
	
	
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		String userName = ServletRequestUtils.getStringParameter(request, USER_NAME_CAMPO , null);
		actualizaIntento(userName, exception);
		
		request.setAttribute("errorAutenticacion", exception.getMessage());	
		
		saveException(request, exception);
		
		request.getRequestDispatcher("login").forward(request, response);
	}
	
	public void actualizaIntento(String userName, AuthenticationException exception) {		
		if(exception instanceof BadCredentialsException) {
			try {
				UsuarioAutenticadoVO usuario = autenticacionService.getUsuario(userName);
				Integer numeroIntentos = usuario.getNumeroIntentos()+1;
				String bloqueado = "B";
				
				if(numeroIntentosPermitidos != null &&
				   numeroIntentosPermitidos > 0		&&
				   numeroIntentos > numeroIntentosPermitidos ){					
						bloqueado = "D";
				}
				
				autenticacionService.registraIntentoSession(usuario.getUsuario(), numeroIntentos, bloqueado);				
			} catch (AutenticacionExcepcion e) {
				e.printStackTrace();
			}				
		}
	}

	public Integer getNumeroIntentosPermitidos() {
		return numeroIntentosPermitidos;
	}

	public void setNumeroIntentosPermitidos(Integer numeroIntentosPermitidos) {
		this.numeroIntentosPermitidos = numeroIntentosPermitidos;
	}

}
