package mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.impl;

import java.util.List;



import mx.gob.segob.dgti.renap.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.entity.Perfil;
import mx.gob.segob.dgti.renap.administracion.usuario.integration.repository.PerfilRepository;
import mx.gob.segob.dgti.renap.comun.integration.constants.IntegrationConstants;
import mx.gob.segob.dgti.infraestructure.base.integration.repository.impl.BaseRepositoryImpl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;



@SuppressWarnings("unchecked")
@Repository("PerfilRepository")
public class PerfilRepositoryImpl extends BaseRepositoryImpl implements PerfilRepository, IntegrationConstants {
	
	// Consulta por paginacion
	@Override
	public List<Perfil> consultarPorPaginacion(PerfilVO perfilVO,
			Integer primerRegistro, Integer ultimoRegistro) {
		List<Perfil> listaPerfil = null;
		Criteria criteria = construirConsulta(perfilVO);
		criteria.setFirstResult(primerRegistro);
		criteria.setMaxResults(ultimoRegistro);
		criteria.addOrder(Order.asc(DESCRIPCION_PERFIL));
		listaPerfil = criteria.list();
		return listaPerfil;
	}
	
	// consulta total
	@Override
	public Integer consultarTotal(PerfilVO perfilVO) {
		Long total=Long.parseLong("0") ;
		Criteria criteria = construirConsulta(perfilVO);
		total = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		return total.intValue();
	}

	@Override
	public List<Perfil> consultarPerfil(PerfilVO perfilVO) {
		List<Perfil> listaPerfil = null;
		Criteria criteria = construirConsulta(perfilVO);
		criteria.addOrder(Order.asc(DESCRIPCION_PERFIL));
		listaPerfil = criteria.list();
		return listaPerfil;
	}
		//construir consulta
		
		public final Criteria construirConsulta (final PerfilVO perfilVO){
			Criteria criteria = this.getSession().createCriteria(Perfil.class);
			if (perfilVO != null ){
			    if (perfilVO.getEstatus()!= null){
			    	criteria.add(Restrictions.eq(ESTATUS, perfilVO.getEstatus().isActivo()));
			    }
			    if(perfilVO.getIdPerfil()!= null){
			    	criteria.add(Restrictions.eq(ID_PERFIL, perfilVO.getIdPerfil()));
			    }
			    if (perfilVO.getDescripcion()!= null && !perfilVO.getDescripcion().isEmpty()){
			    	criteria.add(Restrictions.like(DESCRIPCION_PERFIL,PORCENTAJE + perfilVO.getDescripcion() + PORCENTAJE));
			    }
			    if (perfilVO.getClavePerfil()!= null && !perfilVO.getClavePerfil().isEmpty()){
			    	criteria.add(Restrictions.like(CLAVE_PERFIL,PORCENTAJE + perfilVO.getClavePerfil() + PORCENTAJE));
			    }
			   
			}
		
		return criteria ;
			}

		
	

	@Override
	public boolean existe(Integer idPerfil, String nombre) {
	     Long total = Long.parseLong("0");
	     Criteria criteria = getSession().createCriteria(Perfil.class);
	     if(idPerfil!= null){
	    	 criteria.add(Restrictions.ne(ID_PERFIL, idPerfil));
	     }
	     criteria.add(Restrictions.eq(CLAVE_PERFIL, nombre));
	     total = (Long)criteria.setProjection(Projections.rowCount()).uniqueResult();
	     if(total.intValue() == 0){
		return false;
	     }
	     return true;
	}

	@Override
	public Perfil consultarPorIdPerfil(Integer idPerfil) {
		Perfil perfil=null;
		Criteria criteria = getSession().createCriteria(Perfil.class);
	     if(idPerfil!= null){
	    	 criteria.add(Restrictions.eq(ID_PERFIL, idPerfil));
	     }
	     perfil = (Perfil)criteria.uniqueResult();
	     return perfil;
	}

}