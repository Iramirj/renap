package mx.gob.segob.dgti.renap.comun.presentation.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import mx.gob.segob.dgti.renap.comun.presentation.validator.constraint.AlfabetoEspacioMXConstraintValidator;



@Documented
@Constraint(validatedBy = AlfabetoEspacioMXConstraintValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Alfabeto_Espacio_MX {

	String message() default "{error Espacios }";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
