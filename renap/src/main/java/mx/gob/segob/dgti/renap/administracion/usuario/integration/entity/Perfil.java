package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;



import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;





/**
 * Mapeo de la entidad Perfil
 */
@Entity
@Table(name = "c_perfil")
public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7995675289217089253L;
	private Integer idPerfil;
	private String clavePerfil;
	private String descripcion;
	private boolean activo;
	private List<Permiso> listaPermisos;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_perfil", unique= true, nullable= false)
	public Integer getIdPerfil() {
		return this.idPerfil;
	}

	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	
	@Column(name = "clave_perfil", length= 255)
	public String getClavePerfil() {
		return clavePerfil;
	}

	public void setClavePerfil(String clavePerfil) {
		this.clavePerfil = clavePerfil;
	}

	@Column(name = "descripcion", length= 255)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToMany
	@JoinTable(name="d_perfil_permiso", joinColumns = {
	@JoinColumn(name="id_perfil", nullable=false, updatable=false) }, inverseJoinColumns = {
	@JoinColumn(name="id_permiso", nullable=false, updatable=false) })
	public List<Permiso> getListaPermisos() {
		return listaPermisos;
	}

	public void setListaPermisos(List<Permiso> listaPermisos) {
		this.listaPermisos = listaPermisos;
	}
	
	@Column(name = "activo", length = 10)
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	
	
	

	
	

}
