package mx.gob.segob.dgti.infraestructure.base.business.vo;

public class DataTableResult<T> {
	
	private MensajeRespuestaVO respuesta;
	private Integer recordsTotal;
	private Integer recordsFiltered;
	private Integer draw;
	private T[] data;
	
	public DataTableResult(Integer draw) {
		this.draw = draw;
		respuesta = new MensajeRespuestaVO();
		recordsTotal = 0;
		recordsFiltered = 0;		
	}
		

	public Integer getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(Integer recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public Integer getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(Integer recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}

	public T[] getData() {
		return data;
	}
	public void setData(T[] data) {
		this.data = data;
	}


	public MensajeRespuestaVO getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(MensajeRespuestaVO respuesta) {
		this.respuesta = respuesta;
	}	
}
