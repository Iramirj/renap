package mx.gob.segob.dgti.infraestructure.base.presentation.security.csrf.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

/**
 * Clase que inicializa el token o lo obtiene de la sesión.
 * @author Praxis
 *
 */
public class CSRFTokenManager {
	/**
	 * The token parameter name
	 */
	static final String CSRF_PARAM_NAME = "_csrf";

	/**
	 * El lugar en donde se encuentra el token
	 */
	private final static String CSRF_TOKEN_FOR_SESSION_ATTR_NAME = HttpSessionCsrfTokenRepository.class.getName().concat(".CSRF_TOKEN");

	public static CsrfToken getTokenForSession(HttpServletRequest request) {
		CsrfToken token = null;
		HttpSession sesion = request.getSession();
		synchronized (sesion) {
			token = (CsrfToken) sesion.getAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME);
			if (null == token) {
				HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
				token = repository.generateToken(request);
				sesion.setAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME, token);
			}
		}
		return token;
	}

	/**
	 * Obtiene el valor del token desde el request
	 * 
	 * @param request
	 * @return
	 */
	public static String getTokenFromRequest(HttpServletRequest request) {
		return request.getParameter(CSRF_PARAM_NAME);
	}

	private CSRFTokenManager() {
	};
}
