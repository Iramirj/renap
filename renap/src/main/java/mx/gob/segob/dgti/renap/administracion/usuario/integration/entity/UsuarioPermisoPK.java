
package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Embeddable
public class UsuarioPermisoPK  implements java.io.Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** usuario. */
	private Usuario usuario;
	
	/** permiso. */
	private Permiso permiso;
	
	/**
	 * Obtiene usuario.
	 *
	 * @return usuario
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuario() {
		return usuario;
	}
	
	/**
	 * Establece usuario.
	 *
	 * @param usuario el nuevo usuario
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * Obtiene permiso.
	 *
	 * @return permiso
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	public Permiso getPermiso() {
		return permiso;
	}
	
	/**
	 * Establece permiso.
	 *
	 * @param permiso el nuevo permiso
	 */
	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}
	
	 public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	 
	        UsuarioPermisoPK that = (UsuarioPermisoPK) o;
	 
	        if (usuario != null ? !usuario.equals(that.usuario) : that.usuario != null) return false;
	        if (permiso != null ? !permiso.equals(that.permiso) : that.permiso != null)
	            return false;
	 
	        return true;
	    }
	 
	    public int hashCode() {
	        int result;
	        result = (usuario != null ? usuario.hashCode() : 0);
	        result = 31 * result + (permiso != null ? permiso.hashCode() : 0);
	        return result;
	    }
	
	

}
