
package mx.gob.segob.dgti.renap.administracion.usuario.integration.entity;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.gob.segob.dgti.infraestructure.autenticacion.business.constants.TipoPermisoEnum;




@Entity
@Table(name = "d_usuario_permiso")
@AssociationOverrides({
@AssociationOverride(name = "usuarioPermisoPK.usuario", joinColumns = @JoinColumn(name = "id_m_usuario")),
@AssociationOverride(name = "usuarioPermisoPK.permiso", joinColumns = @JoinColumn(name = "id_m_permiso"))
})
public class UsuarioPermiso  implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UsuarioPermisoPK usuarioPermisoPK=new UsuarioPermisoPK();
	private TipoPermisoEnum tipoPermiso;
	
   
	
	
	@EmbeddedId
	private UsuarioPermisoPK getUsuarioPermisoPK() {
		return usuarioPermisoPK;
	}

	@SuppressWarnings("unused")
	private void setUsuarioPermisoPK(UsuarioPermisoPK usuarioPermisoPK) {
		this.usuarioPermisoPK = usuarioPermisoPK;
	}
	 @Transient
	public Usuario getUsuario() {
		return getUsuarioPermisoPK().getUsuario();
	}

	public void setUsuario(Usuario usuario) {
		getUsuarioPermisoPK().setUsuario(usuario);
		
	}
	 @Transient
	public Permiso getPermiso() {
		return getUsuarioPermisoPK().getPermiso();
	}

	public void setPermiso(Permiso permiso) {
		getUsuarioPermisoPK().setPermiso(permiso);
	}

	@Column(name = "tipo_permiso", nullable = false)
	@Enumerated(EnumType.STRING)
	public TipoPermisoEnum getTipoPermiso() {
		return tipoPermiso;
	}
	
	/**
	 * Establece tipo permiso.
	 *
	 * @param tipoPermiso el nuevo tipo permiso
	 */
	public void setTipoPermiso(TipoPermisoEnum tipoPermiso) {
		this.tipoPermiso = tipoPermiso;
	}
		
	
	public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	 
	        UsuarioPermiso that = (UsuarioPermiso) o;
	 
	        if (getUsuarioPermisoPK() != null ? !getUsuarioPermisoPK().equals(that.getUsuarioPermisoPK()) : that.getUsuarioPermisoPK() != null) return false;
	 
	        return true;
	    }
	 
	    public int hashCode() {
	        return (getUsuarioPermisoPK() != null ? getUsuarioPermisoPK().hashCode() : 0);
	    }
	
	

}
