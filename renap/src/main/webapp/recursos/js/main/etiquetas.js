etiquetas_js = {
	COMUN_ACEPTAR : 'Aceptar',
	COMUN_CERRAR_SESION : 'Cerrar sesi&oacute;n',
	
	ERROR_TITULO : 'Error',
	ERROR_MENSAJE : 'Ha ocurrido un error. Favor de comunicarse con el administrador',
	SIN_RESULTADOS : 'No hay resultados',
	
	ERROR_SESION_EXP_TITULO : 'Su sesi\u00f3n ha expirado',
	LABEL_SESION_EXPIRADA : 'Por seguridad su sesi\u00f3n se ha cerrado, para continuar vuelva a ingresar al sistema',
	
	SOPORTE_NAVEGADOR_TITULO : 'Navegador no compatible',
	SOPORTE_NAVEGADOR_PARRAFO_1 : 'La versi\u00f3n del navegador no es completamente compatible con ciertas funcionalidades de la aplicaci\u00f3n.',
	SOPORTE_NAVEGADOR_PARRAFO_2 : 'Actualice m\u00ednimo a la versi\u00f3n 10 del Internet Explorer',
	SOPORTE_NAVEGADOR_CERRAR_SESSION : 'Actualice m\u00ednimo a la versi\u00f3n 10 del Internet Explorer',
	
	DATATABLE_ETIQUETAS : {
					    	"sProcessing":     'Cargando......',
					    	"sLengthMenu":     "Mostrar _MENU_ registros",
					    	"sZeroRecords":    "No se encontraron resultados",
					    	"sEmptyTable":     "No se encontraron resultados",
					    	"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					    	"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					    	"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					    	"sInfoPostFix":    "",
					    	"sSearch":         "Buscar:",
					    	"sUrl":            "",
					    	"sInfoThousands":  ",",
					    	"sLoadingRecords": "Cargando...",
					    	"oPaginate": {
					    		"sFirst":    "Primero",
					    		"sLast":     "Último",
					    		"sNext":     "Siguiente",
					    		"sPrevious": "Anterior"
					    	},
					    	"oAria": {
					    		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					    		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					    	}
					    },
	DATATABLE_ETIQUETAS_ERROR : 'A ocurrido un error al recuperar la informaci\u00f3n comuniquese con su administrador',
	
	SEGURIDAD_MENSAJE_TITULO : 'Permisos Insuficientes',
	SEGURIDAD_MENSAJE_MENSAJE : 'Si desea acceder a esta opci\u00f3n, contacte a su administrador del sistema',
	SEGURIDAD_MENSAJE_TITULO_INS : 'Informaci\u00f3n insuficiente ',
	SEGURIDAD_MENSAJE_MENSAJE_INS : "Faltan definir opciones de elemento a evaluar formulario.evalua",
	
	VALIDADOR_MENSAJES : {
							required: "Este campo es obligatorio.",
					  	  	remote: "Por favor, rellena este campo.",
					  	  	email: "Por favor, escribe una direcci\u00f3n de correo v\u00e1lida",
					  	  	url: "Por favor, escribe una URL v\u00e1lida.",
					  	  	date: "Por favor, escribe una fecha v\u00e1lida.",
					  	  	dateISO: "Por favor, escribe una fecha (ISO) v\u00e1lida.",
					  	  	number: "Por favor, escribe un n\u00famero entero v\u00e1lido.",
					  	  	digits: "Por favor, escribe s\u00f3lo d\u00edgitos.",
					  	  	creditcard: "Por favor, escribe un n\u00famero de tarjeta v\u00e1lido.",
					  	  	equalTo: "Por favor, escribe el mismo valor de nuevo.",
					  	  	accept: "Por favor, escribe un valor con una extensi\u00f3n aceptada.",
					  	  	maxlength: jQuery.validator.format("Por favor, no escribas m\u00e1s de {0} caracteres."),
					  	  	minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
					  	  	rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
					  	  	range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
					  	  	max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
					  	  	min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
					  	  	/*Etiquetas metodos personalizados*/
					        requerido: "Este campo es obligatorio.",
					        fecha: "La fecha no cumple con el formato (dd/mm/yyyy).",
					        numero: "Solo se permiten n\u00fameros.",
					        decimales : "Solo se permiten n\u00fameros decimales.",
					        texto: "El texto contiene caracteres no validos",
					        datoVacio: "Dato requerido",
					        jpg: "Solo se admiten Imagenes de formato .jpg"
					        	  
					    },
					    
					
};


//# \u00bf -> ¿
//# \u00e1 -> á
//# \u00e9 -> é
//# \u00ed -> í
//# \u00f3 -> ó
//# \u00fa -> ú
//# \u00f1 -> ñ
//
//# \u00c1 -> Á
//# \u00c9 -> É
//# \u00cd -> Í
//# \u00d3 -> Ó
//# \u00da -> Ú
//# \u00d1 -> Ñ