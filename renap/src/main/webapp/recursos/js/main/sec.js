
var sec = (function() {	

	var perfilesUsuario=null;
	var perfilesPermitidosUsuario = [];
	
	var defaultValores = {
							"idElemento" : null,				//un elemento
							"tipoEvaluacion" : 'BLOQUE',		// 'BLOQUE', 'ELEMENTO', 'ACCESO'  //En que contexto se evaluara para inhabilitar							
							"botonesVisibles" : false,			//Si se debe visualizar los controles de boton
							"permisos" : [],					//Arreglo de permisos ['permiso1', 'permiso2']
							"funcionEvaluaCriteriosExtra" : null//Si existe una funcion personalizada que inhabilita *************///No implementada 
																// controles para otros criterios una vez que tiene el perfil permitido
																// debe retornar "false" para inhabilitar los controles
						 };
	
	var opcionesEvaluacion = null;
	
	getPerfilUsuario = function(){
							var opcionesAjax = {
								url :  moduloGeneral.getUrlConContexto("/validarPermisos"),
								async : false,
								success:function(perfiles){
									perfilesUsuario = perfiles;
									
									if(perfilesUsuario == null || perfilesUsuario.length == 0){
										redirectErrorPerfil();
										return true;
									}									
								},
								beforeSend : function(){
									bloqueaPantalla();
								},
								complete : function(){
									desbloqueaPantalla();
								},
								error: function(jqXHR, textStatus, errorThrown){
									redirectErrorPerfil();
								}								
							};		
							moduloGeneral.ajaxSubmit(opcionesAjax);
						};	

	
	
	bloqueaPantalla = function(){
						var divOverlay = $("<div></div>", { "id" : "overlayId",
							"class" : "overlay"
						  });
						var divWaitOverlay = $("<div></div>", {"id" : "overlayModalId",
										"class" : "overlayModal col-md-offset-4 col-md-4"});
						divWaitOverlay.html("<i  class='i-spin animate-spin'></i><span>Cargando...</span>");				 
						
						var documentElement = $(document.body);
						documentElement.append(divOverlay);
						documentElement.append(divWaitOverlay);
					};
					
	
	
	desbloqueaPantalla = function(){
							$("#overlayModalId").remove();
							$("#overlayId").remove();
						};
	
	redirectErrorPerfil = function(){
								BootstrapDialog.show({
									type : BootstrapDialog.TYPE_WARNING,
									title : etiquetas_js.SEGURIDAD_MENSAJE_TITULO,
									message : etiquetas_js.SEGURIDAD_MENSAJE_MENSAJE,
									closable : false, 
									buttons : [{
										label : etiquetas_js.COMUN_ACEPTAR,
										cssClass : 'btn-warning',
										action : function(dialog) {
											$("#formRedirectInicio").submit();
										}
									}]
								});
												
						};
	
	validaElemento = function(index, opcion){
					var elementoAEvaluar = $.extend({}, defaultValores, opcion);
					var esUsuarioConPerfil = contienePerfil(elementoAEvaluar.permisos);
					
					var opcionValida = esUsuarioConPerfil;
					if(opcionValida){
						if(elementoAEvaluar.funcionEvaluaCriteriosExtra != null){
							opcionValida =  elementoAEvaluar.funcionEvaluaCriteriosExtra();
						}
					}
					
					if(!opcionValida){
						if( elementoAEvaluar.tipoEvaluacion =='ACCESO' ){
							redirectErrorPerfil();
						} else{							
							var elemento = $("[id='"+elementoAEvaluar.idElemento+"']");
							if(elemento != null && elemento.length != 0){
								if(elementoAEvaluar.tipoEvaluacion =='BLOQUE'){
									inhabilitaElementoBloque(elemento, elementoAEvaluar.botonesVisibles);	
								} else if( elementoAEvaluar.tipoEvaluacion =='ELEMENTO' ){
									var visible = true;
									if( $(elemento).hasClass("btn") || (  elemento.type != null && elemento.type.toUpperCase() == 'BUTTON')){
										visible = elementoAEvaluar.botonesVisibles;
									}						
									if( $(elemento).hasClass("elementoSeguridad")){
										visible = elementoAEvaluar.botonesVisibles;
									}
									
									inhabilitaElemento(elemento, visible);
								}	
							}
						}
					}
				};
				
	inhabilitaElementoBloque = function(elementoBloque, botonesVisibles){
									var inputs = $(":INPUT[type!='button']", elementoBloque);
									$.each(inputs, function(index, inputElement){			
										inhabilitaElemento(inputElement, true);
									});
									var botones = $(".btn, INPUT[type='button']", elementoBloque);
									$.each(botones, function(index, elementoHtml){
										inhabilitaElemento(elementoHtml, botonesVisibles);
									});
									$(".i-calendario", elementoBloque).closest(".input-group-addon").remove();
								};
	
	inhabilitaElemento = function(elemento, visible){
							var tagName = $(elemento).prop("tagName").toUpperCase();
							
							$(elemento).prop('disabled','disabled');
							if(!visible){
								$(elemento).addClass("hidden");	
							}
							
							if( tagName == 'INPUT' && 
								(elemento.type.toUpperCase() == 'RADIO' || elemento.type.toUpperCase() == 'CHECKBOX')){
									$(elemento).bootstrapSwitch('disabled',true);	
							}
							if(tagName.toUpperCase() == 'SELECT'){
								var isSelectize = ( $(elemento)[ 0 ].selectize != null );
								if(isSelectize){
									$(elemento)[ 0 ].selectize.disable();
								}
								$(elemento).trigger("chosen:updated");								
							}		
						};
						
	contienePerfil = function(perfiles) {
						
						if( perfilesUsuario == null) {
							getPerfilUsuario(); 
						}
						
						var contiene = false;
						if(perfilesUsuario != null) {
							$.each(perfiles, function(index, perfil){
								if(perfilesUsuario.indexOf(perfil) != -1) {
									contiene = true;	
								}							
							});
						}
						return contiene;
					};
	 
	evaluarPermisos = function() {
								opcionesEvaluacion = perfilesPermitidosUsuario;
								if( opcionesEvaluacion == null ||  !(opcionesEvaluacion instanceof Array) ){
									BootstrapDialog.show({
										type : BootstrapDialog.TYPE_WARNING,
										title : etiquetas_js.SEGURIDAD_MENSAJE_TITULO_INS,
										message : etiquetas_js.SEGURIDAD_MENSAJE_MENSAJE_INS,
										closable : false,  
										buttons : [{
											label : etiquetas_js.COMUN_ACEPTAR,
											cssClass : 'btn-warning',
											action : function(dialog) {
												location.href = moduloGeneral.getUrlConContexto('/');
											}
										}]
									});
																	
									return;
								}	
								
								
								if(perfilesUsuario == null){
									getPerfilUsuario();	
								}
								bloqueaPantalla();								
								$.each(opcionesEvaluacion, validaElemento);
								desbloqueaPantalla();
						};
	
	/**
	 * Metodo que agrega permisos a la pantalla para evaluar si se inhabilita un bloque un elemento o se rechaza el acceso
	* Objeto con estructura de permiso a evaluar
	*
	* permiso = {
	*							"idElemento" : null,				//un elemento
	*							"tipoEvaluacion" : 'BLOQUE',		// 'BLOQUE', 'ELEMENTO', 'ACCESO'  //En que contexto se evaluara para inhabilitar							
	*							"botonesVisibles" : false,			//Si se debe visualizar los controles de boton
	*							"permisos" : [],					//Arreglo de permisos ['permiso1', 'permiso2']
	*							"funcionEvaluaCriteriosExtra" : null//Si existe una funcion personalizada que inhabilita ************* ///No implementada 
	*																// controles para otros criterios una vez que tiene el perfil permitido
	*																// debe retornar "false" para inhabilitar los controles
	*						 };
	*/
	agregarPermisoPantalla = function(permiso){
		perfilesPermitidosUsuario.push(permiso);
	};
	
	
	 // public API
    return { 
    	contienePerfil : contienePerfil,
    	agregarPermisoPantalla : agregarPermisoPantalla,
    	evaluarPermisos : evaluarPermisos
    };
    
    
})();