$.ajaxSetup({ cache:false });

var moduloGeneral = (/**
 * @returns {___anonymous14785_15817}
 */
function() {

	/**
	 * contexto De la aplicacion se inicializa en el archivo paginas/plantilla/mainLayout.html
	 */
	var contextoAplicacion = "";
	var DATE_FORMAT = { "ESTANDAR" : "DD/MM/YYYY"};


	/**
	 * 
	 */
	init = function(context){
					contextoAplicacion = context;	
					moduloValidador.inicializaValidator();
					inicializatooltip(".tooltip");
					__inicializaSoporteNavegadores();
			};
			
	/**
	 * 
	 */
	__inicializaSoporteNavegadores = function() {
		   var optionesBrowserPermitido = {
			        header: etiquetas_js.SOPORTE_NAVEGADOR_TITULO,
			        paragraph1 : etiquetas_js.SOPORTE_NAVEGADOR_PARRAFO_1,
			        paragraph2 : etiquetas_js.SOPORTE_NAVEGADOR_PARRAFO_2,
			        close : true,
			        closeLink :etiquetas_js.COMUN_CERRAR_SESION,
			        closeESC : false, 
			        closeMessage :'',
			        closeURL: "javascript:document.getElementById('logout').submit();",
			        browserShow :false,        
			         reject : {
			             all: false,
			             msie: 10
			         }
			    };
			     $.reject(optionesBrowserPermitido);
			};
				
			
	/**
	 * 
	 */							
	getUrlConContexto = function(url){
					    	return contextoAplicacion+url;
					    };
					    
    /**
	 * metodo que redirecciona a la pantalla del login
	 */
	redirectLogin = function(){					
						BootstrapDialog.show({
							type : BootstrapDialog.TYPE_WARNING,
							title : etiquetas_js.ERROR_SESION_EXP_TITULO,
							message : etiquetas_js.LABEL_SESION_EXPIRADA,
							closable : false, 
							buttons : [{
								label : etiquetas_js.COMUN_ACEPTAR,
								cssClass : 'btn-warning',
								action : function(dialog) {
									window.location.href = moduloGeneral.getUrlConContexto('/login');
								}
							}]
						});
	};
	
	
	
	/**
     * Funcion que muestra los errores que devuelve el tipo de objecto MensajeRespuestaVO
     *
     * resultado 									: objecto MensajeRespuestaVO JSON serializado
     * selectorForm									: El formulario sobre el cual se evaluara si existen errores
     * selectorGlobalErrorSection (opcional, defaul-'#errorGlobal') 		: selector de la seccion donde se colocaran los errores globales,
     * 																		  si es una seccion diferente a la seccion de default '#errorGlobal',
     * 																		  debe contener la class 'errorGlobal'
     */
	mostrarErrores = function (resultado, selectorForm, selectorGlobalErrorSection){
					    	limpiarErrores();
					    	//Errores por parametro
					    	if(resultado.erroresCampo != null){
					    		$.each($(":INPUT", selectorForm), function(){
					    			var input = this;
					    			var errores = resultado.erroresCampo[input.name];
					    			if(errores != null && errores.length > 0){
					    				generaErrorDiv(input, errores);    				
					    			}
					    		});
					    	}
					
					    	selectorGlobalErrorSection = selectorGlobalErrorSection == null? '#errorGlobal':selectorGlobalErrorSection;
					
					        if(resultado.mensaje != null ){
					            $(selectorGlobalErrorSection).html("<span>"+resultado.mensaje+"</span><br></br>");
					            $(selectorGlobalErrorSection).show(); 
					        }
					
					    	if(resultado.erroresGlobales!=null && resultado.erroresGlobales.length>0){
					                var span = $("<span></span>");
					    		$(span).html(resultado.erroresGlobales);
					    		$(selectorGlobalErrorSection).append(span).show();           
					    	}
					    };
	
	 /**
     * Funcion que limpia los mensajes de error en general.
     * Se eliminan los errores globales identificados por el id '#errorGlobal' y la clase '.errorGlobal'
     * Se eliminan los errores de cada campo de captura
     */
	limpiarErrores = function(){
			    	$('#errorGlobal').empty().hide();
			    	$('.errorGlobal').empty().hide();
					$(".hasError").removeClass("hasError");
					$(".error").parent().hide().remove();
			    };
			    
			 
	/**
	 * 
	 * @param inputSelector
	 * @param errores
	 */   
	generaErrorDiv = function(inputSelector, errores){
					var inputReferencia = $(inputSelector)[0];
			    	
					
			        if(errores != null && errores.length != 0){
				        	
					        var divWarning = $("<div></div>" );										    	
			                $.each(errores, function(){
			                        var error = this;
			                        var label = $("<label></label>", {class:"error"});
			                        $(label).html(error);
			                                        
			                        $(divWarning).append(label);
			                });
			                $(inputReferencia).addClass("hasError");
			                $(inputReferencia).parent().append(divWarning);
			        }
			    };
			    

	/**
	 * 
	 * @param selector
	 * @returns {___anonymous5748_5749}
	 */
	convertirFormularioJson = function(selector)
			    {
			        var objecto = {};
			        var arrayFormulario = $(selector).serializeArray();
			        $.each(arrayFormulario, function() {
			            if (objecto[this.name] !== undefined) {
			                if (!objecto[this.name].push) {
			                	objecto[this.name] = [objecto[this.name]];
			                }
			                objecto[this.name].push(this.value || '');
			            } else {
			            	objecto[this.name] = this.value || '';
			            }
			        });
			        return objecto;
			    };
			    
			    
	inicializatooltip = function(selector){
				    // inicializar tooltips
				    $(selector).tooltip();
				};
	
				
				
	/**
	 * 
	 * @param selector
	 * @param options
	 */
	inicializarSelectize = function (selector, options){
			    	var optionsDefault = {
			                create: false,
			                sortField: 'text',
			                onChange: function() {
			                	$("#"+this.$input.context.id+"errors").parent().find(".errores").remove();
			                	$("#"+this.$input.context.name+"errors").parent().find(".errores").remove();
			          	    }
			    	};
			    	var opts =  $.extend({},optionsDefault,options);
			
			    	$(selector).selectize(opts);
			    };
			    
			    
	/**
	 * 
	 * @param selector
	 */
	inicializarChoosen	= function (selector){
			    	$(selector).chosen({
			            width: '100%',
			            no_results_text: etiquetas_js.SIN_RESULTADOS,
			            search_contains: true
			       }).change(function(){
			    	   $("#"+this.id+"errors").remove();
			       	   $("#"+this.name+"errors").remove();
			       });
			    };
			    
	/**
	 * 
	 * @param selectorForm
	 */		    
	limpiarFormulario = function (selectorForm) {
			    	var formToClean = $(selectorForm);
			    	formToClean[0].reset();
			
			    	$("select.selectized", formToClean).each(function(){
						$(this)[ 0 ].selectize.clear();
					});
			
			    	$("select.chosen", formToClean).trigger("chosen:updated");
			
			    	$("textarea", formToClean).val("");
			    };
	
	__getHeaderToken = function(){
		var token = $("meta[name='_csrf']").attr("content");
		var headerToken = $("meta[name='_csrf_header']").attr("content");	    					
		var headers = {};
		
		headers[headerToken] = token;
		return headers;
	};
	    /**
	     * Funcion que ejecuta una funcion ajax las opciones disponibles a agregar son 
	     *var opcionesDefault = {
									    url  : null,		//Url del servicio que atendera la peticion
										type : "POST",		//Tipo de peticion a enviar : default - POST
										data : {},			//Datos a enviar
										async : true, 		//Asincrono activado por default
						                cache: false,
										beforeSend 	: null,	//Funcion a ejecutar previo a enviar la peticion
										complete 	: null, //Funcion a ejecutar una vez que se concluya la peticion, sea error o exito
										error 		: null,	//Funcion a ejecutar cuando es un error
										success		: null	//funcion a ejecutar cuando es una respuesta de exito
									};
	     *
	     *
	     *
	     */			    
	    ajaxSubmit = function(opciones){			
	    					
	    	
							var opcionesDefault = {
									url  : null,		//Url del servicio que atendera la peticion
									type : "POST",		//Tipo de peticion a enviar : default - POST
									data : {},			//Datos a enviar
									async : true, 		//Asincrono activado por default
					                cache: false,
									beforeSend 	: null,	//Funcion a ejecutar previo a enviar la peticion
									complete 	: null, //Funcion a ejecutar una vez que se concluya la peticion, sea error o exito
									error 		: null,	//Funcion a ejecutar cuando es un error
									success		: null	//funcion a ejecutar cuando es una respuesta de exito
								};
					
					
							var opcionesAjax = $.extend({}, opcionesDefault, opciones);
					
					
							
							
							$.ajax({
								url: opcionesAjax.url,
								type : opcionesAjax.type,
								data : opcionesAjax.data,
								headers : __getHeaderToken(),
								beforeSend :  function(){
					                            if(opcionesAjax.beforeSend !=  null){
					                                opcionesAjax.beforeSend();
					                            }
					                        },
								complete : function() { 
					                            if(opcionesAjax.complete !=  null){
					                                opcionesAjax.complete();
					                            }
					                        },
								async : opcionesAjax.async,
								global : true,
								success : opcionesAjax.success,
								error : function(jqXHR, textStatus, errorThrown){
									if( jqXHR != null &&
											jqXHR.status != null && 
											jqXHR.status == 401 ){
										if(moduloGeneral != null && moduloGeneral.redirectLogin != null){
											moduloGeneral.redirectLogin();
										}
									} else{
										if(opcionesAjax.error != null){
											opcionesAjax.error(jqXHR, textStatus, errorThrown);
										} else {
											_muestraError();
										}
									}
								}
							});
					    };	
								    
								    
	    /**
	     * Funcion que ejecuta ejecuta una peticion ajax y reemplaza fragmentos de html de manera asincrona de un selector especifico 
	     * 
	     * selectorToReplace : selector de el tag que sera reemplazado por un nuevo codigo html 
	     * url : url del servicio que devuelve el fragmento html nuevo que reemplazara
	     * opcionesAdicionales = {
									    data : {},					//Datos a enviar
									    method : GET				//Tipo de peticion a enviar : default - GET
									    functionCallback : null 	//Funcion a ejecutar una vez concluido el reemplazo de codigo  
									    								( utilizado para reinicializar los componentes )
										error 		: null,	//Funcion a ejecutar cuando es un error
									};
	     *
	     *
	     *
	     */						    
		replaceHtmlFragment = function(selectorToReplace, url, opcionesAdicionales){
							    	var opcionesAjax = {
											url : url,
											data : opcionesAdicionales.data,
											type : opcionesAdicionales.method==null?"GET":options.method,
											success : function(result){
												$(selectorToReplace).replaceWith(result);
												if(functionCallback){
													opcionesAdicionales.functionCallback();
												}
											},
											error : function(jqXHR, textStatus, errorThrown){
							                    if(options.error != null){
							                    	options.error(jqXHR, textStatus, errorThrown);
							                    }else
							                    	_muestraError();
												}
											};
							    	moduloGeneral.postAjaxSubmit(opcionesAjax);
							    };
			    
		/**
		*
		*/
		_muestraError = function(){
								BootstrapDialog.show({
									type : BootstrapDialog.TYPE_DANGER,
									title : etiquetas_js.ERROR_TITULO,
									message : etiquetas_js.ERROR
								});
						};
		/**
		 * Formatea una fecha a una cadena
		 * @param date
		 * @returns
		 */
		formatDate = function(date) {
					var dateFormat = moment(date).format(DATE_FORMAT.ESTANDAR);
					return dateFormat;
				};
				
		/*
		 * El evento agrega la animacion de un spinner a el boton.
		 * Debe contener los siguientes atributos 
		 * data-style="expand-left"
		 */
		agregaAnimacionSppiner = function(buttonSelector) {

			var isAttrExpand = $(buttonSelector)[0].hasAttribute("data-style");
			if(!isAttrExpand){
				$(buttonSelector).attr("data-style", "expand-left");	
			}
			
			var isLaddaButtonClass = $(buttonSelector).hasClass( "ladda-button" ); 
			if(!isLaddaButtonClass) {
				$(buttonSelector).addClass("ladda-button");	
			} 
			
			
			$(buttonSelector).ladda( 'bind' );
		};
		
		detenerAnimacionesSppiner = function() {
			$.ladda( 'stopAll' );
		};
		
		//Ejecutar accion
		
		
		/**
		 * @param accion : URL de la accion a ejecutar
		 * @param formulario : Formulario o control que sera serializado
		 * @param selectorMensaje : Boton que se desea bloquear durante la operacion
		 * @param objeto : Objeto que se va a serializar si no se incluye un formulario
		 */
		ejecutarAccion = function(accion,formulario,selectorMensaje,functionAfterAccion,objeto) {
			
			limpiarErrores();
	    	var datos="";
	    	if(objeto === undefined){
	    		datos=$(formulario).serialize();
	    	}else{
	    		datos=$(formulario).serialize() + '&' + $.param(objeto);
	    	}

	    	
	    	 var opcionesAjax = {
					url : accion,
					data : datos,
					success : function(mensaje){
						$(selectorMensaje).html(mensaje.mensaje);
						limpiarErrores();
	    	},
					error : function(jqXHR, textStatus, errorThrown){
							detenerAnimacionesSppiner();
				    		mostrarErrores(jqXHR.responseJSON, selector);
	    	},
					complete : function(){
						detenerAnimacionesSppiner();
						if(functionAfterAccion !== undefined){
							functionAfterAccion();
						}

					}
				};
			ajaxSubmit(opcionesAjax);
		
		};
//		
		   /*
		    * @selectorTitulo: nombre del elemento que contendra el titulo del modal
		    * @mensaje       : Mensaje que se va a mostrar en el modal
		    * @funcionConfirmacion: funcion que se debera ejecutar cuando se seleccione si
		    * @selectorMensaje: Elemento donde se va a escribir el mensaje
		    * @selectorModal: Elemento que se destina como modal 
		    * 
		    * */
		modalConfirmacion= function(selectorTitulo,mensaje,funcionConfirmacion,selectorMensaje,selectorModal)
		    {
		    	$(selectorMensaje).html(mensaje);
		    	$(selectorModal).find("#tituloModal").html(selectorTitulo);
		    	$("#si").one("click",function(){
		    		funcionConfirmacion();
		    	});

		    	$("#no").one("click",function(){
		    		$(selectorModal).modal('hide');


		    	});
		    };
			    
	return {
		init : init,		
    	getUrlConContexto : getUrlConContexto,
    	redirectLogin : redirectLogin,
    	limpiarFormulario : limpiarFormulario,
    //manejo de errores para el control
    	limpiarErrores : limpiarErrores,
    	mostrarErrores : mostrarErrores,
    	generaErrorDiv : generaErrorDiv,
    	convertirFormularioJson :convertirFormularioJson,
    //inicializa controles html
    	inicializatooltip : inicializatooltip,
    	inicializarSelectize : inicializarSelectize,
    	inicializarChoosen :inicializarChoosen,    	
    //peticiones ajax 
    	//peticion general ajax
    	ajaxSubmit : ajaxSubmit,
    	//ejecuta una accion mediante peticion ajax
    	ejecutarAccion : ejecutarAccion,
    	//genera peticion asincrona espera textplano a reemplazar
    	replaceHtmlFragment : replaceHtmlFragment,
    //format fecha
    	formatDate:formatDate,
    //Para la animacion del boton
    	agregaAnimacionSppiner : agregaAnimacionSppiner,
    	detenerAnimacionesSppiner : detenerAnimacionesSppiner,
    //generacion de un modal generico
    	modalConfirmacion : modalConfirmacion
    };

})();