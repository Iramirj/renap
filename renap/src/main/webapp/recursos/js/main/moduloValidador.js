var moduloValidador = (function() {

	/**
	 * Inicializa las etiquetas generales del plugin jqueryValidator
	 */
	__inicializaEtiquetasGenerales = function(){
										jQuery.extend(jQuery.validator.messages, etiquetas_js.VALIDADOR_MENSAJES);
									};
   
	/**
	 * Agrega los metodos personalizados para uso del validator
	 */
	__agregaMetodosPersonalizados = function(){
								        jQuery.validator.addMethod("soloNumeros", _evaluaSoloNumeros, etiquetas_js.VALIDADOR_MENSAJES.numero);
								        jQuery.validator.addMethod("soloDecimales", _evaluaDecimales, etiquetas_js.VALIDADOR_MENSAJES.decimales);
								        jQuery.validator.addMethod("soloCaracteres", _evaluaCaracteres, etiquetas_js.VALIDADOR_MENSAJES.texto);
								        jQuery.validator.addMethod("formatoFechas", _evaluaFechas,  etiquetas_js.VALIDADOR_MENSAJES.fecha);
								        jQuery.validator.addMethod("noVacio", _evaluaVacio, etiquetas_js.VALIDADOR_MENSAJES.datoVacio);
								        jQuery.validator.addMethod("validaExtensionJpg", _evaluaJpg, etiquetas_js.VALIDADOR_MENSAJES.jpg);
								
									};
									
									
	/**
	 * Evalua elementos unicamente numericos enteros
	 */
	_evaluaSoloNumeros = function(value, element) {
						        return this.optional(element) || /^\d*$/i.test(value);
						    };

    /**
	 * Evalua elementos unicamente numericos decimales
	 */
    _evaluaDecimales = function(value, element) {
					        return this.optional(element) || /^\d*[\.]*\d*$/i.test(value);
					    };

    /**
	 * Evalua elementos unicamente cadenas con ciertos caracteres .\-_()\/:
	 */
    _evaluaCaracteres = function(value, element) {
					        return this.optional(element) || /^[a-zA-Z0-9\s\u00f1\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da.\-_()\\/:]*$/i.test(value);
					    };
					    
    /**
	 * Evalua elementos de fechas
	 */
    _evaluaFechas = function(value, element) {
				        return this.optional(element) || /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]\d\d\d\d$/i.test(value);
				    };
				    
    _evaluaJpg = function(value, element) {
				        if (!value.match(/(?:jpg)$/)) {
				            return false;
				        } else {
				            return true;
				        }
				    };
				    
    /**
	 * Evalua el contenido no debe ser vacio
	 */    
    _evaluaVacio = function(value, element) {
				        if (value === null || value === "") {
				            return false;
				        } else {
				            return true;
				        }
				    };

    /**
	 * Inicializa las cuestiones generales del validador
	 */    
    inicializaValidator = function() {
					    	__inicializaEtiquetasGenerales();
					    	__agregaMetodosPersonalizados();
					
					    	//Configuraciones generales
					        jQuery.validator.setDefaults({
						            errorPlacement: function(error, element) {
						                moduloGeneral.generaErrorDiv(element, error);
						            },
						            errorElement: "div",
						            onkeyup: false,
				                    onfocusout: false
					        });
					    };


    return {
        inicializaValidator: inicializaValidator
    };
})();


