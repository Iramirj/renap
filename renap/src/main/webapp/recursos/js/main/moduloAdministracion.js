var moduloAdministracion = (function() {
	
	function guardar(accion,selector,objeto){
    	moduloGeneral.limpiarErrores(selector);
    	var datos="";
    	if(objeto === undefined){
    		datos=$(selector).serialize();
    	}else{
    		datos=$(selector).serialize() + '&' + $.param(objeto);
    	}

    	$('#guardar').empty().html('<i class="i-spin animate-spin"></i>Guardando').attr('disabled', true);

    	 var opcionesAjax = {
				url : accion,
				data : datos,
				success : function(mensaje){
    		$("#mensajeExito").html(mensaje.mensaje);
   		$(".exito").fadeOut(1900).fadeIn(2200).fadeOut(1800);
    		moduloGeneral.limpiarErrores(selector);
    		limpiar();



			grid.draw();

    	},
				error : function(jqXHR, textStatus, errorThrown){
					 $('#guardar').empty().html('<i class="i-guardar"></i> Guardar').removeAttr('disabled');
			    		moduloGeneral.mostrarErrores(jqXHR.responseJSON, selector);
    	},
				complete : function(){
					///Quita el efecto del boton guardar y el que tiene el id si
					$("#si").empty().html('Aceptar').removeAttr('disabled');
		    	    $('#guardar').empty().html('<i class="i-guardar"></i> Guardar').removeAttr('disabled');

				}
			};
		moduloGeneral.postAjaxSubmit(opcionesAjax);
    }

	
})();