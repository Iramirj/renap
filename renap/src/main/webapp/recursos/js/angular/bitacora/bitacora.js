'use strict';

angular.module('appRenap', []).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] =     $('meta[name="csrf-token"]').attr('content');
}]).factory('bitacoraService',  function($http, $q){
	return{
		__getHeaderToken: function(){
			var token = $("meta[name='_csrf']").attr("content");
			var headerToken = $("meta[name='_csrf_header']").attr("content");	    					
			var headers = {};
			
			headers[headerToken] = token;
			return headers;
		},
		retrieveEntidadBitacora: function (idPais){
			console.log("retrieveEntidadBitacora: " + idPais);
			var defer = $q.defer();
			$http.get('renap/modules/home/retrieveEntidadBitacora/' + idPais)
			.success(function(data){
				console.log("Exito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtubo respuesta del servidor: " + data);
				defer.reject(data);
			});
			return defer.promise;
		},		
		getListFilter: function (flDatos){
			console.log("getListFilter: ");
			var defer = $q.defer();
			$http({
				method: 'POST',
				url: 'renap/modules/bitacora/getListFilter',
				data    : JSON.stringify(flDatos),
				headers: __getHeaderToken()				
			})			
			.success(function(data){
				console.log("Exito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtubo respuesta del servidor: " + data);
				defer.reject(data);
			});
			return defer.promise;
		}
	};
}).controller('BitacoraController',
		[ 'bitacoraService', '$scope', '$http', '$location', function( bitacoraService, $scope, $http, $location) {

			$scope.idPais = 0;
			$scope.objEntidad = null;
			$scope.objBitacora = null;
			$scope.resultMesagge = "none";
			$scope.headTextMesagge = "";
			$scope.tipoAlerta = "";
			
			$scope.retrieveEntidadBitacora = function(){
				bitacoraService.retrieveEntidadBitacora($scope.idPais).then(function(data) {
					$scope.objEntidad = data;
				});		
			};
			
			$scope.getListFilter = function(){
				$scope.resultMesagge = "none";
				var banderaFecha = false;
				var entidad = document.getElementById("slcEntidad").value;
				var accion = document.getElementById("slcAccion").value;
				var slcPerfil = document.getElementById("slcPerfil").value;
				var fechaHasta = document.getElementById("cHasta").value;
				var fechaDesde = document.getElementById("cDesde").value;
				var idUsuario = document.getElementById("slcUsuarios").value;
				
				if(entidad == ""){
					entidad = null;
				}
				if(accion == ""){
					accion = null;
				}
				if(slcPerfil == ""){
					slcPerfil = null;
				}
				if(fechaHasta == ""){
					fechaHasta = null;
				}
				if(fechaDesde == ""){
					fechaDesde = null;
				}
				if(idUsuario == ""){
					idUsuario = null;
				}
				if(fechaHasta != null && fechaDesde == null){
					$scope.textMesagge = "Por favor ingrese la fecha inicial";
					banderaFecha = true;
					$scope.resultMesagge = "block";
					$scope.headTextMesagge = "";
					$scope.tipoAlerta = "alert-warning";
				}
				if(fechaDesde != null && fechaHasta == null){
					$scope.textMesagge = "Por favor ingrese la fecha final";
					banderaFecha = true;
					$scope.resultMesagge = "block";
					$scope.headTextMesagge = "";
					$scope.tipoAlerta = "alert-warning";
				}
				
				if(banderaFecha == false){
					var flDatos = {
							idMUsuario: idUsuario,
							idCEntFed: entidad,
							idCAccion: accion,
							idPerfil: slcPerfil,
							fechaDesde: fechaDesde,
							fechaHasta: fechaHasta						
					};
					bitacoraService.getListFilter(flDatos).then(function(data) {
						$scope.objBitacora = data;
					});	
				}				
			};
			
			$scope.limpiaBusqueda = function(){
				slcUsuarios.empty;
				slcPerfil.empty;
				slcEntidad.empty;
				cDesde.empty;
				cHasta.empty;
			};
			
		} ]);