'use strict';

angular.module('appRenap', []).factory('homeService', function($http, $q){
	return{
		getName: function (flName){
			console.log("getName: " + flName);
			var defer = $q.defer();
			$http.get('renap/home/pruebaInicial/returnName/' + flName)
			.success(function(data){
				console.log("Exito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtubo respuesta del servidor: " + data);
				defer.reject(errorData);
			});
			return defer.promise;
		}
	};	
});