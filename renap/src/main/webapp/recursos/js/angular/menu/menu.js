'use strict';

var app = angular.module('appRenap', []);

app.factory('menuService', function($http, $q) {
	return {
		__getHeaderToken : function() {
			var token = $("meta[name='_csrf']").attr("content");
			var headerToken = $("meta[name='_csrf_header']").attr("content");
			var headers = {};

			headers[headerToken] = token;
			return headers;
		}
	};
});

app.controller('MenuController', [ 'menuService', '$scope', '$http',
		'$location', function(menuService, $scope, $http, $location) {

		} ]);