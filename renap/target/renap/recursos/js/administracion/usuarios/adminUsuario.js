administracionUsuarios = (function() {

	var gridTableUsuarios = "#tableUsuario";
	var formParametroBusqueda = "#formUsuario";
	var formAgregarUsuario = "#formAgregarUsuario";
	var modalEstatus = "#modalEstatus";
	var modalGenerico = "#modalGeneral";
	var MENSAJE_REINICIAR_PASSWORD = "&iquest; Esta seguro que desea reiniciar el password de este usuario?";
	var TITULO_MODAL_REINICIO = "Reiniciar Password";
	var TITULO_MODAL_DESBLOQUEO = "Desbloquear usuario";
	var MENSAJE_DESBLOQUEO = "&iquest; Esta seguro que desea desbloquear este usuario?";

	// funcion para inicializar
	init = function() {
		inicializaGridResultados();
		inicializaControlesBusqueda();
		inicializaControlesOpcionesUsuario();
		buscarUsuarios();
	};

	// funcion para inicializar el grid de resultados
	inicializaGridResultados = function() {

		var columnas = [
				{
					"data" : "nombreCompleto",
					"sDefaultContent" : ""
				},
				{
					"data" : "idUsuario",
					"sDefaultContent" : ""

				},
				{
					"data" : "listaPerfiles",
					"render" : function(data, type, full, meta) {

						if (data != null && data.length > 0) {
							var perfiles = "";
							for (var i = 0; i < data.length; i++) {
								perfiles = perfiles
										+ "<div class=\"perfiles \"><span class=\"glyphicon glyphicon-triangle-right\"></span>"
										+ data[i].descripcion + "</div>";
							}
							return perfiles;
						} else {
							return "";
						}

					}
				},

				{
					"data" : "activo",
					"render" : function(data, type, full, meta) {

						if (data) {
							return 'Activo';
						} else {
							return 'Inactivo';
						}
					}
				},
				{

					"data" : "bloqueado",
					"render" : function(data, type, full, meta) {

						if (data) {
							return "<div class=\"accion\"><button   class=\" desbloquear btn btn-primary btn-xs ctooltip \" title=\"Desbloquear Usuario\" ><span class=\"glyphicon glyphicon-ban-circle\"></span></button> <button   class=\" editar btn btn-primary btn-xs ctooltip \" title=\"Editar Usuario\" ><span class=\"glyphicon glyphicon-pencil\"></span></button>  <button   class=\" reiniciarPass btn btn-primary btn-xs ctooltip \" title=\"Reiniciar Password\"><span class=\"glyphicon glyphicon-random\"></span></button>  <button   class=\" desactivar btn btn-primary btn-xs ctooltip \" title=\"Cambiar Estatus\"><span class=\"glyphicon glyphicon-remove-circle\"></span></button></div> ";

						} else {
							return "<div class=\"accion\"><button   class=\" editar btn btn-primary btn-xs ctooltip \" title=\"Editar Usuario\" ><span class=\"glyphicon glyphicon-pencil\"></span></button> <button   class=\" reiniciarPass btn btn-primary btn-xs ctooltip \" title=\"Reiniciar Password\"><span class=\"glyphicon glyphicon-random\"></span></button>  <button   class=\" desactivar btn btn-primary btn-xs ctooltip \" title=\"Cambiar Estatus\" ><span class=\"glyphicon glyphicon-remove-circle\"></span></button> </div>";

						}
					},
					"sDefaultContent" : "<div class=\"accion\"><button   class=\" editar btn btn-primary btn-xs ctooltip \" title=\"Editar Usuario\" ><span class=\"glyphicon glyphicon-pencil\"></span></button> <button   class=\" reiniciarPass btn btn-primary btn-xs ctooltip \" title=\"Reiniciar Password\"><span class=\"glyphicon glyphicon-random\"></span></button>  <button   class=\" desactivar btn btn-primary btn-xs ctooltip \" title=\"Cambiar Estatus\" ><span class=\"glyphicon glyphicon-remove-circle\"></span></button> </div>"

				} ];

		// inicializar el datatable de usuarios
		moduloDatatable(gridTableUsuarios)
				.inicializaDataTableServer(
						columnas,
						moduloGeneral
								.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/buscar"),
						{
							callback : function() {
								moduloGeneral.inicializatooltip('.ctooltip');
							}
						});
	};

	// asigna funcionalidad a botones
	inicializaControlesBusqueda = function() {
		var btnBusquedaSelector = "#btn-buscar";
		var ctrlPerfiles = "#comboPerfil";
		var ctrlEstatus = "#comboEstatus";
		$(btnBusquedaSelector).click(function() {
			buscarUsuarios();
		});

		moduloGeneral.inicializarSelectize(ctrlPerfiles);
		moduloGeneral.inicializarSelectize(ctrlEstatus);
	};

	bloqueaProcesoBusqueda = function() {

	};

	desbloqueaProcesoBusqueda = function() {

	};

	buscarUsuarios = function() {
		moduloDatatable(gridTableUsuarios).buscar(formParametroBusqueda);
	};

	mostrarMensaje = function() {
		$("#divMensaje").removeClass("mensajeOculto");
		$("#divMensaje").addClass("mensajeVisible");
	};

	ocultarMensaje = function() {
		$("#divMensaje").removeClass("mensajeVisible");
		$("#divMensaje").addClass("mensajeOculto");
	};

	inicializaControlesOpcionesUsuario = function() {

		// agregar un nuevo usuario
		$("#agregar")
				.on(
						"click",
						function() {
							$('#idUsuario').val("");
							$(formAgregarUsuario)
									.attr(
											"action",
											moduloGeneral
													.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/registroUsuario"));
							$(formAgregarUsuario).submit();

						});

		// editar usuario

		$(gridTableUsuarios)
				.delegate(
						".accion .editar",
						'click',
						function() {
							var usuario;
							var renglon = $(this).closest("tr");
							usuario = moduloDatatable(gridTableUsuarios)
									.obtenerDatosRowSeleccionado(renglon);

							$("#idUsuario").val(usuario['idUsuario']);
							$(formAgregarUsuario)
									.attr(
											"action",
											moduloGeneral
													.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/registroUsuario"));
							$(formAgregarUsuario).submit();

						});

		// modal para deshabilitar usuario

		$(gridTableUsuarios).delegate(
				".accion .desactivar",
				'click',
				function() {
					var usuario;
					var renglon = $(this).closest("tr");
					usuario = moduloDatatable(gridTableUsuarios)
							.obtenerDatosRowSeleccionado(renglon);
					$(modalEstatus).modal('show');
					$('#idUsuarioEstatus').val(usuario['idUsuario']);
				});

		// funcionalidad de reiniciar contrasenia
		$(gridTableUsuarios)
				.delegate(
						".accion .reiniciarPass",
						'click',
						function() {
							$(modalGenerico).modal('show');
							var usuario;
							var renglon = $(this).closest("tr");
							usuario = moduloDatatable(gridTableUsuarios)
									.obtenerDatosRowSeleccionado(renglon);
							$('#idElemento').val(usuario['idUsuario']);

							moduloGeneral
									.modalConfirmacion(
											TITULO_MODAL_REINICIO,
											MENSAJE_REINICIAR_PASSWORD,
											function() {
												$(modalGenerico).modal('hide');
												moduloGeneral
														.ejecutarAccion(
																moduloGeneral
																		.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/reiniciarContrasenia"),
																"#formGenerico",
																"#mensajeExito");
											}, "#mensajeConfirmacion",
											"#modalGeneral");

						});

		// funcion para desbloquear un usuario

		$(gridTableUsuarios)
				.delegate(
						".accion .desbloquear",
						'click',
						function() {

							$(modalGenerico).modal('show');
							var usuario;
							var renglon = $(this).closest("tr");
							usuario = moduloDatatable(gridTableUsuarios)
									.obtenerDatosRowSeleccionado(renglon);
							$('#idElemento').val(usuario['idUsuario']);
							moduloGeneral
									.modalConfirmacion(
											TITULO_MODAL_DESBLOQUEO,
											MENSAJE_DESBLOQUEO,
											function() {
												$(modalGenerico).modal('hide');

												moduloGeneral
														.ejecutarAccion(
																moduloGeneral
																		.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/desbloquear"),
																"#formGenerico",
																"#mensajeExito",
																function() {
																	mostrarMensaje();
																	buscarUsuarios();
																});
											}, "#mensajeConfirmacion",
											"#modalGeneral");

						});

		// guardar el cambio de estatus
		$('#cambiarEstatus')
				.on(
						'click',
						function() {
							moduloGeneral
									.ejecutarAccion(
											moduloGeneral
													.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/cambiarEstatus"),
											"#formEstatus", "#mensajeExito",
											function() {
												mostrarMensaje();
												buscarUsuarios();
											});
							moduloGeneral.limpiarFormulario("#formEstatus");
							$('.modal').modal('hide');

						});

		// acciones para el boton de cancelar cambio de estatus
		$('#cancelarEstatus').on('click', function() {
			moduloGeneral.limpiarFormulario("#formEstatus");
			$('.modal').modal('hide');

		});

		// acciones para el boton c
		$('#cerrarMensaje').on('click', function() {
			ocultarMensaje();

		});

	};

	return {
		init : init
	};

})();
