registrarUsuario =  (function() {
	
	init = function(){
		inicializaControles();
	
	};
	
	
	//metodo para inicializar los controles de busqueda.
	inicializaControles = function(){
		//Prepara el boton de buscar
		var btnGuardar = "#btn-guardar";
		var btnCancelar="#btn-cancelar";
		var formUsuario="#formRegistroUsuario";
		var ctrlPermiso='#perfiles';
		
		
		//Inicializa el boton para evento de spinner
		moduloGeneral.agregaAnimacionSppiner(btnGuardar);
		
		//Inicializa el control de permisos
		moduloGeneral.inicializarChoosen(ctrlPermiso);
		
		//inicializa el evento del boton guardar
		$(btnGuardar).click(function(){
			
		var opcionesAjax = {
					url : moduloGeneral.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios/guardar"),
					data : $(formUsuario).serialize(),
					beforeSend : function(){
						moduloGeneral.limpiarErrores();
						
					},
					success : function(){
						location.href= moduloGeneral.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios?m=1");
					     
						
					},
					error : function(jqXHR, textStatus, errorThrown){
							//muestra los errores en el formulario
				    		moduloGeneral.mostrarErrores(jqXHR.responseJSON, $(formUsuario));
				    		},
					complete : function(){
						//Detiene todos los efectos del sppiner
			    		moduloGeneral.detenerAnimacionesSppiner();
			    	   
					} 
				};
			moduloGeneral.ajaxSubmit(opcionesAjax);	
		});
		
		//inicializa el evento de cancelar
		$(btnCancelar).click(function(){
			 moduloGeneral.limpiarErrores();
			 location.href= moduloGeneral.getUrlConContexto("/modules/administracion/usuarios/consultaUsuarios");
			
		});
		
	};

	/*
	 * Metodo que inicializa las funciones generales de la pagina
	 */
	return {
		init : init
	};
	
})();













