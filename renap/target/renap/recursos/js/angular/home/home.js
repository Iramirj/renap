'use strict';

angular.module('appRenap', ['angular-md5']).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] =     $('meta[name="csrf-token"]').attr('content');
}]).factory('homeService',  function($http, $q){
	return{
		__getHeaderToken: function(){
			var token = $("meta[name='_csrf']").attr("content");
			var headerToken = $("meta[name='_csrf_header']").attr("content");	    					
			var headers = {};
			
			headers[headerToken] = token;
			return headers;
		},
		getName: function (flName){
			console.log("getName: " + flName);
			var defer = $q.defer();
			$http.get('renap/modules/home/returnName/' + flName)
			.success(function(data){
				console.log("\u00c9xito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtuvo respuesta del servidor: " + data);
				defer.reject(data);
			});
			return defer.promise;
		},
		getDetalleUserById: function (flIdUsuario){
			console.log("getDetalleUserById: " + flIdUsuario);
			var defer = $q.defer();
			$http.get('renap/modules/home/returnDetalle/' + flIdUsuario)
			.success(function(data){
				console.log("\u00c9xito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtuvo respuesta del servidor: " + data);
				defer.reject(data);
			});
			return defer.promise;
		},
		changePassword: function (flIdUser, flPassChange){
			console.log("changePassword: " + flPassChange);
			var defer = $q.defer();
			var flDatos = {
					flIdUsuario: flIdUser,
					flPassword: flPassChange
			};

			$http({
				method: 'POST',
				url: 'renap/modules/home/changePassword',
				data    : JSON.stringify(flDatos),
				headers: __getHeaderToken()				
			})			
			.success(function(data){
				console.log("\u00c9xito: respuesta correcta");
				defer.resolve(data);
			}).error(function (data){
				console.log("Error: no se obtuvo respuesta del servidor: " + data);
				defer.reject(data);
			});
			return defer.promise;
		}
	};
}).controller('HomeController',
		[ 'homeService', '$scope', '$http', '$location', 'md5', function( homeService, $scope, $http, $location, md5) {

			var flPass = "";
			var idUser = "";
			$scope.claveNueva = "";
			$scope.claveActual = "";
			$scope.claveConfirma = "";
			$scope.showBEnvia = true;
			$scope.disablesPass = false;
			$scope.disablePassActual = false;
			$scope.resultMesagge = "none";
			
			//Codigo para eliminar
			var flnombre = "....";
			$scope.nombre = "";
			homeService.getName(flnombre).then(function(data) {
				$scope.nombre = data;
			});
			
			
			$scope.validatePasswordOriginal = function(){
				idUser = document.getElementById("inputHidden").value;
				if($scope.claveActual != null && idUser.length != null){
					if(flPass == ""){
						homeService.getDetalleUserById(idUser).then(function(data) {
							flPass = data.password;
						});				
					}
					var flPassMd5 = md5.createHash($scope.claveActual || '');
					if(flPass.length == flPassMd5.length){
						if(flPass == flPassMd5){
							$scope.disablesPass = false;
						}else{
							$scope.disablesPass = true;
							$scope.showBEnvia = true;
						}
					}else{
						$scope.flMesageChange = "diferent";
						$scope.disablesPass = true;
						$scope.showBEnvia = true;
					}					
				}
			};
			
			$scope.validatePasswordChange = function(){
				if($scope.claveNueva != null && $scope.claveActual != null && $scope.claveConfirma != null){
					if($scope.claveNueva.length > 6 && $scope.claveNueva.length == $scope.claveConfirma.length){
						if($scope.claveNueva == $scope.claveConfirma){
							$scope.showBEnvia = false;
						}else{
							$scope.showBEnvia = true;
							$scope.flMessajeValidate = "diferent";
						}
					}else{
						$scope.showBEnvia = true;
					}
				}else{
					$scope.flMessajeValidate = "empty";
					$scope.showBEnvia = true;
				}
			};
			
			$scope.changePassword = function(){
				var flPassNew = md5.createHash($scope.claveNueva || '');
				homeService.changePassword(idUser, flPassNew).then(function(data) {
					if(data){
						$scope.result = data;
						$scope.resultMesagge = "block";
						$scope.headTextMesagge = "\u00c9xito:";
						$scope.textMesagge = "La contrase\u00f1a fue actualizada correctamente, "
							+ "favor de reiniciar su sesi\u00f3n para aplicar los cambios";
					}
					$scope.claveNueva = "";
					$scope.claveActual = "";
					$scope.claveConfirma  = "";
					$scope.showBEnvia = true;
					$scope.disablesPass = true;
					$scope.disablePassActual = true;
				});		
			};

			
			$scope.getResources = function() {
				console.log("result");
				homeService.getName(flnombre).then(function(data) {
					console.log("data: " + data);
					$scope.nombre = data;
				});
			};
			
			
		} ]);