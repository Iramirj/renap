var moduloDatatable = (function(selector) {
	
	var selectorDatatable = selector;

	
	
		
	/**
	*
	*/
	_getInstance = function(){
							var instance = null;
							var datatable = $(selector).dataTable();
							if(datatable != null){
								instance = datatable.api();
							}
							return instance;
						};
	
	/**
	*
	*/
	_muestraError = function(){
							BootstrapDialog.show({
								type : BootstrapDialog.TYPE_DANGER,
								title : etiquetas_js.ERROR_TITULO,
								message : etiquetas_js.DATATABLE_ETIQUETAS_ERROR
							});
					};
	
	/*
	 * 
	 */
	_generaFormDataCache = function(selectorForm){
					
					var filtros = {};
					if( selectorForm != null){
						filtros = moduloGeneral.convertirFormularioJson(selectorForm);	
					}
					$(selectorDatatable).data("datosBusqueda", filtros); 
	};
	
	/*
	 * 
	 */
	__obtenFormDataCache = function(){
								var datosBusquedaForm = $(selectorDatatable).data("datosBusqueda");
								return datosBusquedaForm;
							};
	
							
							
		/** 
		 * @param selectorDatatable		El selector de la tabla 
		 * @param columnas				La configuracion de las columnas
		 * @param opcionesDataTable   opciones adicionales de configuracion de la tabla 
		 * 			opcionesDataTable  = {
											iDisplayLength : 10 		//registros por pagina de datos a mostrar (10 default)
											error 		: null,	//Funcion a ejecutar cuando es un error
										};
		 */	
		inicializaDataTableCliente = function(columnas,  opcionesDataTable ) {
									var opcionesCliente = {
											"pagingType"	: "full_numbers",
											"responsive": true,
											"ordering"		: false,		//Oculta las opciones de ordenar
											"lengthChange" 	: false,		//Oculta la posibilidad de cambiar el numero de resultados
											"filter"		: false,       	//Oculta el filtro general
											"serverSide"	: false,		//Habilita el paginado por medio del servidor de aplicaciones
											"iDisplayLength": opcionesDataTable.iDisplayLength == null ? 10 : opcionesDataTable.iDisplayLength,
											"columns" :	opcionesDataTable.columnas,
											"language" : etiquetas_js.DATATABLE_ETIQUETAS
									};
									$(selectorDatatable).DataTable(opcionesCliente);							
							};
							


							
	
		/**
		 * 
		 * @param selectorDatatable		El selector de la tabla 
		 * @param columnas				La configuracion de las columnas
		 * @param opcionesDataTable   opciones adicionales de configuracion de la tabla  opcionales
		 * 			opcionesDataTable  = {
											iDisplayLength : 10 		//registros por pagina de datos a mostrar (10 default)
											callback : null 			//Funcion a ejecutar una vez concluido la busqueda ( ejemplo : inicializar componentes )
										};
		 */
		inicializaDataTableServer = function(columnas, url,  opcionesUsuarioDataTable ) {
												
									var opcionesServidor = {
														"pagingType"	: "full_numbers",
														"ordering"		: false,		//Oculta las opciones de ordenar
														"lengthChange" 	: false,		//Oculta la posibilidad de cambiar el numero de resultados
														"filter"		: false,       	//Oculta el filtro general
														"serverSide"	: true,		//Habilita el paginado por medio del servidor de aplicaciones
														"iDisplayLength": opcionesUsuarioDataTable.iDisplayLength == null ? 10 : opcionesUsuarioDataTable.iDisplayLength,
														"columns": columnas,
														"fnDrawCallback" : function (oSettings) {	//Metodo que se ejecuta al finalizar la peticion
																if(opcionesUsuarioDataTable.callback!=null){
																	opcionesUsuarioDataTable.callback();
																}
														}
												};
														
										 $(selectorDatatable).DataTable({
															"pagingType"	: "full_numbers",
															"responsive": true,
															"ordering"		: false,			//Oculta las opciones de ordenar
															"lengthChange" 	: false,		//Oculta la posibilidad de cambiar el numero de resultados
															"filter"		: false,        	//Oculta el filtro general
															"serverSide"	: true,			//Habilita el paginado por medio del servidor de aplicaciones
															"iDisplayLength": opcionesServidor.iDisplayLength,		//Define el numero maximo por tabla					                   
															"fnDrawCallback": opcionesServidor.fnDrawCallback,
															"deferLoading":0,
															"columns":opcionesServidor.columns,
															"language" : etiquetas_js.DATATABLE_ETIQUETAS,
															"processing" : true,
															"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
											                    	var datosBusquedaForm = __obtenFormDataCache();
																	
																	var dataDataTable = {};
											                    	$.each(aoData, function() {
											     			            if (dataDataTable[this.name] !== undefined) {
											     			                if (!dataDataTable[this.name].push) {
											     			                	dataDataTable[this.name] = [dataDataTable[this.name]];
											     			                }
											     			                dataDataTable[this.name].push(this.value == null ? "" : this.value);
											     			            } else {
											     			            	dataDataTable[this.name] = this.value == null ? "" : this.value;
											     			            }
											     			        });
											                    	$.extend(dataDataTable,datosBusquedaForm);					                    	
											                    	
											                        oSettings.jqXHR = $.ajax( {
											                        	"contentType" : 'application/x-www-form-urlencoded',
											                            "dataType" : 'json',
											                            "type": "POST",
											                            "url": url,
											                            "data": dataDataTable,
											                            "headers" : __getHeaderToken(),
											                            "success": fnCallback,
											                            error: function (xhr, textStatus, error){
											                            	if(moduloGeneral != null && moduloGeneral.redirectLogin != null){
																				moduloGeneral.redirectLogin();
																			} else {
																				_muestraError();	
																			}										                            		
											                            }				                          
											                        } );
											                    },
												});
							};
							
	/*
	* selectorFormFiltros (opcional : null default) Id del selector del formulario de filtros.
	*/
	buscar	 = function(selectorFormFiltros){
		_generaFormDataCache(selectorFormFiltros);
		
		var $datatable = _getInstance();
		$datatable.draw();
	};
							
							
	/*
	 * 
	 */						
	obtenerDatosRowSeleccionado = function(tr) {
										var row = _getInstance().row(tr); 
										return row.data();
									};

	return {
		inicializaDataTableCliente : inicializaDataTableCliente,
		inicializaDataTableServer : inicializaDataTableServer,
		buscar : buscar,
		obtenerDatosRowSeleccionado: obtenerDatosRowSeleccionado
	};
	
});