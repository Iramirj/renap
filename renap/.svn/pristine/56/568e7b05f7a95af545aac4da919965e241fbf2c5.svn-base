<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mvc="http://www.springframework.org/schema/mvc"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation=" 
		http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
		http://www.springframework.org/schema/mvc 
		http://www.springframework.org/schema/mvc/spring-mvc-3.2.xsd
		http://www.springframework.org/schema/context 
		http://www.springframework.org/schema/context/spring-context-3.2.xsd">
	
	<!-- Propiedades generales de configuracion de un aplicativo -->
	<bean id="propertyConfigurer" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer"> 
		<property name="locations">
			<list> 
				<value>classpath:config/properties/configuracion.properties</value>				
			</list>
		</property>
	</bean>
	
	<!--Configuracion del spring MVC -->
	<bean class="org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter"/>
		
	<mvc:annotation-driven validator="validator"  conversion-service="conversionService"/>
	
	<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
		<property name="converters">
	       <set>
	         <bean class="mx.gob.segob.dgti.infraestructure.base.presentation.converter.MultipartConverterString" />	         	         
	       </set>
	    </property>
		<property name="formatters">
	       <set>
	         <bean class="mx.gob.segob.dgti.infraestructure.base.presentation.formatter.DateFormatter" />
	       </set>
	    </property>
	</bean>
	
	<bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean" />
	
	<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
	   <property name="basename" value="bundles.views.UIMessages" />    	
  	</bean>
  	
	<context:component-scan base-package="mx.gob.segob.dgti" >
		<context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
		<context:exclude-filter type="annotation" expression="org.springframework.stereotype.Service"/>
		<context:exclude-filter type="annotation" expression="org.springframework.stereotype.Repository"/>		
	</context:component-scan>
	
	<!-- Configuracion de vistas Controladores simples--> 
	<!-- Main view -->
	<mvc:view-controller path="/" view-name="main"/>	
	<mvc:view-controller path="/main" view-name="main"/>
	<!-- Error -->
	<!-- Acceso prohibido -->	
	<mvc:view-controller path="/denegado" view-name="error/denegado"/>
	<!-- Error General-->
	<mvc:view-controller path="/error" view-name="error/errorGeneral"/>
	<!-- 404-->
	<mvc:view-controller path="/404" view-name="error/404"/>
	<mvc:view-controller path="/500" view-name="error/500"/>
	
	<mvc:resources location="/recursos/css/" mapping="/recursos/css/**"/>
	<mvc:resources location="/recursos/imagenes/" mapping="/recursos/imagenes/**"/>
	<mvc:resources location="/recursos/js/" mapping="/recursos/js/**"/>
	<mvc:resources location="/recursos/assets/" mapping="/recursos/assets/**"/>
	
	 <!--Configuracion para habilitar la carga de archivos -->
	<bean id="multipartResolver"
	    class="org.springframework.web.multipart.commons.CommonsMultipartResolver">	
	    <!-- one of the properties available; the maximum file size in bytes -->
	    <property name="maxUploadSize" value="${upload.file.size}"/>
	</bean>
	
	<!-- Configuracion de motor de maquetado para el front (THYMELEAF) -->	
	  <!-- **************************************************************** -->
	  <!--  THYMELEAF-SPECIFIC ARTIFACTS                                    -->
	  <!--  TemplateResolver <- TemplateEngine <- ViewResolver              -->
	  <!-- **************************************************************** -->

  
  <bean id="templateResolver"
        class="org.thymeleaf.templateresolver.ServletContextTemplateResolver">
    <property name="prefix" value="/WEB-INF/paginas/" />
    <property name="suffix" value=".html" />
    <property name="templateMode" value="HTML5" />
     <property name="characterEncoding" value="UTF-8" />
    <!-- Template cache is true by default. Set to false if you want -->
    <!-- templates to be automatically updated when modified.        -->
    <property name="cacheable" value="${layout.html.cacheable}" /> 
  </bean>
  
  <bean class="org.thymeleaf.spring3.view.ThymeleafViewResolver">
    <property name="templateEngine" ref="templateEngine" />
    <property name="characterEncoding" value="UTF-8" />    
  </bean>
  
  <bean id="templateEngine" class="org.thymeleaf.spring3.SpringTemplateEngine">
    <property name="templateResolver" ref="templateResolver" />
    <property name="additionalDialects">
            <set>
            	<bean class="org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect" />
                <bean class="nz.net.ultraq.thymeleaf.LayoutDialect" />
                <bean class="com.github.mxab.thymeleaf.extras.dataattribute.dialect.DataAttributeDialect" />
            </set>
        </property>
  </bean>    
</beans>