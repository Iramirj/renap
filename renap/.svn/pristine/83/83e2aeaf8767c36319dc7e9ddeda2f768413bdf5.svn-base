
package mx.gob.segob.dgti.appPackageRenombra.administracion.usuario.business.builder;

import java.util.ArrayList;
import java.util.List;

import mx.gob.segob.dgti.appPackageRenombra.administracion.usuario.business.vo.PerfilVO;
import mx.gob.segob.dgti.appPackageRenombra.administracion.usuario.integration.entity.Perfil;
import mx.gob.segob.dgti.appPackageRenombra.administracion.usuario.integration.entity.Permiso;
import mx.gob.segob.dgti.appPackageRenombra.comun.business.constants.EstatusEnum;
import mx.gob.segob.dgti.appPackageRenombra.comun.business.constants.PermisoEnum;
import mx.gob.segob.dgti.infraestructure.base.business.builder.impl.BaseBuilder;

public class PerfilBuilder extends BaseBuilder<PerfilVO, Perfil>{

	PermisoBuilder permisoBuilder=new PermisoBuilder();
	@Override
	public Perfil convertirVOAEntidad(PerfilVO vo) {
		Perfil entidad= null;
		if(vo!= null){
			entidad= new Perfil();
			entidad.setIdPerfil(vo.getIdPerfil());
			entidad.setClavePerfil(vo.getClavePerfil());
			entidad.setDescripcion(vo.getDescripcion());
			entidad.setListaPermisos(convertirPermisoEnumAEntidad(vo.getListaPermiso()));
			if(vo.getEstatus().equals(EstatusEnum.ACTIVO)){
				entidad.setActivo(Boolean.TRUE);
			}
			else{
				entidad.setActivo(Boolean.FALSE);
			}
			
			
		}
		return entidad;
	}

	@Override
	public PerfilVO convertirEntidadAVO(Perfil entidad) {
		PerfilVO vo= null;
		if(entidad!= null){
			vo= new PerfilVO();
			vo.setIdPerfil(entidad.getIdPerfil());
			vo.setClavePerfil(entidad.getClavePerfil());
			vo.setDescripcion(entidad.getDescripcion());
			vo.setListaPermiso(convertirEntidadAPermisoEnum(entidad.getListaPermisos()));
			if(entidad.isActivo()){
				vo.setEstatus(EstatusEnum.ACTIVO);
			}
			else{
				vo.setEstatus(EstatusEnum.INACTIVO);
			}
		}
		return vo;
	}
	
	public List<Permiso> convertirPermisoEnumAEntidad(List<PermisoEnum> listaPermiso){
		List<Permiso> listaPermisoEntidad=null;
		if(listaPermiso!=null && !listaPermiso.isEmpty()){
			listaPermisoEntidad=new ArrayList<Permiso>();
			for(PermisoEnum permiso:listaPermiso){
				Permiso permisoEntidad=new Permiso();
				permisoEntidad.setIdPermiso(permiso);
				listaPermisoEntidad.add(permisoEntidad);				
			}
			
		}
		return listaPermisoEntidad;
		
	}
	
	public List<PermisoEnum> convertirEntidadAPermisoEnum(List<Permiso> listaPermiso){
		List<PermisoEnum> listaPermisoEnum=null;
		if(listaPermiso!=null && !listaPermiso.isEmpty()){
			listaPermisoEnum=new ArrayList<PermisoEnum>();
			for(Permiso permiso:listaPermiso){
				listaPermisoEnum.add(permiso.getIdPermiso());				
			}
			
		}
		return listaPermisoEnum;
		
	}
	
	public List<Integer> convertirEntidadesAId(List<Perfil> listaPerfiles)
	{
		List<Integer> listaIds = null;
		if(listaPerfiles !=null && !listaPerfiles.isEmpty())
		{
			listaIds =  new ArrayList<Integer>();
			
			for(Perfil perfil : listaPerfiles)
			{
				listaIds.add(perfil.getIdPerfil());
			}
		}
		return listaIds;
	}

}
