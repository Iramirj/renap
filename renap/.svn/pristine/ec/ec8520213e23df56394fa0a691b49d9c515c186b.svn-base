package mx.gob.segob.dgti.infraestructure.mail.service.impl;

import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import mx.gob.segob.dgti.infraestructure.mail.service.MailServices;
import mx.gob.segob.dgti.infraestructure.mail.vo.MailAttachment;
import mx.gob.segob.dgti.infraestructure.mail.vo.MailMessage;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;


@Service
public class MailServicesImpl implements MailServices {

	/** debug. */
	public boolean debug;

	/** use ssl. */
	public boolean useSSL;

	/** smtp host. */
	public String smtpHost;

	/** smtp port. */
	public int smtpPort;

	/** smtp username. */
	public String smtpUsername;

	/** smtp password. */
	public String smtpPassword;

	/** char set. */
	public String charSet;
	

	/** logger. */
	protected final Logger logger = LogManager
			.getLogger(MailServicesImpl.class);

	/**
	 * El constructor de mail services impl.
	 */
	public MailServicesImpl() {

	}


	/**
	 * Metodo que soporta correo con formato HTML
	 * 
	 * @param mailMessage : Parametro contiene la informacion del mensaje de correo
	 * 
	 *  from 	: email que se identificara como remitente
	 *  to 		: email a quien se realizara el envio del correo
	 *  subject : asunto que identificara el correo 
	 *  body	: cuerpo del correo ( soporta formato html )
	 *  
	 *  attachments 	: elementos a adjuntar al correo de existir elementos.
	 *  resourcesInLine : elementos a incrustar en el correo ( identificado por un CID (String), y el recurso ) 
	 */
	public void enviarTexto(MailMessage mailMessage) {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(this.smtpHost);
		sender.setPort(this.smtpPort);
		sender.setUsername(this.smtpUsername);
		sender.setPassword(this.smtpPassword);
		sender.getSession().setDebug(isDebug());
		
		MimeMessage message = sender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(mailMessage.getFrom());
			helper.setTo(mailMessage.getTo());
			helper.setSubject(mailMessage.getSubject());

			helper.setText(mailMessage.getBody(), false);

			
			if(mailMessage.getResourcesInLine() != null ) {
				for (Map.Entry<String, Resource> entry : mailMessage.getResourcesInLine()
						.entrySet()) {	
					String cid = entry.getKey();
					Resource resource = entry.getValue();
					helper.addInline(cid, resource);
				}
			}
			
			if(mailMessage.getAttachments() != null){
				for(MailAttachment attachmentDocument : mailMessage.getAttachments()){
					ByteArrayResource attachment = new ByteArrayResource(attachmentDocument.getContent());
					helper.addAttachment(attachmentDocument.getName(), attachment);
				}
			}
			
			sender.send(message);
		} catch (MessagingException exception) {
			logger.error(exception.getMessage());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
		}
	}


	/**
	 * Metodo que soporta correo con formato HTML
	 * 
	 * @param mailMessage : Parametro contiene la informacion del mensaje de correo
	 * 
	 *  from 	: email que se identificara como remitente
	 *  to 		: email a quien se realizara el envio del correo
	 *  subject : asunto que identificara el correo 
	 *  body	: cuerpo del correo ( soporta formato html )
	 *  
	 *  attachments 	: elementos a adjuntar al correo de existir elementos.
	 *  resourcesInLine : elementos a incrustar en el correo ( identificado por un CID (String), y el recurso ) 
	 */
	public void enviarCorreoHtml(MailMessage mailMessage) {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(this.smtpHost);
		sender.setPort(this.smtpPort);
		sender.setUsername(this.smtpUsername);
		sender.setPassword(this.smtpPassword);
		sender.getSession().setDebug(isDebug());

		MimeMessage message = sender.createMimeMessage();
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(mailMessage.getFrom());
			helper.setTo(mailMessage.getTo());
			helper.setSubject(mailMessage.getSubject());
			helper.setText(mailMessage.getBody(), true);
			
			if(mailMessage.getResourcesInLine() != null ) {
				for (Map.Entry<String, Resource> entry : mailMessage.getResourcesInLine()
						.entrySet()) {
					String cid = entry.getKey();
					Resource resource = entry.getValue();
					helper.addInline(cid, resource);
				}
			}

			if(mailMessage.getAttachments() != null){
				for(MailAttachment attachmentDocument : mailMessage.getAttachments()){
					ByteArrayResource attachment = new ByteArrayResource(attachmentDocument.getContent());
					helper.addAttachment(attachmentDocument.getName(), attachment);
				}
			}
			sender.send(message);
		} catch (MessagingException exception) {
			logger.error(exception.getMessage());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
		}

	};
	
	
	/**
	 * Metodo que soporta correo con formato HTML
	 * 
	 * @param mailMessage : Parametro contiene la informacion del mensaje de correo
	 * @param template : plantilla html de carpeta /WebInf/email/templates
	 * 
	 *  from 	: email que se identificara como remitente
	 *  to 		: email a quien se realizara el envio del correo
	 *  subject : asunto que identificara el correo 
	 *  body	: cuerpo del correo ( soporta formato html )
	 *  
	 *  attachments 	: elementos a adjuntar al correo de existir elementos.
	 *  resourcesInLine : elementos a incrustar en el correo ( identificado por un CID (String), y el recurso ) 
	 */
	public void enviarCorreoHtmlTemplate(MailMessage mailMessage) {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(this.smtpHost);
		sender.setPort(this.smtpPort);
		sender.setUsername(this.smtpUsername);
		sender.setPassword(this.smtpPassword);
		sender.getSession().setDebug(isDebug());

		MimeMessage message = sender.createMimeMessage();
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(mailMessage.getFrom());
			helper.setTo(mailMessage.getTo());
			helper.setSubject(mailMessage.getSubject());
			helper.setText(mailMessage.getBody(), true);
			
			if(mailMessage.getResourcesInLine() != null ) {
				for (Map.Entry<String, Resource> entry : mailMessage.getResourcesInLine()
						.entrySet()) {
					String cid = entry.getKey();
					Resource resource = entry.getValue();
					helper.addInline(cid, resource);
				}
			}

			if(mailMessage.getAttachments() != null){
				for(MailAttachment attachmentDocument : mailMessage.getAttachments()){
					ByteArrayResource attachment = new ByteArrayResource(attachmentDocument.getContent());
					helper.addAttachment(attachmentDocument.getName(), attachment);
				}
			}
			sender.send(message);
		} catch (MessagingException exception) {
			logger.error(exception.getMessage());
		} catch (Exception exception) {
			logger.error(exception.getMessage());
		}

	};
	
	/**
	 * Verifica si debug es verdadero.
	 * 
	 * @return verdadero si es debug
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * Establece debug.
	 * 
	 * @param debug
	 *            el nuevo debug
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	/**
	 * Verifica si use ssl es verdadero.
	 * 
	 * @return verdadero si es use ssl
	 */
	public boolean isUseSSL() {
		return useSSL;
	}

	/**
	 * Establece use ssl.
	 * 
	 * @param useSSL
	 *            el nuevo use ssl
	 */
	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
	}

	/**
	 * Obtiene smtp host.
	 * 
	 * @return smtp host
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * Establece smtp host.
	 * 
	 * @param smtpHost
	 *            el nuevo smtp host
	 */
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	/**
	 * Obtiene smtp port.
	 * 
	 * @return smtp port
	 */
	public int getSmtpPort() {
		return smtpPort;
	}

	/**
	 * Establece smtp port.
	 * 
	 * @param smtpPort
	 *            el nuevo smtp port
	 */
	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	/**
	 * Obtiene smtp username.
	 * 
	 * @return smtp username
	 */
	public String getSmtpUsername() {
		return smtpUsername;
	}

	/**
	 * Establece smtp username.
	 * 
	 * @param smtpUsername
	 *            el nuevo smtp username
	 */
	public void setSmtpUsername(String smtpUsername) {
		this.smtpUsername = smtpUsername;
	}

	/**
	 * Obtiene smtp password.
	 * 
	 * @return smtp password
	 */
	public String getSmtpPassword() {
		return smtpPassword;
	}

	/**
	 * Establece smtp password.
	 * 
	 * @param smtpPassword
	 *            el nuevo smtp password
	 */
	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	/**
	 * Obtiene char set.
	 * 
	 * @return char set
	 */
	public String getCharSet() {
		return charSet;
	}

	/**
	 * Establece char set.
	 * 
	 * @param charSet
	 *            el nuevo char set
	 */
	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

}
